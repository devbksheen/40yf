<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Model extends CI_Model
{
    protected $table;
    protected $primaryKey;
    protected $jsonDataKey;

    public function __construct()
    {
        parent::__construct();
        $this->table = $this->table ? $this->table." A" : Null; 
    }

    private function _setQuery($data, $sop = "AND")
    {
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                if (empty($value)) continue;
                switch ($key) {
                    case 'select':
                        $this->db->select($value);
                        break;
                    case 'join':
                        if (!empty($value['table']) && !empty($value['on'])) {
                            $value['type'] = isset($value['type']) ? $value['type'] : "INNER";
                            $this->db->join($value['table'], $value['on'], $value['type']);
                        } else {
                            foreach ($value as $key2 => $join) {
                                $join['type'] = isset($join['type']) ? $join['type'] : "INNER";
                                $this->db->join($join['table'], $join['on'], $join['type']);
                            }
                        }
                        break;
                    case 'where':
                        $this->db->group_start();
                        if (strtoupper($sop) == "AND") {
                            $this->db->where($value);
                        } else {
                            $this->db->or_where($value);
                        }           
                        $this->db->group_end();
                        break;
                    case 'freeWhere':
                        $this->db->group_start();
                        if (strtoupper($sop) == "AND") {
                            $this->db->where($value, null, false);
                        } else {
                            $this->db->or_where($value, null, false);
                        }           
                        $this->db->group_end();
                        break;
                    case 'like':
                        $this->db->group_start();
                        if (strtoupper($sop) == "AND") {
                            $this->db->like($value);
                        } else {
                            $this->db->or_like($value);
                        }           
                        $this->db->group_end();
                        break;
                    case 'groupBy':
                        $this->db->group_by($value);
                        break;
                    case 'limit':
                        if (!empty((int)$value)) {
                            if (isset($data['page']) && !empty((int)$data['page'])) {
                                $offset = ((int)$data['page'] - 1) * (int)$value;
                            } else {
                                $offset = '';
                            }
                            $this->db->limit($value, $offset);
                        }
                        break;
                    case 'orderBy':
                        if (is_array($value)) {
                            foreach ($value as $index => $order) {
                                $this->db->order_by($index, $order);
                            }
                        } else {
                            $this->db->order_by($value);
                        }
                    break;
                }
            }
        }
    }

    public function getList($data = array()) 
    {
        if (empty($this->table) || empty($this->primaryKey)) {
            echo "Please set table and primaryKey of getList function";
            exit;
        }
        
        if (!isset($data['orderBy'])) $data['orderBy'] = 'A.'.$this->primaryKey." DESC";

        $this->db->reset_query();
        $this->db->from($this->table);
        $this->_setQuery($data);
        $qry = $this->db->get();
        $list = $qry->result_array();

        if (!empty($this->jsonDataKey))
        $list = $this->jsonDecodeData($list, __FUNCTION__);

        if (isset($data['limit']) && isset($data['page'])) {
            $limit = $data['limit'];
            $page = $data['page'];
            unset($data['select']);
            unset($data['limit']);
            unset($data['page']);

            $this->db->reset_query();
            $this->db->from($this->table);
            $this->db->select('count(*) as rownum');
            $this->_setQuery($data);

            $qry = $this->db->get();
            $rows = $qry->row_array();
            $count = $rows['rownum'];
            
            if (!empty($list)) {
                $num = $count - ($limit * ($page - 1));
                foreach ($list as &$row) {
                    $row['_num'] = $num;
                    $num--;
                }
            }

            return array('list' => $list, 'count' => $count);
        } else {
            return $list;
        }
    }

    public function getInfo($data = array()) 
    {
        if (empty($this->table) || empty($this->primaryKey)) {
            echo "Please set table and primaryKey of getInfo function";
            exit;
        }

        $this->db->reset_query();
        $this->db->from($this->table);
        $this->_setQuery($data);
        $qry = $this->db->get();
        $result = $qry->row_array();

        if (!empty($this->jsonDataKey))
        $result = $this->jsonDecodeData($result, __FUNCTION__);

        return $result;
    }

    public function getInfoById($id, $data = array())
    {
        if (empty($this->table) || empty($this->primaryKey)) {
            echo "Please set table and primaryKey of getInfoById function";
            exit;
        }
        $data['where'][$this->primaryKey] = $id;
        $result = $this->getInfo($data);
        return $result;
    }

    public function getCount($data = array()) 
    {
        if (empty($this->table) || empty($this->primaryKey)) {
            echo "Please set table and primaryKey of getCount function";
            exit;
        }

        $this->db->reset_query();
        $this->db->from($this->table);
        $this->_setQuery($data);
        return $this->db->count_all_results();
    }
    
    public function update($updateData, $data = array()) 
    {
        if (empty($this->table) || empty($this->primaryKey)) {
            echo "Please set table and primaryKey of update function";
            exit;
        }

        if (empty($updateData)) return false;

        if (!empty($this->jsonDataKey))
        $updateData = $this->jsonEncodeData($updateData);

        $this->db->reset_query();
        $this->db->set($updateData);
        if (empty(element('where', $data)) && empty(element('freeWhere', $data)) && empty(element('like', $data))) {
            $result = $this->db->insert(str_replace(' A', '', $this->table));
            if ($result) $result = $this->db->insert_id();
        } else {
            $this->_setQuery($data);
            $result = $this->db->update(str_replace(' A', '', $this->table));
        }
        return $result;
    }

    public function updateById($id, $updateData)
    {
        if (empty($this->table) || empty($this->primaryKey)) {
            echo "Please set table and primaryKey of updateById function";
            exit;
        }

        if (!empty($this->jsonDataKey))
        $updateData = $this->jsonEncodeData($updateData);

        if (isset($updateData[$this->primaryKey]))
        unset($updateData[$this->primaryKey]);
        
        $data = array('where' => array($this->primaryKey => $id));
        $result = $this->update($updateData, $data);
        return $result;
    }

    public function delete($data)
    {
        if (empty($this->table) || empty($this->primaryKey)) {
            echo "Please set table and primaryKey of delete function";
            exit;
        }

        if (empty(element('where', $data)) && empty(element('freeWhere', $data)) && empty(element('like', $data))) {
            echo "The delete function requires where or freeWhere or like";
            exit;
        
        }
        $this->db->reset_query();
        $this->_setQuery($data);
        $result = $this->db->delete(str_replace(' A', '', $this->table));
        return $result;
    }

    public function deleteById($id)
    {
        if (empty($this->table) || empty($this->primaryKey)) {
            echo "Please set table and primaryKey of deleteById function";
            exit;
        }
        
        $data = array('where' => array($this->primaryKey => $id));
        $result = $this->delete($data);
        return $result;
    }

    public function jsonEncodeData($data)
    {
        if (!empty($this->jsonDataKey)) {
            foreach ($this->jsonDataKey as &$key) {
                if (isset($data[$key]) && $data[$key] && is_array($data[$key])) {
                    $data[$key] = json_encode($data[$key]);
                }
            }
        }
        return $data;
    }

    public function jsonDecodeData($data, $func)
    {
        if (!empty($this->jsonDataKey)) {
            if ($func == "getList") {
                foreach ($this->jsonDataKey as &$key) {
                    foreach ($data as &$item) {
                        if (isset($item[$key]) && $item[$key] && !is_array($item[$key])) {
                            $item[$key] = json_decode($item[$key], true);
                        }
                    }
                }
            } else if ($func == "getInfo") {
                foreach ($this->jsonDataKey as &$key) {
                    if (isset($data[$key]) && $data[$key] && !is_array($data[$key])) {
                        $data[$key] = json_decode($data[$key], true);
                    }
                }
            }
        }
        return $data;
    }
}