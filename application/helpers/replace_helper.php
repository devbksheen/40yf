<?php

function getRv($id)
{
    $jsonData = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/application/json/replaceValue.json'), true);
    if (isset($jsonData[$id])) {
        if (isset($jsonData[$id]['DESCRIPTION'])) unset($jsonData[$id]['DESCRIPTION']);
        return $jsonData[$id];
    } else {
        return false;
    }
}

function rv($id, $value, $none = false)
{   
    $jsonData = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/application/json/replaceValue.json'), true);
    if (isset($jsonData[$id][$value])) {
        return $jsonData[$id][$value];
    } else {
        return $none;
    }
}

function rvArray($id, $valueArr, $none = array())
{   
    $jsonData = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/application/json/replaceValue.json'), true);
    if (isset($jsonData[$id])) {
        $result = array();
        if (!empty($valueArr)) {
            foreach ($valueArr as &$value) {
                if (isset($jsonData[$id][$value])) {
                    $result[] = $jsonData[$id][$value];
                }
            }
        }
        return $result;
    } else {
        return $none;
    }
    
}