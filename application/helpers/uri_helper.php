<?php

function getParams($getKey)
{
    $result = "";
    $REQUEST_URI = explode("?", $_SERVER['REQUEST_URI']);
    if (isset($REQUEST_URI[1])) {
        $paramsArr = explode("&", $REQUEST_URI[1]);
        foreach ($paramsArr as &$value) {
            $valueArr = explode("=", $value);
            if ($valueArr[0] == $getKey) {
                $result = urldecode($valueArr[1]);
                break;
            }
        }
    }
    return $result;
}   

function getLimit($limit = 5) 
{
    return getParams('limit') ? getParams('limit') : $limit;
}

function getPage()
{
    return getParams('page') ? getParams('page') : 1;
}

function getUri() 
{
    $REQUEST_URI = explode('?', $_SERVER['REQUEST_URI']);
    return $REQUEST_URI[0];
}

function getParamString($addParams = array()) 
{
    $parmas = "";
    $REQUEST_URI = explode("?", $_SERVER['REQUEST_URI']);
    if (isset($REQUEST_URI[1])) {
        $parmas = $REQUEST_URI[1];
    }
    if (!empty($addParams)) {
        if ($parmas) {
            $parmasArr = explode("&", $parmas);
            foreach($addParams as $key => $value) {
                $is = false;
                foreach ($parmasArr as $idx => $param) {
                    $paramArr = explode("=", $param);
                    if ($paramArr[0] == $key) {
                        $is = true;
                        $parmasArr[$idx] = $key.'='.$value;
                        break;
                    }
                }
                if (!$is) {
                    $parmasArr[] = $key.'='.$value;
                }
            }
            $parmas = implode("&", $parmasArr);
        } else {
            $parmasArr = array();
            foreach ($addParams as $key => $value) {
                $parmasArr[] = $key.'='.$value;
            }
            $parmas = implode("&", $parmasArr);
        }
    }
    return $parmas ? "?" . $parmas : "";
}