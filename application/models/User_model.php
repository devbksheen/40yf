<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * 유저 모델
 */
class User_model extends MY_Model
{

    public $table = 'user';
    public $primaryKey = 'user_id';
    public $jsonDataKey = array();

    public function __construct()
    {   
        parent::__construct();
    }

}