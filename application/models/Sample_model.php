<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * 모델 샘플
 */
class Sample_model extends MY_Model
{

    public $table = 'sample_tabe';
    public $primaryKey = 'sample_tabe_pk';
    public $jsonDataKey = array();

    public function __construct()
    {   
        parent::__construct();
    }

}