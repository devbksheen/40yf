<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * 설문 결과 모델
 */
class Survey_result_model extends MY_Model
{

    public $table = 'survey_result';
    public $primaryKey = 'svr_id';
    public $jsonDataKey = array();

    public function __construct()
    {   
        parent::__construct();
    }

}