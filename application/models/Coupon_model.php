<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * 쿠폰 모델
 */
class Coupon_model extends MY_Model
{

    public $table = 'coupon';
    public $primaryKey = 'coupon_id';
    public $jsonDataKey = array();

    public function __construct()
    {   
        parent::__construct();
    }

}