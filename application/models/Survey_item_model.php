<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * 설문 항목 모델
 */
class Survey_item_model extends MY_Model
{

    public $table = 'survey_item';
    public $primaryKey = 'svi_id';
    public $jsonDataKey = array();

    public function __construct()
    {   
        parent::__construct();
    }

}