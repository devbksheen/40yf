<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * 기관 사용자 모델
 */
class Organ_user_model extends MY_Model
{

    public $table = 'organ_user';
    public $primaryKey = 'orguser_id';
    public $jsonDataKey = array();

    public function __construct()
    {   
        parent::__construct();
    }

}