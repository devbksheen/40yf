<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 검사 관련 라이브러리
 */
class Survey_lib {
    
    private $CI;

    private $accessSurveyType = array('mind-ab', 'mindle');

    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->model(array('survey_result_model', 'survey_item_model'));
    }

    /**
     * 검사 항목 목록
     */
    public function getQuestionList($type)
    {
        if (!in_array($type, $this->accessSurveyType)) return false;
        $jsonData = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/application/json/'.$type.'.json'), true);
        return $jsonData;
    }

    /**
     * 검사 항목 타이틀
     */
    public function getQuestionTitle($type, $name)
    {
        if (!in_array($type, $this->accessSurveyType)) return false;
        $jsonData = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/application/json/'.$type.'.json'), true);
        foreach ($jsonData as $idx => $item) {
            if ($item['name'] == $name) {
                return $item['question'];
            }
        } 
    }

    /**
     * 검사 등록
     */
    public function submit($type, $post)
    {
        if (empty($type) || empty($post)) return false;
        if (!in_array($type, $this->accessSurveyType)) return false;
        if (isset($post['type'])) unset($post['type']);
        if (isset($post['step'])) unset($post['step']);

        // 검사 항목 데이터
        $items = array();
        // 검사 결과 데이터
        $result = array(
            'user_id' => $this->CI->login_lib->info('user_id'), 
            'orguser_id' => $this->CI->login_lib->info('orguser_id'), 
            'svr_type' => $type, 
            'svr_score' => 0
        );

        // 검사 항목 데이터, 검사 결과 데이터 추출
        foreach ($post as $key => $score) {
            $score = preg_replace("/[^0-9]*/s", "", $score);
            $items[] = array(
                'svi_question' => $this->getQuestionTitle($type, $key),
                'svi_key' => $key,
                'svi_value' => $score
            );
            $result['svr_score'] += $score;
        }
        if (empty($items)) return false;

        // 결과 해석 분류
        $resultClassify = array();
        switch ($type) {
            case "mind-ab":
                $resultClassify = $this->mindAbResultClassify($result['svr_score']);
            break;
            case "mindle":
                $resultClassify = $this->mindleResultClassify($post);
            break;
        }
        if (empty($resultClassify)) return false;
        $result = array_merge($result, $resultClassify);

        // INSERT 트랜잭션 시작
        $this->CI->db->trans_start();
        $svr_id = $this->CI->survey_result_model->update($result);
        foreach ($items as $idx => $item) {
            $item['svr_id'] = $svr_id;
            $this->CI->survey_item_model->update($item);
        }
        $this->CI->db->trans_complete();
        // INSERT 트랜잭션 끝

        if ($this->CI->db->trans_status() === false) {
            return false;
        } else {
            return $svr_id;
        }
    }

    /* ========================= mindAb ========================= */

    /**
     * mindAb 결과 해석 분류
     */
    public function mindAbResultClassify($score)
    {
        if ($score <= 15) {
            return array(
                'svr_classify' => 'A',
                'svr_classify_txt' => '우울 아님'
            );
        } else if ($score >= 16 && $score <= 20) {
            return array(
                'svr_classify' => 'B',
                'svr_classify_txt' => '경미한 우울'
            );
        } else if ($score >= 21 && $score <= 24) {
            return array(
                'svr_classify' => 'C',
                'svr_classify_txt' => '중증 우울'
            );
        } else if ($score >= 25) {
            return array(
                'svr_classify' => 'D',
                'svr_classify_txt' => '심한 우울'
            );
        } else {
            return array();
        }
    }

    /* ========================= mindAb ========================= */


    /* ========================= mindle ========================= */

    /**
     * mindle 검사 항목 다이아그램
     */
    public function getMindleQuestionDiagram($name)
    {
        $jsonData = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/application/json/mindle.json'), true);
        foreach ($jsonData as $idx => $item) {
            if ($item['name'] == $name) {
                return $item['diagram'];
            }
        } 
    }

    /**
     * mindle 캐릭터
     */
    public function getCharacter($value)
    {
        $character = array(
            'A' => '엄격이',
            'B' => '물렁이',
            'C' => '고독이',
            'D' => '콩콩이',
            'E' => '버럭이'
        );
        return $character[$value];
    }

    /**
     * mindle 결과 해석 분류
     */
    public function mindleResultClassify($post, $inclusionScore = false)
    {
        $score = array();
        foreach ($post as $key => $value) {
            $valueArr = explode(':', $value);
            if (!isset($score[$valueArr[0]])) $score[$valueArr[0]] = 0;
            $score[$valueArr[0]] += (int)$valueArr[1];
        }

        /* 
            ※ 항목 별 도식 구분값
            A : 유기
            B : 사회적 소외
            C : 의존/무능감
            D : 비관주의
            E : 융합
            F : 버럭이
            G : 자기희생
            H : 엄격한 기준
            I : 처벌
            J : 사회불안
            K : 정서적 결핍 
        */
        
        $svr_classify = array();
        $svr_classify_txt = array();
        $scoreArr = array();

        // 엄격한 기준 점수
        $scoreH = isset($score['H']) ? round($score['H'] / 2.5, 1) : 0;
        // 엄격이 점수
        $scoreArr['A'] = $scoreH;
        // 엄격한 기준 점수 >= 6.8 = 엄격이 추가
        if ($scoreH >= 6.8) {
            $svr_classify[] = 'A';
            $svr_classify_txt[] = $this->getCharacter('A');
        }

        // 자기희생 점수
        $scoreG = isset($score['G']) ? round($score['G'] / 2.5, 1) : 0;
        // 물렁이 점수
        $scoreArr['B'] = $scoreG;
        // 자기희생 점수 >= 6.8 = 물렁이 추가
        if ($scoreG >= 6.8) {
            $svr_classify[] = 'B';
            $svr_classify_txt[] = $this->getCharacter('B');
        }

        // 사회적 소외 점수
        $scoreB = isset($score['B']) ? round($score['B'] / 2.5, 1) : 0;
        // 사회불안 점수
        $scoreJ = isset($score['J']) ? round($score['J'] / 2, 1) : 0;
        // 고독이 점수
        $scoreArr['C'] = $scoreB >= $scoreJ ? $scoreB : $scoreJ;
        // 사회적 소외 점수, 사회불안 점수 둘중에 큰 수 >= 6.8 = 고독이 추가
        if (($scoreB >= $scoreJ && $scoreB >= 6.8) || ($scoreB <= $scoreJ && $scoreJ >= 6.8)) {
            $svr_classify[] = 'C';
            $svr_classify_txt[] = $this->getCharacter('C');
        }

        // 의존/무능감 점수
        $scoreC = isset($score['C']) ? round($score['C'] / 2.5, 1) : 0;
        // 비관주의 점수
        $scoreD = isset($score['D']) ? round($score['D'] / 2.5, 1) : 0;
        // 콩콩이 점수
        $scoreArr['D'] = $scoreC >= $scoreD ? $scoreC : $scoreD;
        // 의존/무능감 점수, 비관주의 점수 둘중에 큰 수 >= 6.8 = 콩콩이 추가
        if (($scoreC >= $scoreD && $scoreC >= 6.8) || ($scoreC <= $scoreD && $scoreD >= 6.8)) {
            $svr_classify[] = 'D';
            $svr_classify_txt[] = $this->getCharacter('D');
        }

        // 버럭이 점수
        $scoreF = isset($score['F']) ? round($score['F'] / 3, 1) : 0;
        // 버럭이 점수
        $scoreArr['E'] = $scoreF;
        // 버럭이 점수 >= 6.8 = 물렁이 추가
        if ($scoreF >= 6.8) {
            $svr_classify[] = 'E';
            $svr_classify_txt[] = $this->getCharacter('E');
        }

        if ($inclusionScore) {
            return array(
                'scoreArr' => $scoreArr,
                'svr_classify' => implode(',', $svr_classify),
                'svr_classify_txt' => implode(',', $svr_classify_txt)
            ); 
        } else {
            return array(
                'svr_classify' => implode(',', $svr_classify),
                'svr_classify_txt' => implode(',', $svr_classify_txt)
            ); 
        }
    }

    /**
     * 결과 별 항목
     */
    public function getItemListBySvrId($SvrId)
    {
        return $this->CI->survey_item_model->getList(array('where' => array('svr_id' => $SvrId)));
    }

    /**
     * 계정 별 검사 결과
     */
    public function getResultByUserIdAndType($userId, $type)
    {
        return $this->CI->survey_result_model->getInfo(array('where' => array('user_id' => $userId, 'svr_type' => $type)));
    }

    /**
     * 검사 결과 수정
     */
    public function resultUpdateById($id, $data)
    {
        return $this->CI->survey_result_model->updateById($id, $data);
    }

    /* ========================= mindle ========================= */



}