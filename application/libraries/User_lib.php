<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 회원 관련 라이브러리
 */
class User_lib {
    
    private $CI;

    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->model(array('user_model'));
    }

    /**
     * 회원
     */
    public function getUserById($id)
    {
        return $this->CI->user_model->getInfoById($id);
    }

    /**
     * 아이디 중복확인
     */
    public function overlabLoginIdCheck($loginid)
    {
        $count = $this->CI->user_model->getCount(array('where' => array('user_loginid' => trim($loginid))));
        return $count;
    }

    /**
     * 회원가입
     */
    public function join($post)
    {
        if (isset($post['user_password_check'])) unset($post['user_password_check']);
        if (isset($post['user_birth']) && is_array($post['user_birth'])) $post['user_birth'] = implode('-', $post['user_birth']);
        if (isset($post['user_phone']) && is_array($post['user_phone'])) $post['user_phone'] = implode('', $post['user_phone']);

        if (isset($post['user_password']) && $post['user_password']) 
            $post['user_password'] = password_hash($post['user_password'], PASSWORD_DEFAULT);
        
        $rs = $this->CI->user_model->update($post);

        return $rs;
    }

    /**
     * 회원정보 수정
     */
    public function userUpdate($id, $data)
    {
        if (isset($data['user_birth']) && is_array($data['user_birth'])) $data['user_birth'] = implode('-', $data['user_birth']);
        if (isset($data['user_phone']) && is_array($data['user_phone'])) $data['user_phone'] = implode('', $data['user_phone']);
        if (isset($data['user_password']) && $data['user_password']) $data['user_password'] = password_hash($data['user_password'], PASSWORD_DEFAULT);
        return $this->CI->user_model->updateById($id, $data);
    }

}