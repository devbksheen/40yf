<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 쿠폰 관련 라이브러리 
 */
class Coupon_lib {
    
    private $CI;

    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->model(array('coupon_model'));
    }

    public function freeCouponRegister($couponCode)
    {
        $data = array(
            'coupon_code' => $couponCode,
            'coupon_user_id' => $this->CI->login_lib->info('user_id')
        );
        return $this->CI->coupon_model->update($data);
    }

}