<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 비디오 관련 라이브러리
 */
class Video_lib {
    
    private $CI;

    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->model(array());
    }

    /**
     * Mind-Ab 영상 목록
     */
    public function getMindAbVideoList()
    {
        $jsonData = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/application/json/mind-ab-video.json'), true);
        return $jsonData;
    }

    /**
     * Mindle 영상 목록
     */
    public function getMindleVideoList($type)
    {
        $jsonData = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/application/json/mindle-'.$type.'-video.json'), true);
        return $jsonData;
    }

}