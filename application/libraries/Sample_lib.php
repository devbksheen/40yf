<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 라이브러리 샘플
 */
class Sample_lib {
    
    private $CI;

    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->model(array());
    }

}