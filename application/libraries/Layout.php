<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Layout {

    private $_head = true, $_header = true, $_breadcrumbs = true, $_nav = true, $_footer = true;
    public $head = null, $header = null, $breadcrumbs = null, $nav = null, $contents = null, $footer = null;
    public $title = null;
    public $addCss = array();
    public $addJs = array();

    function __construct()
    {   
        $this->CI =& get_instance();
    }

    // 사용자 레이아웃
    public function user($contentsUrl, $data = array()) {
        
        $data['layout'] = $this;

        if ($this->title == null) {
            $this->title = config_item('default_user_title');
        }
        if ($this->_head) {
            $this->setImp('head', 'user/head', $data);
        }
        if ($this->_nav) {
            $jsonData = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/application/json/userNav.json'), true);
            $data['_nav'] = $jsonData;
            $this->setImp('nav', 'user/nav', $data);
        }
        if ($this->_header) {
            $this->setImp('header', 'user/header', $data);
        }
        if ($this->_breadcrumbs) {
            $this->setImp('breadcrumbs', 'user/breadcrumbs', $data);
        }
        if ($this->_footer) {
            $this->setImp('footer', 'user/footer', $data);
        }

        $this->contents = $this->CI->load->view($contentsUrl, $data, true);

        $this->CI->load->view('imp/user/layout', $data);
    }

    // 사용자 팝업 레이아웃
    public function userPopup ($contentsUrl, $data = array()) {
        
        $data['layout'] = $this;

        if ($this->title == null) {
            $this->title = config_item('default_user_title');
        }
        if ($this->_head) {
            $this->setImp('head', 'user/popupHead', $data);
        }
        if ($this->_header) {
            $this->setImp('header', 'user/popupHeader', $data);
        }

        $this->contents = $this->CI->load->view($contentsUrl, $data, true);
        
        $this->CI->load->view('imp/user/popupLayout', $data);
    }
    
    // 관리자 레이아웃
    public function admin($contentsUrl, $data = array()) {
        
        $data['layout'] = $this;
        
        
        if ($this->title == null) {
            $this->title = config_item('default_admin_title');
        }
        if ($this->_head) {
            $this->setImp('head', 'admin/head', $data);
        }
        if ($this->_nav) {
            $jsonData = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/application/json/adminNav.json'), true);
            $data['_nav'] = $jsonData;
            $this->setImp('nav', 'admin/nav', $data);
        }
        if ($this->_header) {
            $this->setImp('header', 'admin/header', $data);
        }
        if ($this->_footer) {
            $this->setImp('footer', 'admin/footer', $data);
        }

        $this->contents = $this->CI->load->view($contentsUrl, $data, true);

        $this->CI->load->view('imp/admin/layout', $data);
    }

    // 관리자 팝업 레이아웃
    public function adminPopup ($contentsUrl, $data = array()) {
        
        $data['layout'] = $this;

        $this->contents = $this->CI->load->view($contentsUrl, $data, true);
        
        if ($this->title == null) {
            $this->title = config_item('default_admin_title');
        }
        if ($this->_head) {
            $this->setImp('head', 'admin/popupHead', $data);
        }
        if ($this->_header) {
            $this->setImp('header', 'admin/popupHeader', $data);
        }
        
        $this->CI->load->view('imp/admin/popupLayout', $data);
    }


    // Css 추가
    public function addCss($cssUrl) {
        $this->addCss[] = $cssUrl;
    }

    // Js 추가
    public function addJs($jsUrl) {
        $this->addJs[] = $jsUrl;
    }

    // Imp 설정
    public function setImp($var, $impName, $data = array())
    {
        if ($impName) {
            $this->{'_'.$var} = true;
            $this->{$var} = $this->CI->load->view('imp/' . $impName, $data, true);
        } else {
            $this->{'_'.$var} = false;
            $this->{$var} = null;
        }
    }
    
}