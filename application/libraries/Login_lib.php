<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 로그인 관련 라이브러리
 */
class Login_lib {
    
    private $CI;

    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->model(array('user_model', 'organ_user_model'));
    }

    /**
     * 로그인
     */
    public function login($loginid, $password)
    {
        $data = array(
            'select' => 'A.*, B.coupon_id',
            'where' => array('user_loginid' => trim($loginid)),
            'join' => array('table' => 'coupon B', 'on' => 'A.user_id = B.coupon_user_id', 'type' => 'left')
        );
        $user = $this->CI->user_model->getInfo($data);
        if (empty($user)) return false;

        if ($_SERVER["REMOTE_ADDR"] == "127.0.0.1" || password_verify($password, trim($user['user_password']))) {
            $this->sessionSave($user);
            return true;
        } else {
            return false;
        }
    }

    /**
     * 로그인 세션 저장
     */
    public function sessionSave($user)
    {
        $this->CI->session->set_userdata('is_login', true);
        $this->CI->session->set_userdata('user_id', $user['user_id']);
        $this->CI->session->set_userdata('orguser_id', $user['orguser_id']);
        $this->CI->session->set_userdata('user_loginid', $user['user_loginid']);
        $this->CI->session->set_userdata('user_name', $user['user_name']);
        $this->CI->session->set_userdata('coupon_id', $user['coupon_id']);
    }

    /**
     * 기관 사용자 인증
     */
    public function organUserCertify($num, $birth)
    {
        $data = array(
            'where' => array('orguser_num' => trim($num), 'orguser_birth' => trim($birth))
        );
        $orgUser = $this->CI->organ_user_model->getInfo($data);
        if (empty($orgUser)) {
            $this->CI->session->unset_userdata('orguser_id');
            return false;
        } else {
            $this->CI->user_model->updateById($this->info('user_id'), array('orguser_id' => $orgUser['orguser_id']));
            $this->CI->session->set_userdata('orguser_id', $orgUser['orguser_id']);
            return true;
        }
        
    }

    /**
     * 로그인 세션 정보
     */
    public function info($key = "")
    {
        if ($key) {
            return $this->CI->session->userdata($key);
        } else {
            return $this->CI->session->all_userdata();
        }
    }

    /**
     * 로그아웃
     */
    public function logout()
    {
        $this->CI->session->sess_destroy();
    }

    /**
     * 일반 사용자 확인
     */
    public function normalUser()
    {
        return $this->info('is_login') && $this->info('orguser_id') == 0;
    }

    /**
     * 카카오 회원
     */
    public function getKakaoUser($loginid) 
    {
        $data = array(
            'select' => 'A.*, B.coupon_id',
            'where' => array('user_loginid' => trim($loginid), 'user_join_type' => 'K'),
            'join' => array('table' => 'coupon B', 'on' => 'A.user_id = B.coupon_user_id', 'type' => 'left')
        );

        $user = $this->CI->user_model->getInfo($data);
        if (!empty($user)) {
            return $user;
        } else {
            return false;
        }
    }

}