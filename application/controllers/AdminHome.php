<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminHome extends CI_Controller {

	private $data = array();

	function __construct()
    {      
        parent::__construct();
        $this->allow = array();
        $this->data = array();
        $this->load->library(array());
	}
	
	public function index()
	{
		$this->layout->admin('admin/index', $this->data);
	}

}
