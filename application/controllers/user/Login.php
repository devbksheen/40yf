<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	private $data = array();

	function __construct()
    {      
        parent::__construct();
        $this->allow = array('index', 'organFail', 'join', 'kakao');
        $this->data = array();
        $this->load->library(array('user_lib', 'coupon_lib'));
	}
	
	public function index()
	{
		$this->form_validation->set_rules('user_loginid', '아이디', 'required');
        $this->form_validation->set_rules('user_password', '비밀번호', 'required');
        if ($this->form_validation->run() ==! false) {
			$user_loginid = $this->input->post('user_loginid');
			$user_password = $this->input->post('user_password');
			$rs = $this->login_lib->login($user_loginid, $user_password);
			if ($rs) {
				if ($this->login_lib->info('orguser_id')) {
					redirect(base_url());
				} else {
					if ($this->login_lib->info('coupon_id')) {
						redirect(base_url('mindle'));
					} else {
						redirect(base_url('login/type'));
					}
				}
			} else {
				alert("아이디와 패스워드가 일치하지 않습니다.");
			}
		} else if ($_POST) {
			alert("아이디와 비밀번호를 입력해주세요.");
		} else {
			$this->layout->user('user/login/index', $this->data);
		}
	}

	public function type()
	{
		$this->form_validation->set_rules('coupon_code', '쿠폰번호', 'required');
        if ($this->form_validation->run() ==! false) {

			if ($this->login_lib->info('coupon_id')) alert("이미 쿠폰을 등록하셨습니다.");

			$couponCodeArr = array(
				'mindfree1', 
				'mindlesg', 
				'mindle1', 
				'mindle2', 
				'mindle3', 
				'mindle4', 
				'mindle5', 
				'mindle6', 
				'mindle7', 
				'mindle8', 
				'mindle9'
			);
			$couponCode = $this->input->post('coupon_code');

			if (!in_array($couponCode, $couponCodeArr)) {
				alert("잘못된 쿠폰 번호 입니다.");
			} 

			$rs = $this->coupon_lib->freeCouponRegister($couponCode);
			if ($rs) {
				$this->session->set_userdata('coupon_id', $rs);

				alert("쿠폰이 등록되었습니다.", base_url('mindscan/mindle'));
			} else {
				alert("쿠폰 등록에 실패하였습니다.");
			}

		} else {
			$this->layout->user('user/login/type', $this->data);
		}
	}

	public function organ()
	{
		$this->form_validation->set_rules('orguser_num', '사번', 'required');
        $this->form_validation->set_rules('orguser_birth', '생년월일', 'required');
        if ($this->form_validation->run() ==! false) {
			$orguser_num = $this->input->post('orguser_num');
			$orguser_birth = $this->input->post('orguser_birth');
			$rs = $this->login_lib->organUserCertify($orguser_num, $orguser_birth);
			if ($rs) {
				redirect(base_url('login/organSuccess'));
			} else {
				redirect(base_url('login/organFail'));
			}
		} else if ($_POST) {
			alert("사번과 생년월일을 입력해주세요.");
		} else {
			$this->layout->user('user/login/organ', $this->data);
		}
	}

	public function organSuccess()
	{
		$this->layout->user('user/login/organSuccess', $this->data);
	}

	public function organFail()
	{
		$this->layout->user('user/login/organFail', $this->data);
	}

	public function logout()
	{
		$this->login_lib->logout();
		redirect(base_url('login'));
	}

	public function join()
	{
		$this->form_validation->set_rules('user_loginid', '아이디', 'required');
        $this->form_validation->set_rules('user_password', '비밀번호', 'required');
        $this->form_validation->set_rules('user_password_check', '비밀번호 확인', 'required');
        $this->form_validation->set_rules('user_name', '이름', 'required');
        $this->form_validation->set_rules('user_gender', '성별', 'required');
        $this->form_validation->set_rules('user_birth[]', '생년월일', 'required');
        $this->form_validation->set_rules('user_phone[]', '휴대전화', 'required');
        $this->form_validation->set_rules('user_email', '이메일', 'required');
        $this->form_validation->set_rules('user_use_agree', '이용약관', 'required');
        $this->form_validation->set_rules('user_info_agree', '개인정보 수집', 'required');
        if ($this->form_validation->run() ==! false) {
			$post = $this->input->post();

			if ($this->user_lib->overlabLoginIdCheck(trim($post['user_loginid']))) {
				alert('이미 사용중인 아이디 입니다.\n다른 아이디를 입력해주세요.');
			}

			if (trim($post['user_password']) != trim($post['user_password_check'])) {
				alert('비밀번호와 비밀번호 확인이 일치하지 않습니다.');
			}

			$rs = $this->user_lib->join($post);

			if ($rs) {
				alert("회원가입이 완료되었습니다.", base_url('login'));
			} else {
				alert("회원가입에 실패하였습니다.");
			}

		} else {
			$this->layout->user('user/login/join', $this->data);
		}
	}

	public function kakao()
	{
		$this->form_validation->set_rules('user_loginid', '아이디', 'required');
        if ($this->form_validation->run() ==! false) {
			$post = $this->input->post();

			$user = $this->login_lib->getKakaoUser($post['user_loginid']);
			
			if (!empty($user)) {
				$this->login_lib->sessionSave($user);
				echo json_encode($user);
			} else {
				$post['user_phone'] = str_replace('+82 ', '0', $post['user_phone']);
				if ($post['user_birth']) {
					$post['user_birth'] = substr($post['user_birth'], 0, 4).'-'.substr($post['user_birth'], 4, 2).'-'.substr($post['user_birth'], 6, 2);
				} else {
					unset($post['user_birth']);
				}
				if (!$post['user_gender']) unset($post['user_gender']);
				$post['user_join_type'] = 'K';

				$rs = $this->user_lib->join($post);

				if ($rs) {
					$user = $this->login_lib->getKakaoUser($post['user_loginid']);
					$this->login_lib->sessionSave($user);
					echo json_encode($user);
				} else {
					echo json_encode(false);
				}
			}

		}
	}

}
