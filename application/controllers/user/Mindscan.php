<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mindscan extends CI_Controller {

	private $data = array();

	function __construct()
    {      
        parent::__construct();
        $this->allow = array('index', 'mindab', 'mindle');
        $this->data = array();
        $this->load->library(array('survey_lib', 'user_lib'));
    }
    
    public function index()
    {
        // 일반 사용자는 mindle 검사 페이지로 이동
        if ($this->login_lib->normalUser()) redirect(base_url('mindscan/mindle'));
		$this->layout->user('user/mindscan/index', $this->data);
    }

    public function mindab()
    {
        // 일반 사용자는 접근 불가
        if ($this->login_lib->normalUser()) alert();

        // Mind-ab 검사 결과가 있으면 결과 페이지로 이동
        if ($this->login_lib->info('is_login')) {
            $userId = $this->login_lib->info('user_id');
            $result = $this->survey_lib->getResultByUserIdAndType($userId, 'mind-ab');
            if (!empty($result)) {
                alert('이미 검사를 진행하셨습니다.\n검사결과 페이지로 이동합니다.',  base_url('mindscan/mindabResult'));
            }
        } else {
            alert("로그인 후 검사 가능합니다.", base_url('login'));
        }

        $this->form_validation->set_rules('type', '검사 유형', 'required');
        $question = $this->survey_lib->getQuestionList('mind-ab');
        foreach ($question as $idx => $item) { 
            $this->form_validation->set_rules($item['name'], $idx.'번 항목', 'required');
        }
        if ($this->form_validation->run() ==! false) {
            $post = $this->input->post();
            $rs = $this->survey_lib->submit($post['type'], $post);
            if ($rs) {
                redirect('mindscan/mindabResult');
            } else {
                alert("시스템 오류가 발생하였습니다. [code: mindscan-45]\n해당 오류가 지속될 경우 홈페이지 하단에 문의처로 문의바랍니다.");
            }
        } else if ($_POST) {
            alert("모든 문항에 답변을 체크해주세요.");
        } else {
            $this->data['question'] = $question;
            $this->layout->user('user/mindscan/mindab', $this->data);
        }
    }

    public function mindabResult()
    {
        // 일반 사용자는 접근 불가
        if ($this->login_lib->normalUser()) alert();

        $userId = $this->login_lib->info('user_id');
        $result = $this->survey_lib->getResultByUserIdAndType($userId, 'mind-ab');
        if (empty($result)) alert("잘못된 접근입니다.");

        $this->data['result'] = $result;
        $this->layout->user('user/mindscan/mindabResult', $this->data);
    }

    public function mindle()
    {
        if ($this->login_lib->normalUser() && !$this->login_lib->info('coupon_id')) 
            alert("인증 후 이용가능합니다.", base_url('login/type'));

        // Mindle 검사 결과가 있으면 결과 페이지로 이동
        if ($this->login_lib->info('is_login')) {
            $userId = $this->login_lib->info('user_id');
            $result = $this->survey_lib->getResultByUserIdAndType($userId, 'mindle');
            if (!empty($result)) {
                alert('이미 검사를 진행하셨습니다.\n검사결과 페이지로 이동합니다.',  base_url('mindscan/mindleResult'));
            }
        } else {
            alert("로그인 후 검사 가능합니다.", base_url('login'));
        }

        $question = $this->survey_lib->getQuestionList('mindle');
        $questionStep = array_chunk($question, (count($question) / 2));

        $this->form_validation->set_rules('type', '검사 유형', 'required');
        $this->form_validation->set_rules('step', '검사 단계', 'required');
        if ($_POST && $_POST['step'] == 1) {
            foreach ($questionStep[0] as $idx => $item) { 
                $this->form_validation->set_rules($item['name'], $idx.'번 항목', 'required');
            }
        } else if ($_POST && $_POST['step'] == 2) {
            foreach ($question as $idx => $item) { 
                $this->form_validation->set_rules($item['name'], $idx.'번 항목', 'required');
            }
        }
        if ($this->form_validation->run() ==! false) {
            $post = $this->input->post();
            
            if ($post['step'] == 1) {
                unset($post['step']);
                unset($post['type']);
                $this->data['post'] = $post;
                $this->data['step'] = 2;
                $this->data['question'] = $questionStep[1];
                $this->data['buttonText'] = '결과 보러가기';
                $this->layout->user('user/mindscan/mindle', $this->data);
            } else if ($post['step'] == 2) {
                $rs = $this->survey_lib->submit($post['type'], $post);
                if ($rs) {
                    redirect('mindscan/mindleResult');
                } else {
                    alert("시스템 오류가 발생하였습니다. [code: mindscan-108]\n해당 오류가 지속될 경우 홈페이지 하단에 문의처로 문의바랍니다.");
                }
            } else {
                alert();
            }

        } else if ($_POST) {
            alert("모든 문항에 답변을 체크해주세요.");
        } else {
            $this->data['step'] = 1;
            $this->data['question'] = $questionStep[0];
            $this->data['buttonText'] = '다음 문항보기';
            $this->layout->user('user/mindscan/mindle', $this->data);
        }
    }

    public function mindleResult()
    {        
        $loginUserInfo = $this->user_lib->getUserById($this->login_lib->info('user_id'));
        if (
            !trim($loginUserInfo['user_name']) || 
            !trim($loginUserInfo['user_gender']) || 
            !trim($loginUserInfo['user_birth']) || 
            (trim($loginUserInfo['user_birth']) && strlen(trim($loginUserInfo['user_birth'])) != 10)
        ) { // 이름, 성별, 생년월일이 정상적으로 등록되지 않았을때 추가정보입력
            redirect(base_url('mindscan/addInfo'));
        }

        $this->form_validation->set_rules('svr_id', '검사결과 고유번호', 'required');
        $this->form_validation->set_rules('svr_classify_txt', '악동이야기', 'required');
        if ($this->form_validation->run() ==! false) {
            $post = $this->input->post();

            $rs = $this->survey_lib->resultUpdateById($post['svr_id'], array('svr_classify_txt' => $post['svr_classify_txt']));
            if ($rs) {
                alert("내 악동이야기가 등록되었습니다.", base_url('mindscan/mindleResult'));
            } else {
                alert("내 악동이야기 등록에 실패하였습니다.");
            }

        } else if ($_POST) {
            alert("내 악동이야기를 입력해주세요.");
        } else {
            $userId = $this->login_lib->info('user_id');
            $result = $this->survey_lib->getResultByUserIdAndType($userId, 'mindle');
            if (empty($result)) alert("잘못된 접근입니다.");
    
            $result['svr_classify'] = $result['svr_classify'] ? explode(',', $result['svr_classify']) : array();
            $result['svr_classify_txt'] = $result['svr_classify_txt'] ? explode(',', $result['svr_classify_txt']) : array();
            $this->data['result'] = $result;
            
            $items = $this->survey_lib->getItemListBySvrId($result['svr_id']);
            if (!empty($items)) {
                $post = array();
                foreach ($items as $idx => $item) {
                    $diagram = $this->survey_lib->getMindleQuestionDiagram($item['svi_key']);
                    $post[$item['svi_key']] = $diagram.':'.$item['svi_value'];
                }
                
                $resultClassify = $this->survey_lib->mindleResultClassify($post, true);
                $resultClassify['svr_classify'] = $resultClassify['svr_classify'] ? explode(',', $resultClassify['svr_classify']) : array();
                $resultClassify['svr_classify_txt'] = $resultClassify['svr_classify_txt'] ? explode(',', $resultClassify['svr_classify_txt']) : array();
                $this->data['result'] = $result = array_merge($result, $resultClassify);
            }

            $this->layout->addJs(base_url('plugin/bar-chart-jqp-graph/distrib/jquery.dvstr_jqp_graph.js'));
            $this->layout->addCss(base_url('plugin/bar-chart-jqp-graph/distrib/jquery.dvstr_jqp_graph.css'));

            if (count($result['svr_classify']) == 0) { // 캐릭터 결과가 없을때
                $this->layout->user('user/mindscan/mindleResultNull', $this->data);
            // } else if (count($result['svr_classify']) == 1) { // 캐릭터 결과가 1개일때 상세보기 페이지
                // if ($this->input->get('type') == 'all') { // 상세보기 페이지
                //     $this->layout->user('user/mindscan/mindleResults2', $this->data);
                // } else {
                //     $this->data['type'] = $result['svr_classify'][0];
                //     $this->data['character'] = $result['svr_classify_txt'][0];
                //     $this->data['listBtn'] = false;
                //     $this->layout->user('user/mindscan/mindleResult', $this->data);
                // }
            } else if (count($result['svr_classify']) > 0) { // 캐릭터 결과가 여러개일때
            // } else if (count($result['svr_classify']) > 1) { // 캐릭터 결과가 여러개일때
                if ($this->input->get('type') && $this->input->get('type') != 'all') { // 상세보기 페이지
                    $this->data['type'] = trim($this->input->get('type'));
                    if (!in_array($this->data['type'], $result['svr_classify'])) alert();
                    $this->data['character'] = $this->survey_lib->getCharacter($this->data['type']);
                    $this->data['listBtn'] = true;
                    $this->layout->user('user/mindscan/mindleResult', $this->data);
                } else {
                    $this->layout->user('user/mindscan/mindleResults2', $this->data);
                }
            } else {
                alert();
            }
        }
    }

    public function addInfo()
    {
        $this->form_validation->set_rules('user_name', '이름', 'required');
        $this->form_validation->set_rules('user_gender', '성별', 'required');
        $this->form_validation->set_rules('user_birth[]', '생년월일', 'required');
        if ($this->form_validation->run() ==! false) {
            $post = $this->input->post();
            $rs = $this->user_lib->userUpdate($this->login_lib->info('user_id'), $post);
            if ($rs) {
                redirect(base_url('mindscan/mindleResult'));
            } else {
                alert("추가정보 등록에 실패하였습니다.");
            }
        } else {
            $user = $this->user_lib->getUserById($this->login_lib->info('user_id'));
            if (trim($user['user_birth'])) {
                $user['user_birth'] = explode('-', $user['user_birth']);
            } else {
                $user['user_birth'] = array();
            }
            $this->data['user'] = $user;
            $this->layout->user('user/mindscan/addInfo', $this->data);
        }
    }

}