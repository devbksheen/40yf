<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mindle extends CI_Controller {

	private $data = array();

	function __construct()
    {      
        parent::__construct();
        $this->allow = array('index');
        $this->data = array();
        $this->load->library(array('video_lib', 'survey_lib'));
    }
    
    public function index()
    {
        // Mindle 검사 결과가 있으면 캐릭터 목록 페이지로 이동
        if ($this->login_lib->info('is_login')) {
            $userId = $this->login_lib->info('user_id');
            $result = $this->survey_lib->getResultByUserIdAndType($userId, 'mindle');
            if (!empty($result)) {
                redirect(base_url('mindle/type'));
            }
        }
		$this->layout->user('user/mindle/index', $this->data);
    }

    public function type()
    {
        if ($this->login_lib->normalUser() && !$this->login_lib->info('coupon_id')) 
            alert("무료체험 신청 및 쿠폰 등록 후 이용가능합니다", base_url('login/type'));
            
		$this->layout->user('user/mindle/type', $this->data);
    }
    
    public function list()
    {
        if ($this->login_lib->normalUser() && !$this->login_lib->info('coupon_id')) 
            alert("무료체험 신청 및 쿠폰 등록 후 이용가능합니다", base_url('login/type'));
        
        $type = $this->input->get('type');
        if (!$type) alert();

        $this->data['type'] = $type;
        $this->data['videos'] = $this->video_lib->getMindleVideoList($type);
        $this->layout->user('user/mindle/list', $this->data);
    }

    public function view()
    {
        $id = $this->data['id'] = $this->input->get('id');
        $type = $this->data['type'] = $this->uri->segment(3);
        $character = $this->data['character'] = $this->survey_lib->getCharacter($type);
        $videos = $this->video_lib->getMindleVideoList($type);
        
        $video = false;
        $nextVideo = false;
        foreach ($videos as $idx => $item) {
            if ($item['id'] == $id) {
                $video = $item;
                if (isset($videos[$idx+1])) {
                    $nextVideo = $videos[$idx+1];
                }
            }
        }
        if (!$video) alert("해당 영상정보가 없습니다.");

        $this->data['video'] = $video;
        $this->data['nextVideo'] = $nextVideo;

        $this->layout->user('user/mindle/view', $this->data);
    }
    
}