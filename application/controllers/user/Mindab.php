<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mindab extends CI_Controller {

	private $data = array();

	function __construct()
    {      
        parent::__construct();
        $this->allow = array();
        $this->data = array();
        $this->load->library(array('survey_lib', 'video_lib'));

        // Mind-ab 검사 결과가 없으면 검사 페이지로 이동
        if ($this->login_lib->info('is_login')) {
            $userId = $this->login_lib->info('user_id');
            $result = $this->survey_lib->getResultByUserIdAndType($userId, 'mind-ab');
            if (empty($result)) {
                alert('Mind-Ab 검사 후 입장이 가능합니다.\nMind-Ab 검사 페이지로 이동합니다.',  base_url('mindscan/mindab'));
            }
        }
    }
    
    public function index()
    {   
        $this->data['videos'] = $this->video_lib->getMindAbVideoList();
		$this->layout->user('user/mindab/index', $this->data);
    }

    public function view()
    {
        $id = $this->uri->segment(3);
        $videos = $this->video_lib->getMindAbVideoList();
        
        $video = false;
        $nextVideo = false;
        foreach ($videos as $idx => $item) {
            if ($item['id'] == $id) {
                $video = $item;
                if (isset($videos[$idx+1])) {
                    $nextVideo = $videos[$idx+1];
                }
            }
        }
        if (!$video) alert("해당 영상정보가 없습니다.");

        $this->data['video'] = $video;
        $this->data['nextVideo'] = $nextVideo;

		$this->layout->user('user/mindab/view', $this->data);
    }

}