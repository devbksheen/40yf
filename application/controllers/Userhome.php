<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userhome extends CI_Controller {

	private $data = array();

	function __construct()
    {      
        parent::__construct();
        // $this->allow = array();
        $this->data = array();
        $this->load->library(array());
	}
	
	public function index()
	{
		redirect(base_url('mindle'));
		// $this->layout->user('user/index', $this->data);
	}

	public function personalInformation()
	{
		$this->layout->user('user/personalInformation', $this->data);
    }
    
    public function serviceAgreement()
	{
		$this->layout->user('user/serviceAgreement', $this->data);
	}

}
