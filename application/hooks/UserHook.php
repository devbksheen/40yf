<?php
class UserHook {
    function checkPermission()
    {
        $CI =& get_instance();
        if (isset($CI->allow) && (is_array($CI->allow) === false OR in_array($CI->router->method, $CI->allow) === false))
        {
            if (!$CI->session->userdata('is_login')) {
                redirect(base_url('login'));
            }
        }
    }
}
?>