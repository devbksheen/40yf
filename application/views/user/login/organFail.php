<?php echo $layout->breadcrumbs?>
<div class="row mb50 text-center">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/login-organ-text-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/login-organ-text-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 text-center mb30">
        <img src="<?php echo base_url('assets/user/img/organ-fail.png')?>" class="w100p" style="max-width: 506px"/>
    </div>
    <div class="col-lg-12 text-center mb40">
        <p class="text-color1">인증이 실패하였습니다.</p>
        <p>계속 인증에 실패할 경우, 대상자 명단에서 누락된 상태일 수 있습니다.</p>
        <p>카카오친구 '40FY'로 문의주세요.</p>
    </div>
    <div class="col-lg-12 text-center mb90">
        <button type="button" class="button1" onclick="location.href='<?php echo base_url('login/organ')?>'">재인증하기</button>
    </div>
</div>