<?php echo $layout->breadcrumbs?>
<div class="row mb50 text-center">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/login-organ-text-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/login-organ-text-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 text-center mb30">
        <img src="<?php echo base_url('assets/user/img/organ-success.png')?>"  class="w100p" style="max-width: 506px; border-radius: 15px; border: solid 1px #847fcb;"/>
    </div>
    <div class="col-lg-12 text-center mb40">
        <p class="text-color1">인증이 완료되었습니다.</p>
        <p>이제부터 서비스를 이용할 수 있습니다.</p>
    </div>
    <div class="col-lg-12 text-center mb90">
        <button type="button" class="button1" onclick="location.href='<?php echo base_url('mindscan')?>'">서비스 시작</button>
    </div>
</div>