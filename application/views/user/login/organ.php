<?php echo $layout->breadcrumbs?>
<div class="row mb30 text-center">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/login-organ-text-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/login-organ-text-banner.png')?>" class="w100p mobile"/>
    </div>
</div>
<div class="row pb140">
    <div class="col-lg-1">
    </div>
    <div class="col-lg-5">
        <div class="mt30 text-center">
            <img src="<?php echo base_url('assets/user/img/organ-user.png')?>"/>
        </div>
    </div>
    <div class="col-lg-5">
        <form method="POST">
            <div class="row form-group mt60">
                <div class="col-sm-4">
                    <label for="orguser_num">사번</label>
                </div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="orguser_num" name="orguser_num" maxlength="6">
                </div>
            </div>
            <div class="row form-group mt50 mb50">
                <div class="col-sm-4">
                    <label for="orguser_birth">생년월일</label>
                </div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="orguser_birth" name="orguser_birth" placeholder="YYMMDD" maxlength="6">
                </div>
            </div>
            <div class="row text-center">
                <button class="button1">사용자 인증</button>
            </div>
        </form>
    </div>
    <div class="col-lg-1">
    </div>
</div>