<div class="row pb140">
    <div class="col-lg-6 pt50 text-center">
        <p class="user-type text-color1">기관사용자</p>
        <div class="mt30">
            <img src="<?php echo base_url('assets/user/img/organ-user.png')?>"/>
        </div>
        <p class="mt30">소속기관을 통해 서비스를 이용하는<br/>사용자의 경우 인증이 필요합니다.</p>
        <button type="button" class="button1 mt30" onclick="location.href='<?php echo base_url('login/organ')?>'">사용자 인증</button>
    </div>
    <div class="col-lg-6 pt50 text-center">
        <p class="user-type text-color2">일반회원</p>
        <div class="mt30">
            <img src="<?php echo base_url('assets/user/img/normal-user.png')?>"/>
        </div>
        <p class="mt30">40FY와 함께 내 마음을 위한 시간!<br/>시작해볼까요?</p>
        <button type="button" class="button2 mt30" onclick="serviceStart()">서비스 시작</button>
    </div>
</div>

<div id="popup-area" style="display:none;">
    <div class="dim"></div>
    <div class="popup">
        <a class="close fr" href="javascript: $('#popup-area').hide();">Ⅹ</a>
        <p>일반회원 경우, <span class="text-color2">무료체험 신청 후 이용가능</span>합니다.</p>
        <p class="mb20">쿠폰을 보유하고 계시면, 등록 후 서비스를 이용할 수 있습니다.</p>
        <div class="text-center">
            <form id="couponForm" method="post">
                <input type="text" name="coupon_code" class="mb20" maxlength="10">
            </form>
        </div>
        <div>
            <button type="button" class="button3 fl" onclick="window.open('https:\/\/docs.google.com/forms/d/e/1FAIpQLScd6MflgDqKroiN81ZmNvoNNH4G6bXssxR8ZhIew0ztFxS_3A/viewform')">무료체험 신청하기</button>
            <button type="button" class="button2 fr" onclick="couponInsert()">쿠폰 등록하기</button>
        </div>
    </div>
</div>

<script>
    function serviceStart() {
        var coupon_id = '<?php echo $this->login_lib->info('coupon_id')?>';
        if (!coupon_id) {
            $("#popup-area").show();
        } else {
            location.href = '<?php echo base_url()?>';
        }
    } 

    function couponInsert()
    {
        var code = $.trim($('[name="coupon_code"]').val());
        if (!code) {
            alert("쿠폰 번호를 입력해주세요.");
            $('[name="coupon_code"]').focus();
            return false;
        }
        $("#couponForm").submit();
    }
</script>