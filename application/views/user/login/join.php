<?php echo $layout->breadcrumbs?>
<div class="row mb30 text-center">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/login-text-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/login-text-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<form id="userRegForm" method="POST">
    <div class="row join">
        <div class="col-lg-12 border-bottom">
            <p class="join-title mb20">▶ 기본정보</p>
            <div class="row form-group mb20">
                <div class="col-sm-2">
                    <label class="input-label" for="user_loginid">아이디</label>
                </div>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="user_loginid" name="user_loginid" maxlength="16">
                </div>
                <div class="col-sm-6">
                    <p class="join-desc">(영문소문자/숫자, 4~16자)</p>
                </div>
            </div>
            <div class="row form-group mb20">
                <div class="col-sm-2">
                    <label class="input-label" for="user_password">비밀번호</label>
                </div>
                <div class="col-sm-4">
                    <input type="password" class="form-control" id="user_password" name="user_password" maxlength="16">
                </div>
                <div class="col-sm-6">
                    <p class="join-desc">(영문대소문자/숫자/특수문자를 포함한 조합, 8~16자)</p>
                </div>
            </div>
            <div class="row form-group mb30">
                <div class="col-sm-2">
                    <label class="input-label" for="user_password_check">비밀번호 확인</label>
                </div>
                <div class="col-sm-4">
                    <input type="password" class="form-control" id="user_password_check" name="user_password_check" maxlength="16">
                </div>
                <div class="col-sm-6">
                </div>
            </div>
        </div>
        <div class="col-lg-12 mb30">
            <p class="join-title mt30 mb20">▶ 추가정보</p>
            <div class="row form-group mb20">
                <div class="col-sm-2">
                    <label class="input-label" for="user_name">이름</label>
                </div>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="user_name" name="user_name" maxlength="10">
                </div>
                <div class="col-sm-6">
                </div>
            </div>
            <div class="row form-group mb20">
                <div class="col-sm-2">
                    <label class="input-label" for="user_gender">성별</label>
                </div>
                <div class="col-sm-4 radio-group">
                    <input type="radio" id="user_gender_M" name="user_gender" value="M"/>
                    <label class="mr10" for="user_gender_M">남자</label>

                    <input type="radio" id="user_gender_W" name="user_gender" value="W"/>
                    <label for="user_gender_W">여자</label>
                </div>
                <div class="col-sm-6">
                </div>
            </div>
            <div class="row form-group mb30">
                <div class="col-sm-2">
                    <label class="input-label" for="user_birth">생년월일</label>
                </div>
                <div class="col-sm-5 birth-group">
                    <input type="text" name="user_birth[]" class="form-control birth-input1" onlynum maxlength="4" placeholder="YYYY"/> 년
                    <input type="text" name="user_birth[]" class="form-control birth-input2" onlynum maxlength="2" placeholder="MM"/> 월
                    <input type="text" name="user_birth[]" class="form-control birth-input2" onlynum maxlength="2" placeholder="DD"/> 일
                </div>
                <div class="col-sm-5">
                </div>
            </div>
            <div class="row form-group mb30">
                <div class="col-sm-2">
                    <label class="input-label" for="user_phone">휴대전화</label>
                </div>
                <div class="col-sm-5 phone-group">
                    <select class="form-control phone-input" name="user_phone[]">
                        <option value="010">010</option>
                        <option value="011">011</option>
                        <option value="016">016</option>
                        <option value="017">017</option>
                        <option value="018">018</option>
                        <option value="019">019</option>
                    </select>
                    - <input type="text" name="user_phone[]" class="form-control phone-input" onlynum maxlength="4"/>
                    - <input type="text" name="user_phone[]" class="form-control phone-input" onlynum maxlength="4"/>
                </div>
                <div class="col-sm-5">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-2">
                    <label class="input-label" for="user_email">E-Mail</label>
                </div>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="user_email" name="user_email" maxlength="100">
                </div>
                <div class="col-sm-6">
                </div>
            </div>
        </div>
    </div>

    <div class="row join-agree mb50">
        <div class="col-lg-12 p20 border">
            <input type="checkbox" class="mr20" id="checkedAll"/><label for="checkedAll" class="checked-all">이용약관 및 개인정보 수집 및 이용, 마케팅 활용(선택)에 모두 동의합니다.</label>
        </div>
        <div class="col-lg-12 p20 join-agree-contents border">
            <p class="mb10 agree-title">[필수] 이용약관 동의</p>
            <div class="mb10 agree-contents">
                <?php $this->load->view('user/serviceAgreement')?>
            </div>
            <label>이용약관에 동의하십니까?</label>
            <input type="checkbox" id="user_use_agree" name="user_use_agree" value="Y" class="ml10 mr10 agreeCheckbox"/><label for="user_use_agree">동의함</label>
        </div>
        <div class="col-lg-12 p20 join-agree-contents border">
            <p class="mb10 agree-title">[필수] 개인정보 수집 및 이용 동의</p>
            <div class="mb10 agree-contents">
                <?php $this->load->view('user/personalInformation')?>
            </div>
            <label>개인정보 수집 및 이용에 동의하십니까?</label>
            <input type="checkbox" id="user_info_agree" name="user_info_agree" value="Y" class="ml10 mr10 agreeCheckbox"/><label for="user_info_agree">동의함</label>
        </div>
        <div class="col-lg-12 p20 join-agree-contents border">
            <p class="mb10 agree-title">[선택] 마케팅 활용 동의</p>
            <p class="mb10 text-color3">서비스와 관련된 신상품 소식, 이벤트 안내, 고객 혜택 등 다양한 정보를 제공합니다.</p>
            <input type="checkbox" id="user_sms_agree" name="user_sms_agree" value="Y" class="ml10 mr10 agreeCheckbox"/><label for="user_sms_agree">SMS 수신동의</label>
            <input type="checkbox" id="user_email_agree" name="user_email_agree" value="Y" class="ml10 mr10 agreeCheckbox"/><label for="user_email_agree">E-Mail 수신동의</label>
        </div>
    </div>
</form>
<div class="row text-right mb130">
    <button type="button" class="button1" onclick="formSubmit()">가입완료</button>
</div>
<script>
function formSubmit()
{
    var user_loginid = $('#user_loginid');
    var idReg = RegExp(/^[a-z0-9]{4,16}$/);
    if (!$.trim(user_loginid.val())) {
        alert("아이디를 입력해주세요.");
        user_loginid.focus();
        return false;
    }
    if (!idReg.test($.trim(user_loginid.val()))) {
        alert("아이디는 영문소문자/숫자, 4~16자로 입력해주세요.");
        user_loginid.focus();
        return false;
    }
    var user_password = $('#user_password');
    var pwdReg = RegExp(/^(?=.*[a-zA-Z0-9])(?=.*[!@#$%^*+=-]).{8,16}$/);
    if (!$.trim(user_password.val())) {
        alert("비밀번호를 입력해주세요.");
        user_password.focus();
        return false;
    }
    if (!pwdReg.test($.trim(user_password.val()))) {
        alert("비밀번호는 영문대소문자/숫자/특수문자를 포함한 조합, 8~16자로 입력해주세요.");
        user_password.focus();
        return false;
    }
    var user_password_check = $('#user_password_check');
    if (!$.trim(user_password_check.val())) {
        alert("비밀번호 확인을 입력해주세요.");
        user_password_check.focus();
        return false;
    }
    if ($.trim(user_password.val()) != $.trim(user_password_check.val())) {
        alert("비밀번호와 비밀번호 확인이 일치하지 않습니다.");
        user_password_check.focus();
        return false;
    }
    var user_name = $('#user_name');
    if (!$.trim(user_name.val())) {
        alert("이름을 입력해주세요.");
        user_name.focus();
        return false;
    }
    var user_gender = $('[name="user_gender"]:checked');
    if (user_gender.length == 0) {
        alert("성별을 선택해주세요.");
        $('[name="user_gender"]:first').focus();
        return false;
    }
    var user_birth = $('[name="user_birth[]"]');
    if (user_birth.length > 0) {
        var rs = true;
        $.each(user_birth, function(idx, item){
            var value = $.trim($(item).val());
            var maxlength = $(item).attr('maxlength');
            if (!value) {
                alert("생년월일을 입력해주세요.");
                $(item).focus();
                rs = false;
                return false;
            }
            if (maxlength && value.length != maxlength) {
                alert("생년월일을 정확히 입력해주세요.");
                $(item).focus();
                rs = false;
                return false;
            }
        });
        if (!rs) return false;
    }
    var user_phone = $('[name="user_phone[]"]');
    if (user_phone.length > 0) {
        var rs = true;
        $.each(user_phone, function(idx, item){
            var value = $.trim($(item).val());
            var maxlength = $(item).attr('maxlength');
            if (!value) {
                alert("휴대전화를 입력해주세요.");
                $(item).focus();
                rs = false;
                return false;
            }
            if (maxlength && value.length != maxlength) {
                alert("휴대전화를 정확히 입력해주세요.");
                $(item).focus();
                rs = false;
                return false;
            }
        });
        if (!rs) return false;
    }
    var user_use_agree = $('[name="user_use_agree"]:checked');
    if (user_use_agree.length == 0) {
        alert("이용약관에 동의해주세요.");
        $('[name="user_use_agree"]:first').focus();
        return false;
    }
    var user_info_agree = $('[name="user_info_agree"]:checked');
    if (user_info_agree.length == 0) {
        alert("개인정보 수집 및 이용에 동의해주세요.");
        $('[name="user_info_agree"]:first').focus();
        return false;
    }
    $("#userRegForm").submit();
}
$(document).on('click', '#checkedAll', function(){
    if ($(this).prop('checked')) {
        $('.agreeCheckbox').prop('checked', true);
    } else {
        $('.agreeCheckbox').prop('checked', false);
    }
})
$(document).on('click', '.agreeCheckbox', function(){
    if ($('.agreeCheckbox').length == $('.agreeCheckbox:checked').length) {
        $('#checkedAll').prop('checked', true);
    } else {
        $('#checkedAll').prop('checked', false);
    }
})
</script>