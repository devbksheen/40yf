<?php echo $layout->breadcrumbs?>
<div class="row mb30 text-center">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/login-text-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/login-text-banner.png')?>" class="w100p mobile"/>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 p40 text-center login-input-area">
        <button type="button" class="kakao-login mb30" onclick="kakaoLogin()">
            <img src="<?php echo base_url('assets/user/img/kakao.png')?>"/>
            카카오 로그인
        </button>
        <br/>
        <button type="button" class="normal-join" onclick="location.href='<?php echo base_url('join')?>'">일반 회원가입</button>
    </div>
    <div class="col-lg-6 p40">
        <form method="POST">
            <div class="row form-group mb40">
                <div class="col-sm-3">
                    <label for="user_loginid">아이디</label>
                </div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="user_loginid" name="user_loginid" maxlength="20">
                </div>
            </div>
            <div class="row form-group mb40">
                <div class="col-sm-3">
                    <label for="user_password">비밀번호</label>
                </div>
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="user_password" name="user_password" maxlength="20">
                </div>
            </div>
            <div class="col-lg-12 text-center">
                <button class="button1 login-button">로그인</button>
            </div>
        </form>
    </div>
</div>

<script src="https://developers.kakao.com/sdk/js/kakao.min.js"></script>
<script>
Kakao.init('d237db9772d10260366c7a2608916d66');  
function kakaoLogin() {
    Kakao.Auth.login({
        persistAccessToken: true,
        persistRefreshToken: true,
        success: function(authObj) {

			Kakao.Auth.setAccessToken(authObj.access_token, false);
			Kakao.API.request({
		        url: '/v2/user/me',
		        success: function(res) {
                    var userData = {};
                    userData.user_loginid = res.kakao_account.email;
                    userData.user_name = res.kakao_account.profile.nickname;
                    userData.user_gender = res.kakao_account.gender ? (res.kakao_account.gender == "male" ? 'M':'W') : null;
                    userData.user_phone = res.kakao_account.phone_number;
                    userData.user_birth = 
                    (res.kakao_account.birthyear ? res.kakao_account.birthyear : '') +
                    (res.kakao_account.birthday ? res.kakao_account.birthday : '');
                    userData.user_birth = userData.user_birth ? userData.user_birth : null;

                    $.ajax({
                        url: "<?php echo base_url('login/kakao')?>", 
                        data: userData,
                        type: "POST",
                        dataType: "json",
                        success: function(res2) {
                            if (res2) {
                                if (res2.orguser_id == 0) {
                                    location.href = '<?php echo base_url('login/type')?>';
                                } else {
                                    location.href = '<?php echo base_url()?>';
                                }
                            } else {
                                alert('카카오 로그인에 실패하였습니다.\n해당 오류가 지속될 경우 홈페이지 하단에 문의처로 문의바랍니다.');
                                return false;
                            }
                        }
                    });
		        },
		        fail: function(error) {
		            alert(JSON.stringify(error));
		        }
            });
        },
        fail: function(err) {
            alert(JSON.stringify(err));
        }
    });
}
</script>