<style>
    #serviceAgreemant ul { padding-left: 25px; }
    #serviceAgreemant li { font-size: 15px; }
    #serviceAgreemant * { font-family: NanumGothic!important; font-weight: inherit; }
</style>
<div id="serviceAgreemant" class="mb100 mt50">
    <div><p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:center;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><b><span style="font-size:14.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">서비스 이용약관<span lang="EN-US"><o:p></o:p></span></span></b></p>

<p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:center;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:center;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">제<span lang="EN-US">1</span>장 총칙<span lang="EN-US"><o:p></o:p></span></span></b></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제1조<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">목적<span lang="EN-US">) <o:p></o:p></span></span></b></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">본 약관은<span lang="EN-US"> 40FY</span>㈜<span lang="EN-US"> (</span>이하<span lang="EN-US"> “</span>회사<span lang="EN-US">”</span>라 합니다<span lang="EN-US">)</span>가 제공하는 서비스 이용과 관련한 회사와 회원 간의 권리와
의무<span lang="EN-US">, </span>책임 및 서비스 이용 등 기본적인 사항을 규정함을 목적으로 합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">이용자는 본 이용 약관을
자세히 읽은 후 이용 약관에 동의하지 않을 경우<span lang="EN-US">, </span>본 이용약관에 동의하지 말아야 하며 서비스에 등록하거나
이를 이용하지 않아야 합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 회원의 권리를
보호하기 위하여 위와 같은 내용을 쉽게 알 수 있도록 작성하여 명확하게 표시하고 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사 서비스 이용 전
꼭 확인하여 주세요 </span><span lang="EN-US" style="font-size:12.0pt;font-family:&quot;Segoe UI Emoji&quot;,sans-serif;
mso-fareast-font-family:굴림;mso-bidi-font-family:&quot;Segoe UI Emoji&quot;;mso-font-kerning:
0pt">😊<o:p></o:p></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제2조<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">용어의 정의<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l34 level1 lfo9;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">1.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">‘</span><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">서비스<span lang="EN-US">’</span>란 회사가 모바일 기기 등을 통해 제공하는 멘탈 헬스케어 서비스 및 이에 부수하는 서비스
일체를 의미합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l34 level1 lfo9;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">‘</span><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">이용자<span lang="EN-US">’</span>란 회사 서비스에 접속하여<span lang="EN-US">, </span>이 약관에 따라
회사가 제공하는 서비스를 이용하는 회원과 비회원을 말합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l34 level1 lfo9;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">3.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">‘</span><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">회원<span lang="EN-US">’</span>이란 회사와 서비스 이용 계약을 체결한 개인 또는 기업을 말합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l34 level1 lfo9;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">4.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">‘</span><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">비회원<span lang="EN-US">’</span>이란 일부 정보만 제공하고 회사가 제공하는 서비스의 일부만 이용하는 자를 의미합니다<span lang="EN-US">. <o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l34 level1 lfo9;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">5.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">‘</span><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">모바일 기기 등<span lang="EN-US">’</span>이란 회사가 제공하는 서비스를 이용하기 위하여<span lang="EN-US"> PC, </span>휴대폰<span lang="EN-US">, </span>스마트폰<span lang="EN-US">, </span>태블릿
등 모바일 기기를 통해 다운로드 받거나 설치하여 사용하는 프로그램 및 모바일 기기를 통해 접속할 수 있는 네트워크<span lang="EN-US">, </span>웹사이트<span lang="EN-US">, </span>클라이언트 프로그램 등 일체를 의미합니다<span lang="EN-US">. <o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l34 level1 lfo9;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">6.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">‘</span><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">회원가입<span lang="EN-US">’</span>이란 회원이 회사와의 사이에 이 약관의 내용에 따라 회사 서비스 이용계약을 체결하는
것을 말합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l34 level1 lfo9;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">7.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">‘</span><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">회원탈퇴<span lang="EN-US">’</span>란 회원이 회사와의 사이에 이 약관의 내용에 따라 회사 서비스 이용계약을 해지하는
것을 말합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l34 level1 lfo9;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">8.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">‘</span><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">닉네임<span lang="EN-US">’</span>이란 회원식별과 회원의 서비스 이용을 위하여 회원이 선정하고 회사가 승인하는 문자열을
말합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l34 level1 lfo9;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림"><span style="mso-list:Ignore">9.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">‘</span><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">비밀번호<span lang="EN-US">’</span>란 회원이
사용하는 이메일과 일치하는 회원임을 확인하고 회원의 비밀 보호를 위해 회원 자신이 설정한 문자와 숫자의 조합을 말합니다<span lang="EN-US">.</span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l34 level1 lfo9;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">10.<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span><!--[endif]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">‘</span><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">컨텐츠<span lang="EN-US">’</span>란 모바일
기기로 이용할 수 있도록 회사 또는 제휴사<span lang="EN-US">(</span>상담사 포함<span lang="EN-US">)</span>가
서비스 제공과 관련하여 디지털 방식으로 제작한 유료 또는 무료의 내용물 일체<span lang="EN-US">(</span>동영상<span lang="EN-US">, </span>이미지<span lang="EN-US">, </span>텍스트<span lang="EN-US">, </span>채팅
및 네트워크 서비스 등<span lang="EN-US">)</span>를 의미합니다<span lang="EN-US">. <o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l34 level1 lfo9;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">11.<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span><!--[endif]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">‘</span><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">검사도구<span lang="EN-US">’</span>란 이용자들
특성에 맞춘 컨텐츠를 제공하기 위해 회사가 제공하고 이용자들이 수행하는<span lang="EN-US">&nbsp;</span>심리검사를 말합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l34 level1 lfo9;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">12.<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span><!--[endif]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">‘</span><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">상품<span lang="EN-US">’</span>이란 회원에게
유상 또는 무상으로 제공하는 유형 또는 무형의 재화를 말합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l34 level1 lfo9;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">13.<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span><!--[endif]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">‘</span><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">이용권<span lang="EN-US">’</span>이란 회사가
제공하는 컨텐츠 중 유료 컨텐츠를 이용하기 위하여 이용자가 일정한 대가를 지급하고 구매하는 유료 서비스 상품을 말합니다<span lang="EN-US">. <o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l34 level1 lfo9;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">14.<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span><!--[endif]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">‘</span><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">상담사<span lang="EN-US">’</span>란 회사가
제공하는 서비스를 통해 회원에게 상담을 제공하기 위하여 회사와 제휴 계약을 체결한 전문가를 말합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l34 level1 lfo9;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">15.<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span><!--[endif]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">‘</span><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">상담권<span lang="EN-US">’</span>이란 온<span lang="EN-US">/</span>오프라인 모임을 통해 상담사로부터 <span lang="EN-US">1:1 </span>또는 그룹상담 서비스를 제공받기
위하여 이용자가 일정한 대가를 지급하고 구매하는 유료 서비스 상품을 말합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제3조<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">약관의 효력 및 변경<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.15pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.15pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l30 level1 lfo10;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">본 약관은 서비스를 이용하고자 하는 모든 이용자에게 그 효력이
발생합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.15pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.15pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l30 level1 lfo10;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원이 본 약관의<span lang="EN-US"> "</span>동의하기<span lang="EN-US">" </span>버튼을 클릭하였을 경우 본 약관의 내용을 모두 읽고 이를 충분히 이해하였으며<span lang="EN-US">, </span>회사가 운영하는 본 서비스를 지속적으로 사용하는 것에 동의함을 의미합니다<span lang="EN-US">. </span>변경된
약관에 대한 정보를 알지 못하여 발생하는 회원의 피해에 대하여 회사는 책임이 없습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.15pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.15pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l30 level1 lfo10;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 「위치정보의 보호 및 이용 등에 관한 법률」<span lang="EN-US">, </span>「콘텐츠산업 진흥법」<span lang="EN-US">, </span>「전자상거래 등에서의 소비자보호에 관한 법률」<span lang="EN-US">, </span>「소비자기본법」<span lang="EN-US">, </span>「 약관의 규제에 관한 법률」 등 관련법령을 위배하지
않는 범위에서 본 약관을 변경할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.15pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.15pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l30 level1 lfo10;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">④<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사가 약관을 변경할 경우에는 적용일자와 변경사유를 밝혀 본
서비스 화면에 게시하거나 기타의 방법으로 그 적용일자 <span lang="EN-US">7</span>일 전부터 공지합니다<span lang="EN-US">. </span>다만<span lang="EN-US">, </span>변경 내용이 회원에게 불리한 경우에는 그 적용일자<span lang="EN-US"> 30</span>일 전부터 적용일 이후 상당한 기간 동안 각각 이를 서비스 홈페이지에 게시하거나 회원에게 전자적 형태<span lang="EN-US">(</span>이메일<span lang="EN-US">, SMS </span>등<span lang="EN-US">)</span>로
약관 변경 사실을 발송하여 고지합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.15pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.15pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l30 level1 lfo10;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑤<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원이 연락처를 기재하지 않았거나 변경하여 발송이 어려울 경우에는
본 서비스 화면에 변경 약관을 공지하는 것을 개별 발송한 것으로 간주합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.15pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.15pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l30 level1 lfo10;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑥<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사가 본 조 제<span lang="EN-US">4</span>항에
따라 변경약관을 공지하거나 통지하면서<span lang="EN-US">, </span>회원이 약관 변경 적용일까지 거부 의사를 표시하지 아니하면 약관
변경에 동의한 것으로 간주한다는 내용을 공지 또는 통지하였음에도 회원이 변경된 약관의 효력 발생일까지 약관 변경에 대한 거부 의사를 표시하지 않을
때에는 회원이 변경약관에 동의한 것으로 간주합니다<span lang="EN-US">. </span>회원은 변경된 약관에 동의하지 않으면 본 서비스의
이용을 중단하고 이용계약을 해지할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.15pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.15pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l30 level1 lfo10;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑦<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 이용자가 요구하면 본 약관의 사본을 이용자에게 교부하며<span lang="EN-US">, </span>이용자가 서비스 내에서 본 약관을 인쇄할 수 있도록 조치합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제4조<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">관계법령의 적용<span lang="EN-US">)</span></span></b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"> <o:p></o:p></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">본 약관은 신의성실의
원칙에 따라 공정하게 적용하며<span lang="EN-US">, </span>본 약관에서 정하지 아니한 사항과 본 약관의 해석에 관하여는 「전자상거래
등에서의 소비자보호에 관한 법률」<span lang="EN-US">, </span>「약관의 규제에 관한 법률」<span lang="EN-US">, </span>「정보통신망이용촉진
및 정보보호 등에 관한 법률」<span lang="EN-US">, </span>「콘텐츠산업진흥법」 등 관련 법령 또는 상관례에 따릅니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제5조<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">운영정책<span lang="EN-US">) <o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l36 level1 lfo11;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">약관을 적용하기 위하여 필요한 사항과 약관에서 구체적 범위를
정하여 위임한 사항을 서비스 운영정책<span lang="EN-US">(</span>이하<span lang="EN-US"> “</span>운영정책<span lang="EN-US">”</span>이라 합니다<span lang="EN-US">)</span>으로 정할 수 있습니다<span lang="EN-US">.
<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.15pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.15pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l36 level1 lfo11;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 운영정책의 내용을 회원이 알 수 있도록 서비스 내 또는
그 연결화면에 게시합니다<span lang="EN-US">. <o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.15pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.15pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l36 level1 lfo11;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">운영정책을 개정하는 경우에는 제<span lang="EN-US">3</span>조 제<span lang="EN-US">4</span>항의 절차에 따릅니다<span lang="EN-US">. </span>다만<span lang="EN-US">, </span>운영정책 개정내용이 다음 각 호의 어느 하나에 해당하는 경우에는 전항의 방법으로 사전에 공지합니다<span lang="EN-US">. <o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l13 level1 lfo12;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">1.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">약관에서 구체적으로 범위를 정하여 위임한 사항을 개정하는 경우
<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l13 level1 lfo12;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원의 권리<span lang="EN-US">·</span>의무와
관련 없는 사항을 개정하는 경우 <span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l13 level1 lfo12;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">3.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">운영정책의 내용이 약관에서 정한 내용과 근본적으로 다르지 않고
회원이 예측할 수 있는 범위 내에서 운영정책을 개정하는 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제6조<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">회원가입<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l14 level1 lfo13;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원이 되고자 하는 이용자는 회사가 정한 절차에 따라 이 약관과
개인정보처리방침에 동의함으로써 회원 가입을 신청합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l14 level1 lfo13;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">모든 회원은 반드시 이용자 본인의 이메일 또는 회원가입에 필요한
정보를 제공하고 회원가입을 완료해야만 서비스를 이용할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l14 level1 lfo13;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">이용자가 만<span lang="EN-US"> 14</span>세
미만인 경우에는 법정대리인의 동의를 받아야만 회원 가입을 할 수 있습니다<span lang="EN-US">. <o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l14 level1 lfo13;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">④<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">모든 회원은 가입 시 지정한 비밀번호에 의한 활동이나 행위 일체에
대한 책임을 부담합니다<span lang="EN-US">. </span>회사는 이용자에게 강력한 비밀번호<span lang="EN-US">(</span>대<span lang="EN-US">∙</span>소문자<span lang="EN-US">, </span>숫자 및 특수문자를 조합한 형태의 비밀번호<span lang="EN-US">)</span>를 설정할 것을 권장합니다<span lang="EN-US">. </span>회사는 회원이 이를 준수하지 아니함으로
인하여 발생하는 손해에 대한 책임을 부담할 수 없습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l14 level1 lfo13;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑤<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원가입계약의 성립시기는 이용자가 서비스에 접속하여 정보 입력
및 약관에 동의 후<span lang="EN-US">, </span>회원가입에 대한 승낙을 받은 때 완료됩니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l14 level1 lfo13;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑥<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">이용자가 서비스를 이용하면서 기재하는 모든 정보는 실제 데이터인
것으로 간주되며<span lang="EN-US">, </span>허위의 정보를 입력한 이용자는 법적인 보호를 받을 수 없고<span lang="EN-US">, </span>서비스 이용에 제한을 받을 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l14 level1 lfo13;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑦<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">정확한 정보로 등록하지 않은 이용자는 일체의 권리를 주장할 수
없으며<span lang="EN-US">, </span>타인의 정보를 도용하여 회원가입을 한 회원의 모든 계정은 삭제되며 관계 법령에 따라 처벌을
받을 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l14 level1 lfo13;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑧<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 아래 사항에 해당하는 경우에 대하여 회원 가입 신청을
거부할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l22 level1 lfo14;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">1.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">타인의 정보를 이용하거나 허위 내용으로 신청한 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l22 level1 lfo14;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">사회의 질서를 저해할 목적으로 신청한 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l22 level1 lfo14;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">3.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">영리를 추구할 목적으로 본 서비스를 이용하고자 하는 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l22 level1 lfo14;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">4.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">부정한 용도로 본 서비스를 이용하고자 하는 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l22 level1 lfo14;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">5.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">이용자가 회원탈퇴 또는 신청자의 귀책사유로 인한 회사의 계약
해지로 회원 자격을 상실한 사실이 있는 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l22 level1 lfo14;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">6.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사로부터 서비스 이용 정지 조치 등을 받은 자가 그 조치기간
중에 이용계약을 임의 해지하고 재이용신청을 하는 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l22 level1 lfo14;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">7.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">관련 법령에서 금지하는 행위를 할 목적으로 신청하는 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l22 level1 lfo14;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">8.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">그 밖에 각 호에 준하는 사유로서 승낙이 부적절하다고 판단되는
경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l14 level1 lfo13;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑨<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원은 가입 시 회사에 제공한 정보에 변경이 있을 경우 즉시
이메일<span lang="EN-US">, </span>기타 방법으로 회사에 그 변경사항을 알려야 합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제7조<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">계정 해지 및 회원탈퇴<span lang="EN-US">) <o:p></o:p></span></span></b></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원은 언제든지 회원
탈퇴를 진행하거나 회사에 요청할 수 있으며 회사는 위 요청을 받은 즉시 해당 회원과 체결한 이용계약을 해지하며 개인정보처리방침에 따라 해당 회원의
정보 삭제를 위한 절차를 밟습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제8조<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">회사의 권리 및 의무<span lang="EN-US">) <o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l16 level1 lfo15"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 관련 법령과 본 약관이 금지하거나
공서양속에 반하는 행위를 하지 않으며 본 약관이 정하는 바에 따라 지속적이고<span lang="EN-US">, </span>안정적인 서비스를 제공하기
위해서 노력합니다<span lang="EN-US">. </span>다만<span lang="EN-US">, </span>응급상황<span lang="EN-US">, </span>천재지변<span lang="EN-US">, </span>비상사태<span lang="EN-US">, </span>정기점검<span lang="EN-US">, </span>설비장애 또는 이용 폭주 등으로 인하여 서비스 이용에 지장이 있는 경우 등 부득이한 경우에는 그 서비스를 일시
중단하거나 중지할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l16 level1 lfo15"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 회원이 안전하게 서비스를 이용할
수 있도록 개인정보 보호를 위해 보안시스템을 구축하며 개인정보처리방침을 공시하고 준수합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l16 level1 lfo15"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 회원이 서비스 이용과 관련하여
의견과 불만을 제공할 경우 이것이 정당하다고 인정한다면 이를 처리하여야 합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l16 level1 lfo15"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">④<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 회원이 서비스를 이용함에 있어
법률적인 증명이 가능한 고의 또는 중대한 과실로 회원에게 손해를 입힐 경우 이로 인한 손해를 배상할 책임이 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l16 level1 lfo15"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑤<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 서비스의 향상과 신규 서비스
개발<span lang="EN-US">, </span>심리 관련 컨텐츠 연구 등을 위하여 서비스 제공 과정에서 회원이 작성한 모든 자료와 정보를 개인정보처리방침에
따라 수집하여 통계<span lang="EN-US">, </span>연구 또는 심리 관련 컨텐츠 개발에 활용할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l16 level1 lfo15"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑥<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 서비스 질의 향상과 회사 및
서비스의 홍보를 위하여 서비스에서 회원이 작성한 이용 후기 등의 자료를 익명화하여 기타 포털사이트 등에 제휴 및 협약 등을 통해 게시하거나 전송할
수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제9조<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">상담사의 권리 및 의무<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l35 level1 lfo16"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">상담사는 회사가 제공하는 서비스를
통해서만 회원에게 상담 서비스를 제공할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l35 level1 lfo16"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">상담사는 회사가 요구하는 심리 상담과
관련된 자격증 취득 정보 등 최소한의 정보를 회사에 공개해야 합니다<span lang="EN-US">. </span>상담사는 회사에게 위와 같은 정보를
전달함에 있어 거짓이 없어야 하며<span lang="EN-US">, </span>회사가 관련 증명서류를 요청할 경우 이에 응해야 합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l35 level1 lfo16"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">상담사는 회사가 제공하는 컨텐츠<span lang="EN-US">, </span>검사도구 등을 서비스 내에서만 사용할 수 있으며<span lang="EN-US">, </span>서비스의 지식재산권을
침해하는 일체의 행위를 해서는 안 됩니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l35 level1 lfo16"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">④<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">상담사는 서비스 제공 외 목적으로
회원의 개인정보를 수집<span lang="EN-US">, </span>저장하거나 이용해서는 안 됩니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l35 level1 lfo16"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑤<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">상담사는 회원과의 상담 내용에 대하여
비밀준수 의무를 지닙니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l35 level1 lfo16"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑥<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">상담사는 상담 도중 본 약관을 위반하는
행위를 하는 회원에 대하여 타 회원의 정상적인 서비스 이용을 위해 필요한 조치를 취하여야 합니다<span lang="EN-US">. </span>상담사는
본 약관을 위반하거나 광고<span lang="EN-US">, </span>스토킹<span lang="EN-US">, </span>장난 등 회사 서비스의
취지에 부합하지 않는 목적으로 상담을 요청하는 회원에 대하여 회사 또는 경찰 등 관계기관에 신고할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제10조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원의 권한 및 의무<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l2 level1 lfo17"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">이용자는 회원가입 또는 회원정보 변경
시 본인에 대하여 정확한 정보를 사실에 근거하여 작성하여야 합니다<span lang="EN-US">. </span>허위 또는 타인의 정보로 작성할
경우 일체의 권리를 주장할 수 없습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l2 level1 lfo17"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원은 본 약관에서 규정하는 사항과
공지사항<span lang="EN-US">, </span>기타 회사가 정한 규정 및 관계법령을 준수하여야 하며<span lang="EN-US">, </span>기타
회사의 업무에 방해되는 행위를 해서는 안 됩니다<span lang="EN-US">. </span>이를 지키지 않을 경우<span lang="EN-US">, </span>서비스 이용에 제한을 받거나 법적 조치를 포함한 제재를 받을 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l2 level1 lfo17"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원은 이메일 주소 등 정보가 변경된
경우에 회사에 즉시 알려야 합니다<span lang="EN-US">. </span>회원은 자신의 계정에 관한 정보를 제<span lang="EN-US">3</span>자에게 제공하거나 유출되게 하여서는 아니 되며<span lang="EN-US">, </span>회사가 관계법령 및
개인정보처리방침에 의거하여 그 책임을 지는 경우를 제외하고 회원의 계정<span lang="EN-US">, </span>비밀번호 등의 관리 소홀<span lang="EN-US">, </span>부정 사용으로 인하여 발생하는 모든 책임은 회원에게 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l2 level1 lfo17"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">④<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원은 컨텐츠를 포함한 일체의 서비스를
회원 본인만이 이용할 수 있고 제<span lang="EN-US">3</span>자에게 이용권한을 제공<span lang="EN-US">(</span>담보<span lang="EN-US">, </span>신탁 등의 설정을 포함<span lang="EN-US">)</span>하거나 이용을 허락할 수 없습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l2 level1 lfo17"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑤<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원이 회사가 제공하는 유료 컨텐츠를
이용하거나 전문 상담사로부터 상담을 받기 위해서는 유료 서비스 상품을 구매해야 하며<span lang="EN-US">, </span>유료 서비스 상품
구매 후 환불에 대하여 본 약관 및 회사가 공지하는 환불 정책을 따라야 합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l2 level1 lfo17"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑥<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원은 상품 등을 구매하기 전에 반드시
회사가 제공하는 상품 등의 상세 내용과 조건을 정확하게 확인한 후 구매를 신청하여야 합니다<span lang="EN-US">. </span>구매하려는
상품 등의 상세 내용과 조건을 확인하지 않고 구매하여 발생하는 손해에 대하여 회원이 책임을 부담합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l2 level1 lfo17"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑦<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원은 회사가 서비스를 안전하게 제공할
수 있도록 회사에 협조해야 하며<span lang="EN-US">, </span>회사가 회원의 이 약관 위반 행위를 발견하여 회원에게 해당 위반 행위에
대하여 소명을 요청할 경우 회원은 회사의 요청에 적극 응하여야 합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l2 level1 lfo17"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑧<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">미성년자인 회원이 서비스를 이용하여
상품 등을 구매 시 법정대리인이 해당 계약에 대하여 동의를 하여야 정상적인 상품 등의 구매계약이 체결될 수 있습니다<span lang="EN-US">. </span>만약<span lang="EN-US">, </span>미성년자인 회원이 법정대리인의 동의 없이 상품 등을 구매하는
경우 본인 또는 법정대리인이 이를 취소할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l2 level1 lfo17"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑨<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원은 서비스 내 컨텐츠보호기능을
우회<span lang="EN-US">, </span>삭제<span lang="EN-US">, </span>수정<span lang="EN-US">, </span>무효화<span lang="EN-US">, </span>약화 또는 훼손하거나<span lang="EN-US">, </span>서비스에 접근하는 데 로봇<span lang="EN-US">, </span>스파이더<span lang="EN-US">, </span>스크레이퍼나 기타 자동화 수단을 이용하거나<span lang="EN-US">, </span>서비스를 통하여 접근 가능한 소프트웨어나 기타 제품<span lang="EN-US">, </span>프로세스를
역컴파일<span lang="EN-US">, </span>리버스 엔지니어링 또는 역어셈블하거나<span lang="EN-US">, </span>코드나
제품을 삽입하거나 어떤 방식으로든 서비스를 조작하거나<span lang="EN-US">, </span>데이터 마이닝<span lang="EN-US">,
</span>데이터 수집 또는 추출 방법을 사용할 수 없습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l2 level1 lfo17"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑩<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원은 컴퓨터바이러스 기타 컴퓨터
코드<span lang="EN-US">, </span>파일이나 프로그램을 포함하여<span lang="EN-US">, </span>서비스와 관련된 컴퓨터
소프트웨어나 하드웨어 또는 통신 장비의 기능을 방해<span lang="EN-US">, </span>파괴 또는 제한하기 위해 설계된 자료를 업로드<span lang="EN-US">, </span>게시<span lang="EN-US">, </span>이메일 전송<span lang="EN-US">, </span>또는
다른 방식으로 발송<span lang="EN-US">, </span>전송할 수 없습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l2 level1 lfo17"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑪<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원은 서비스 이용에 필요한 최소한의
구체적이고 진실한 정보를 제공하여야 하며<span lang="EN-US">, </span>정보가 부족할 경우 서비스 이용에 제한을 받을 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l2 level1 lfo17"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑫<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원은 상담시 상담사의 귀책사유 있는
불법 내지 위법행위를 알게 된 경우 회사에 해당 사실을 통지하며<span lang="EN-US">, </span>회사는 관련 사실관계를 확인하고 상담사와의
제휴관계 해지 등 필요한 조치를 취할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l2 level1 lfo17"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑬<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">전항의 경우<span lang="EN-US">, </span>회원은 상담사에 대하여 상담사의 귀책사유 있는 불법 내지 위법행위에 대한 책임을 청구할 수 있습니다<span lang="EN-US">. </span>단<span lang="EN-US">, </span>회사에게 해당 불법 내지 위법행위에 대한 고의 또는 중과실이
있는 경우를 제외하고는 회원은 회사에 대하여 상담사의 귀책사유 있는 불법 내지 위법행위에 대한 책임을 물을 수 없습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제11조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원의 개인정보 보호 및 사용<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l28 level1 lfo18"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 관계법령이 정하는 바에 따라
회원의 개인정보를 보호하기 위해 노력합니다<span lang="EN-US">. </span>회원이 회사에 제공하는 정보 일체는 회사의 개인정보처리방침에
따릅니다<span lang="EN-US">. </span>단<span lang="EN-US">, </span>회사가 제공하는 서비스 이외의 링크된 서비스에서는
회사의 개인정보처리방침이 적용되지 않습니다<span lang="EN-US">. <o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l28 level1 lfo18"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">서비스의 특성에 따라 회원의 개인정보와
관련이 없는 별명</span><span lang="EN-US" style="font-size:12.0pt;line-height:107%;
font-family:&quot;MS Mincho&quot;;mso-bidi-font-family:&quot;MS Mincho&quot;;mso-font-kerning:0pt">‧</span><span style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt">캐릭터 사진</span><span lang="EN-US" style="font-size:12.0pt;
line-height:107%;font-family:&quot;MS Mincho&quot;;mso-bidi-font-family:&quot;MS Mincho&quot;;
mso-font-kerning:0pt">‧</span><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">상태정보 등 자신을 소개하는 내용이
공개될 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l28 level1 lfo18"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 관련 법령에 의해 관련 국가기관
등의 요청이 있는 경우를 제외하고는 회원의 개인정보를 본인의 동의 없이 타인에게 제공하지 않습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l28 level1 lfo18"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">④<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 회원의 귀책사유로 개인정보가
유출되어 발생한 피해에 대하여 책임을 지지 않습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제12조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원의 금지행위<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l20 level1 lfo22"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원은 다음 각 호에 해당하는 행위를
해서는 안 되며<span lang="EN-US">, </span>해당 행위를 하는 경우 서비스의 일부 또는 전부를 이용 정지하거나 서비스 이용 계약을
임의로 해지할 수 있습니다<span lang="EN-US">. </span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l7 level1 lfo1;tab-stops:list 81.8pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">1.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">서비스 이용 신청 또는 변경 시 허위 사실을 기재하거나<span lang="EN-US">, </span>타인의 정보를 도용하여 부정하게 사용하는 행위<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l7 level1 lfo1;tab-stops:list 81.8pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사가 게시한 정보를 변경하는 행위<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l7 level1 lfo1;tab-stops:list 81.8pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">3.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사가 정한 정보 이외의 정보를 송신하거나 게시하는 행위<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l7 level1 lfo1;tab-stops:list 81.8pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">4.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">서비스에 포함되어 있거나 서비스를 통하여 취득한 컨텐츠와 정보를
아카이브<span lang="EN-US">, </span>복제<span lang="EN-US">, </span>배포<span lang="EN-US">, </span>수정<span lang="EN-US">, </span>전시<span lang="EN-US">, </span>시연<span lang="EN-US">, </span>출판<span lang="EN-US">, </span>라이선스<span lang="EN-US">, 2</span>차적 저작물로 생성<span lang="EN-US">,
</span>판매 또는 권유하거나 이용하는 행위를 포함하여 회사와 기타 제<span lang="EN-US">3</span>자의 저작권 등 기타 권리를
침해하는 행위<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l7 level1 lfo1;tab-stops:list 81.8pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">5.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사<span lang="EN-US">, </span>회원<span lang="EN-US">, </span>상담사 및 기타 제<span lang="EN-US">3</span>자의 명예 또는 신용을 훼손하거나 업무를 방해하는
행위<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l7 level1 lfo1;tab-stops:list 81.8pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">6.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">불특정 다수를 대상으로 하여 광고 또는 선전을 게시하거나 음란물
또는 불쾌감을 조성할 수 있는 표현을 하는 행위<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l7 level1 lfo1;tab-stops:list 81.8pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">7.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사의 서비스를 이용하여 얻은 정보를 사전 승낙 없이 복제 또는
유통시키거나 상업적으로 이용하는 행위<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l7 level1 lfo1;tab-stops:list 81.8pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">8.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">비방<span lang="EN-US">, </span>욕<span lang="EN-US">, </span>외설적인 음담패설<span lang="EN-US">, </span>개인의 인권 또는 평등권을 침해하는 차별적 표현
등 규정에 적합하지 않은 표현 및 행위<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l7 level1 lfo1;tab-stops:list 81.8pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">9.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">컴퓨터 소프트웨어<span lang="EN-US">, </span>하드웨어<span lang="EN-US">, </span>전기통신 장비 정상적인 가동을 방해 또는 파괴할 적으로 고안된 소프트웨어 바이러스<span lang="EN-US">, </span>기타 다른 컴퓨터 코드<span lang="EN-US">, </span>파일<span lang="EN-US">, </span>프로그램을
포함하고 있는 자료를 게시하거나 전송하는 행위<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l7 level1 lfo1;tab-stops:list 81.8pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">10.</span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">회사의 동의 없이 영리를 목적으로 서비스를 사용하는 행위<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l7 level1 lfo1;tab-stops:list 81.8pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">11.</span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">제<span lang="EN-US">3</span>자의 정보를 무단으로 수집하는 행위<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l7 level1 lfo1;tab-stops:list 81.8pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">12.</span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">서비스 이용 도중 본인의 개인정보를 알리거나<span lang="EN-US">, </span>회원 및 상담사 등 제<span lang="EN-US">3</span>자의 개인정보를 알리거나<span lang="EN-US">, </span>알려고 하는 일체의 행위<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l7 level1 lfo1;tab-stops:list 81.8pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">13.</span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">신고 남발<span lang="EN-US">, </span>허위 제보 등 서비스의 원활한 제공을 방해하거나 허위사실을 회사 또는 타인에게
전달함으로써 부당한 피해를 입도록 하는 행위<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l7 level1 lfo1;tab-stops:list 81.8pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">14.</span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">기타 관계 법령에 위배 또는 미풍양속에 반하는 행위<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l20 level1 lfo22"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원이 작성한 게시물이 아래의 어느
하나에 해당하는 표현을 포함하는 경우 회사는 회원에게 삭제를 요청하거나 회원의 동의 없이 삭제할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l0 level1 lfo5;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">1.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">대한민국 또는 외국의 법령에 위배되는 표현<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l0 level1 lfo5;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">대한민국 또는 외국에서의 범죄와 관련되는 표현<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l0 level1 lfo5;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">3.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원 또는 제<span lang="EN-US">3</span>자의
영리를 목적으로 한 표현<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l0 level1 lfo5;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">4.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">음란성ㆍ포악성ㆍ잔인성ㆍ사행성 등이 포함된 표현<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l0 level1 lfo5;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">5.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">사회질서를 파괴하는 표현<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l0 level1 lfo5;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">6.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">사실 또는 허위사실의 적시에 의하여 회사 또는 제<span lang="EN-US">3</span>자의 명예를 훼손하는 표현<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l0 level1 lfo5;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">7.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사 및 제<span lang="EN-US">3</span>자의
지식재산을 침해하는 표현<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l0 level1 lfo5;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">8.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">개인의 인권 또는 평등권을 침해하는 차별적 표현<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l20 level1 lfo22"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원은 상담권을 사용하여 상담을 진행하는
경우 다음 각호와 같은 언어사용을 해서는 안 됩니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<ol start="1" type="1">
 <li class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
     text-align:left;line-height:normal;mso-pagination:widow-orphan;mso-list:
     l19 level1 lfo19;tab-stops:list 36.0pt;text-autospace:ideograph-numeric ideograph-other;
     word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;
     mso-bidi-font-family:굴림;mso-font-kerning:0pt">타인의 명예를 훼손<span lang="EN-US">,
     </span>모욕<span lang="EN-US">, </span>권리를 침해하는 등의 언어 사용<span lang="EN-US"><o:p></o:p></span></span></li>
</ol>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l19 level1 lfo19;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">욕설이나 노골적인 성 묘사를 하는 언어 사용<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l19 level1 lfo19;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">3.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">특정 정치<span lang="EN-US">, </span>인종<span lang="EN-US">, </span>민족<span lang="EN-US">, </span>종교<span lang="EN-US">, </span>성별<span lang="EN-US">, </span>인물을 조롱하는 등 타인의 명예를 훼손시키거나 권리를 침해하는 언어 사용<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l19 level1 lfo19;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">4.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">부모 또는 가족에 대한 욕설<span lang="EN-US">,
</span>비방 혹은 혐오감을 직<span lang="EN-US">/</span>간접적으로 상징하는 언어 사용<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l19 level1 lfo19;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">5.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">범죄행위를 목적으로 하거나 이와 관련된 내용을 직<span lang="EN-US">/</span>간접적으로 표현하는 언어 사용<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l19 level1 lfo19;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">6.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">선량한 풍속<span lang="EN-US">, </span>기타
사회질서를 해하는 언어 사용<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l19 level1 lfo19;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">7.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">기타 타인에게 불쾌감을 줄 수 있는 부적절한 어휘 사용<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l20 level1 lfo22"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">④<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원은 그룹상담을 진행하는 경우 다음
각호와 같이 모임 내<span lang="EN-US">/</span>외적으로 의도적 또는 반복적으로 모임 진행 및 타 회원의 서비스 이용에 피해를
끼치는 일체의 행위를 하여서는 안 됩니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<ol start="1" type="1">
 <li class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
     text-align:left;line-height:normal;mso-pagination:widow-orphan;mso-list:
     l32 level1 lfo20;tab-stops:list 36.0pt;text-autospace:ideograph-numeric ideograph-other;
     word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;
     mso-bidi-font-family:굴림;mso-font-kerning:0pt">그룹상담 내에서 과도한 발언으로 다른 회원의 발언에
     피해를 주는 경우 <span lang="EN-US"><o:p></o:p></span></span></li>
</ol>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l32 level1 lfo20;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">그룹상담 내에서 의도적<span lang="EN-US">, </span>반복적으로
대화의 흐름에 맞지 않는 발언을 할 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l32 level1 lfo20;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">3.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">그룹상담 내에서 상담사의 진행 지시에 따르지 않고 방해되는 행동을
반복하는 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l32 level1 lfo20;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">4.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">유출금지 그룹상담에서 공유된 일체의 내용에 대한 외부 유출<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l20 level1 lfo22"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑤<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원은 서비스 내<span lang="EN-US">/</span>외에서 회사 서비스 또는 회사와 무관한 내용을 홍보하거나 광고하여 타인에게 불편함을 줄 수 있는 일체의 행위를
하여서는 안 됩니다<span lang="EN-US">. <o:p></o:p></span></span></p>

<ol start="1" type="1">
 <li class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
     text-align:left;line-height:normal;mso-pagination:widow-orphan;mso-list:
     l5 level1 lfo21;tab-stops:list 36.0pt;text-autospace:ideograph-numeric ideograph-other;
     word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;
     mso-bidi-font-family:굴림;mso-font-kerning:0pt">대화의 흐름 또는 상황과 관계없는 내용을 본 서비스<span lang="EN-US">, </span>모임 및 카카오톡 단체채팅방 등에서 홍보 및 판촉 등을 하는 경우<span lang="EN-US"><o:p></o:p></span></span></li>
</ol>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l5 level1 lfo21;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사의 승인을 받지 않고 영리 목적으로 각종 홍보 및 판촉 등을
하는 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l20 level1 lfo22"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑥<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">상담사가 중단 경고를 하였음에도 불구하고
회원이 서비스 이용 과정에서 욕이나 음담패설<span lang="EN-US">, </span>외설적 행위 등 본 약관에서 금지된 행위를 하는 경우
상담사는 서비스를 중단하고 해당 회원에 대하여 적절한 조치를 취할 수 있습니다<span lang="EN-US">. </span>또한 피해 주체가 된
상담사 또는 타 회원은 증빙 자료 확보 및 법적조치를 위해 회원의 금지행위에 대해 녹음<span lang="EN-US">/</span>녹화하여 적절한
조치가 취해질 때까지 보유할 수 있으며<span lang="EN-US">, </span>이 과정에서 회원의 신체정보<span lang="EN-US">(</span>성명<span lang="EN-US">, </span>얼굴<span lang="EN-US">, </span>음성 등<span lang="EN-US">), </span>정신<span lang="EN-US">, </span>재산<span lang="EN-US">, </span>사회적 정보와 개인정보가 수집될 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l20 level1 lfo22"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑦<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 필요한 경우 회원의 금지행위
사실을 관련 정부기관 또는 사법기관에 통지할 수 있으며 회원은 법적 조치를 포함한 제재를 받을 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l20 level1 lfo22"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑧<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원은 자신의 귀책사유로 인하여 회사<span lang="EN-US">, </span>상담사 및 타 회원이 입은 손해를 배상할 책임이 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:center;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">제<span lang="EN-US">2</span>장 서비스 이용<span lang="EN-US"><o:p></o:p></span></span></b></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제13조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">서비스의 내용<span lang="EN-US">) <o:p></o:p></span></span></b></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사가 제공하거나 추후
제공할 서비스는 다음 각 호와 같으나<span lang="EN-US">, </span>이에 한정되지 않습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.3pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.15pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l6 level1 lfo4;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">1.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">영상 컨텐츠 및 영상 컨텐츠와 함께 제공되는 활동자료<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.3pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.15pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l6 level1 lfo4;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">채팅<span lang="EN-US">, </span>전화<span lang="EN-US">, </span>화상 통화 형태의 온<span lang="EN-US">/</span>오프라인 모임<span lang="EN-US">, </span>혹은<span lang="EN-US"> 1:1 </span>상담 서비스<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.3pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.15pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l6 level1 lfo4;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">3.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원이 예약한 모임<span lang="EN-US">, </span>상담과
관련된 알림 기능<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.3pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.15pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l6 level1 lfo4;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">4.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">모임 및 상담 후기 및 평가를 작성하고 공유하는 서비스<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.3pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.15pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l6 level1 lfo4;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">5.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">익명 커뮤니티 및 상담 서비스<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.3pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.15pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l6 level1 lfo4;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">6.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">자가진단<span lang="EN-US">, </span>심리 검사를
위한 컨텐츠<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.3pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.15pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l6 level1 lfo4;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">7.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">기타 회사가 본 서비스와 부합하며<span lang="EN-US">, </span>회원의 편의성을 향상시킨다고 판단하여 제공하는 모든 서비스<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제14조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">서비스 이용계약<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l8 level1 lfo23"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">서비스 이용계약은 이용자가 회사가
정한 이용약관에 대하여 동의하고 회원가입에 필요한 온라인 신청 양식을 작성하여 서비스 이용을 신청한 후 이에 대하여 회사가 승인함으로써 성립합니다<span lang="EN-US">. </span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l8 level1 lfo23"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 이용계약이 완료된 회원에게
그 즉시 서비스를 이용할 수 있도록 합니다<span lang="EN-US">. </span>다만<span lang="EN-US">, </span>일부
서비스의 경우 회사의 필요에 따라 지정된 일자부터 서비스를 개시할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l8 level1 lfo23"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원은 회사가 제공하는 다음 각호
또는 이와 유사한 절차에 의하여 유료 서비스 이용을 신청합니다<span lang="EN-US">. </span>회사는 계약 체결 전<span lang="EN-US">, </span>다음 각호의 사항에 관하여 회원이 정확하게 이해하고 실수 또는 착오 없이 거래할 수 있도록 정보를 제공합니다<span lang="EN-US">.</span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:38.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-17.85pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l21 level1 lfo6;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">1.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">서비스 목록의 열람 및 선택<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:38.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-17.85pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l21 level1 lfo6;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">서비스 상세정보 확인<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:38.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-17.85pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l21 level1 lfo6;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">3.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">결제하기 클릭<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:38.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-17.85pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l21 level1 lfo6;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">4.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">주문상품 및 결제금액 확인<span lang="EN-US">(</span>환불규정
안내<span lang="EN-US">)<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:38.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-17.85pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l21 level1 lfo6;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">5.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">성명<span lang="EN-US">, </span>주소<span lang="EN-US">, </span>연락처 등 개인정보 입력<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:38.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-17.85pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l21 level1 lfo6;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">6.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">결제수단 선택<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:38.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-17.85pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l21 level1 lfo6;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">7.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">결제금액 재확인<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:38.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-17.85pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l21 level1 lfo6;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">8.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">결제<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l8 level1 lfo23"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">④<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원은 신용카드<span lang="EN-US">, </span>무통장입금<span lang="EN-US">, </span>인터넷 계좌이체<span lang="EN-US">, </span>휴대폰
결제 등 회사에서 정하고 있는 방법으로 유료서비스의 결제가 가능합니다<span lang="EN-US">. </span>다만<span lang="EN-US">, </span>각 결제수단마다 결제수단의 특성에 따른 일정한 제한이 있을 수 있습니다<span lang="EN-US">.</span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l8 level1 lfo23"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">⑤<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 회원의 유료 서비스 이용신청이
다음 각호에 해당하는 경우에는 승낙하지 않거나<span lang="EN-US">, </span>그 사유가 해소될 때까지 승낙을 유보할 수 있습니다<span lang="EN-US">.</span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:38.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-17.85pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l29 level1 lfo7;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">1.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">유료 서비스의 이용요금을 납입하지 않는 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:38.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-17.85pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l29 level1 lfo7;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">유료 서비스 신청금액 총액과 입금총액이 일치하지 않은 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:38.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-17.85pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l29 level1 lfo7;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">3.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">기타 합리적인 이유가 있는 경우로써 회사가 필요하다고 인정되는
경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:38.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-17.85pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l29 level1 lfo7;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">4.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">만<span lang="EN-US"> 14</span>세 미만의
아동이 이용신청을 하는 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l8 level1 lfo23"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">⑥<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 회원이 위 조항의 절차에 따라
유료서비스 이용을 신청할 경우<span lang="EN-US">, </span>승낙의 의사표시를 회원에게 통지하고<span lang="EN-US">,
</span>승낙의 통지가 회원에게 도달한 시점에 계약이 성립한 것으로 봅니다<span lang="EN-US">.</span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l8 level1 lfo23"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">⑦<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사의 승낙의 의사표시에는 회원의
이용신청에 대한 확인 및 서비스제공 가능여부<span lang="EN-US">, </span>이용신청의 정정<span lang="EN-US">, </span>취소
등에 관한 정보 등을 포함합니다<span lang="EN-US">.</span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l8 level1 lfo23"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">⑧<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 회원에게 서비스를 제공할 때
이 약관에 정하고 있는 서비스를 포함하여 기타 부가적인 서비스를 함께 제공할 수 있습니다<span lang="EN-US">.</span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제15조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">서비스 이용자격<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l12 level1 lfo24"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">만<span lang="EN-US">
19</span>세 미만의 미성년자인 회원이 유료서비스 상품 등을 구매하고자 할 경우<span lang="EN-US">, </span>법정대리인으로부터
서비스 이용에 대한 동의를 받아야 합니다<span lang="EN-US">. </span>이 경우 법정대리인은 이용약관에서 규정하는 회원의 권리 및
의무를 모두 가집니다<span lang="EN-US">.</span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l12 level1 lfo24"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">본 서비스를 이용하려면 모바일 기기
등이 반드시 필요하며<span lang="EN-US">, </span>정기적인 업데이트가 필요할 수 있습니다<span lang="EN-US">. </span>또한
이러한 요인에 의해 기능이 일부 영향을 받을 수 있으며<span lang="EN-US">, </span>이용자는 해당 내용에 대해 이해하고 있어야
합니다<span lang="EN-US">.</span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l12 level1 lfo24"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 회원의 등급을 구분하고 이용시간<span lang="EN-US">, </span>이용횟수<span lang="EN-US">, </span>제공 서비스의 범위 등을 세분화하여 이용에 차등을 둘
수 있습니다<span lang="EN-US">.</span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l12 level1 lfo24"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">④<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원이 상담권을 구매하여 모임 참가
혹은 상담 서비스를 이용할 경우 참가 전 준수사항 등을 확인하고 회사에서 요청하는 내용에 대해 응답 및 동의를 해야 합니다<span lang="EN-US">.</span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l12 level1 lfo24"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">⑤<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">유료 서비스 상품 구매한 후 정정<span lang="EN-US">, </span>취소 및 환불을 원할 경우 회원은 회사의 환불 정책에 따라 이를 진행하여야 합니다<span lang="EN-US">.</span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제16조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">서비스 이용 시 주의 사항<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l24 level1 lfo25"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사가 제공하는 컨텐츠는 상담 전문가의
검수를 받은 신뢰할 수 있는 정보이지만<span lang="EN-US">, </span>의료 및 질병과 관련한 일체의 의문점이 있거나 심리건강 상태에
있어서 조금이라도 우려되는 부분이 있는 경우에는 서비스를 이용하기 전에 반드시 전문적인 의학 진단이나 처방을 받기 위하여 의료기관에서 의사와 직접
상담하여야 합니다<span lang="EN-US">.</span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l24 level1 lfo25"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사에서 제공하는 컨텐츠<span lang="EN-US">, </span>자가평가<span lang="EN-US">, </span>심리검사<span lang="EN-US">, </span>모임
및 심리상담은 의사의 진료를 대신할 수 없습니다<span lang="EN-US">. </span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l24 level1 lfo25"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 회원이 본 약관의 내용을 위반하여
서비스를 이용할 경우 발생하거나 발생할 수 있는 일체의 손해에 대하여 책임을 지지 않습니다<span lang="EN-US">. </span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l24 level1 lfo25"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">④<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">그룹모임에서의 정보의 유출은 허용되지
않으며<span lang="EN-US">, </span>유출로 인한 모든 피해상황에 대해 회사는 법적으로 책임을 지지 않습니다<span lang="EN-US">.</span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l24 level1 lfo25"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">⑤<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span lang="EN-US" style="font-size:12.0pt;
line-height:107%;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">1:1
</span><span style="font-size:12.0pt;line-height:107%;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">상담 및 그룹모임 등 회사가 제공하는 서비스 이외의 개입이 필요한
정신질환<span lang="EN-US">, </span>자타해 등의 가능성이 있다고 판단되는 경우에는 회사는 해당 회원에게 서비스를 중지하고 도움이
될 수 있는 다른 방법을 안내할 수 있습니다<span lang="EN-US">. </span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l24 level1 lfo25"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">⑥<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">만 <span lang="EN-US">19</span>세 미만의 미년성자의 경우 필요 시에<span lang="EN-US"> 1388 </span>청소년 상담을 권유하며<span lang="EN-US">, 14</span>세 미만의 회원의 경우 법정대리인의 동의 없이 본 서비스를 이용할 수 없습니다<span lang="EN-US">.</span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제17조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">서비스 이용요금<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l10 level1 lfo26"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사가 제공하는 유료 서비스는 해당
서비스에 명시된 요금을 지불하여야 사용 가능합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l10 level1 lfo26"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 유료 서비스 이용요금을 회사와
계약한 전자지불업체에서 정한 방법에 의하거나 회사가 정한 청구서에 합산하여 청구할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l10 level1 lfo26"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">유료서비스 이용을 통하여 결제된 대금에
대한 취소 및 환불은 회사의 환불정책 등 관계법령에 따릅니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l10 level1 lfo26"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">④<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원의 개인정보도용 및 결제사기로
인한 환불요청 또는 결제자의 개인정보 요구는 법률이 정한 경우 외에는 거절될 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l10 level1 lfo26"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑤<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">무선 서비스 이용 시 발생하는 데이터
통신료는 별도이며 가입한 각 이동통신사의 정책에 따릅니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l10 level1 lfo26"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑥<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">유료 온라인 모임 혹은 상담 시 서비스
제공자의 인터넷 환경의 문제로 시스템 연결이 불안정할 경우 환불정책에 따라 이용요금이 환불되지만<span lang="EN-US">, </span>회원의
인터넷 환경의 문제로 시스템 연결이 불안정한 경우 환불이 불가합니다<span lang="EN-US">. <o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제18조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">서비스 내용 변경 통지 등<span lang="EN-US">) <o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l33 level1 lfo27"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사가 회원에게 서비스 내용 변경
등의 통지를 하는 경우 회원의 이메일 주소<span lang="EN-US">, </span>전자메모<span lang="EN-US">, </span>서비스
내 쪽지<span lang="EN-US">, </span>문자메시지<span lang="EN-US">(LMS/SMS) </span>등으로 할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l33 level1 lfo27"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 회원 전체에게 통지를 하는
경우<span lang="EN-US"> 7</span>일 이상 서비스 내에 게시하거나 팝업화면 등을 제시함으로써 제<span lang="EN-US">1</span>항의
통지에 갈음할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제19조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">서비스 이용 제한 및 중지<span lang="EN-US">) <o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l27 level1 lfo28"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 다음의 각 호에 해당하는 사유가
발생할 경우<span lang="EN-US">, </span>회원의 서비스 이용을 제한하거나 중지시킬 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l15 level1 lfo2;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">1.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원이 회사 서비스의 운영을 고의 또는 중과실로 방해하는 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l15 level1 lfo2;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">다른 사람의 명의사용 등 이용자가 허위 정보로 서비스를 신청하는
경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l15 level1 lfo2;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">3.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">신청 시 필요한 사항을 허위로 기재하거나 중요한 사항에 기재누락
오기가 있는 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l15 level1 lfo2;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">4.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">부정한 방법으로 서비스를 이용하거나 정보를 조작하여<span lang="EN-US">, </span>회사에 피해를 줄 수 있다고 판단하는 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l15 level1 lfo2;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">5.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사에서 공지한 주의사항 또는 경고사항을 준수하지 않거나 동의하지
않는 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l15 level1 lfo2;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">6.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">서비스용 설비 점검<span lang="EN-US">, </span>보수
등의 이유로 서비스 제공이 불가능한 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l15 level1 lfo2;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">7.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">전기통신사업법에 규정된 기간통신사업자가 전기통신 서비스를 중지했을
경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l15 level1 lfo2;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">8.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">국가비상사태<span lang="EN-US">, </span>서비스
설비의 장애 또는 서비스 이용의 폭주 등으로 서비스 이용에 지장이 있는 때<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-17.85pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l15 level1 lfo2;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><a name="_Hlk57318235"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">9.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원이 서비스 이용 중 본 약관 제<span lang="EN-US">11</span>조에서 금지하는 행위를 하는 등 본 약관을 위반한 경우</span></a><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p></o:p></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l15 level1 lfo2;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">10.</span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">기타 중대한 사유로 인하여 회사가 서비스 제공을 지속하는 것이 부적당하다고 인정하는 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l27 level1 lfo28"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 전항의 규정에 의하여 다음
각 호의 구분에 따른 서비스 이용제한<span lang="EN-US">, </span>관련 정보<span lang="EN-US">(</span>글<span lang="EN-US">, </span>사진<span lang="EN-US">, </span>영상 등<span lang="EN-US">) </span>삭제
및 기타의 조치를 포함한 이용제한 조치를 할 수 있습니다<span lang="EN-US">. </span>단<span lang="EN-US">, </span>서비스의
이용을 제한하거나 중지한 때에는 그 사유 및 제한기간 등을 회원에게 알려야 합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:40.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l27 level2 lfo28"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">1.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt">일부 권한 제한<span lang="EN-US"> : </span>일정기간 게시글 업로드 등 일정 권한을
제한<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:40.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l27 level2 lfo28"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt">계정 이용제한<span lang="EN-US"> : </span>일정기간 또는 영구히 회원 계정의 이용을
제한<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:40.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l27 level2 lfo28"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">3.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt">회원 이용제한<span lang="EN-US"> : </span>일정기간 또는 영구히 회원의 서비스 이용을
제한<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l27 level1 lfo28"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 회원의 서비스 신청 이후에
제<span lang="EN-US">1</span>항의 사유가 있음을 알게 된 경우<span lang="EN-US">, </span>이용계약의 해지 또는
중단 등의 이용제한 조치를 취할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l27 level1 lfo28"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">④<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">본조의 이용제한이 정당한 경우에 회사는
이용제한으로 인하여 회원이 입은 손해를 배상하지 않습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l27 level1 lfo28"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑤<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 다음 각 호의 사유에 대한
조사가 완료될 때까지 해당 계정의 서비스 이용을 정지할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:40.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l27 level2 lfo28"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">1.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-spacerun:yes">&nbsp;</span></span><span style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt">계정이 해킹 또는 도용 당했다는 정당한 신고가 접수된 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:40.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l27 level2 lfo28"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-spacerun:yes">&nbsp;</span></span><span style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt">불법프로그램 사용자 등 위법행위자로 의심되는 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:40.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l27 level2 lfo28"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">3.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-spacerun:yes">&nbsp;</span></span><span style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt">이 계약 제<span lang="EN-US">11</span>조의 위반자로 의심되거나 다른 회원의 신고가
접수된 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:40.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l27 level2 lfo28"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">4.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp; </span></span></span><!--[endif]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-spacerun:yes">&nbsp;</span></span><span style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt">그 밖에 각 호에 준하는 사유로 서비스 이용의 잠정조치가 필요한 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l27 level1 lfo28"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑥<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">제<span lang="EN-US">5</span>항의
조사가 완료된 후<span lang="EN-US">, </span>유료 콘텐츠의 경우에는 정지된 시간만큼 회원의 이용시간을 연장하거나 그에 상당하는
유료 콘텐츠 또는 캐쉬 등으로 보상합니다<span lang="EN-US">. </span>다만<span lang="EN-US">, </span>회원이
제<span lang="EN-US">3</span>항 각 호의 사유에 해당하는 경우에는 그러하지 아니합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l27 level1 lfo28"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑦<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 회원이 제공하는 정보<span lang="EN-US">, </span>자료에 대하여 회사의 공지<span lang="EN-US">, </span>서비스 이용안내에서 정한 바에 따라
일정한 게재기한 및 용량을 정할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제20조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원정보와 상담정보<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l1 level1 lfo29"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원은 언제든지 자신의 개인정보를
열람하고 수정할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l1 level1 lfo29"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원이 전항의 변경사항을 수정하지
않아 불이익이 발생하였을 경우<span lang="EN-US">, </span>이에 대해 회사는 책임지지 않습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l1 level1 lfo29"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원은 유료 컨텐츠<span lang="EN-US">, </span>상담 등 유료 서비스 상품을 이용하기 위하여 회원가입과 별도의 신청서를 제출하여야 합니다<span lang="EN-US">. </span>신청서를 통해 수집되는 내용은 서비스의 특성상 민감 정보가 포함될 수 있으며<span lang="EN-US">,
</span>회원은 해당 사실을 인지하고 동의하여야 합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l1 level1 lfo29"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">④<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">상담서비스 제공 및 이용을 위하여
수집된 회원의 개인정보와 상담 내용 등은 회사와 제휴된 상담사가 공유하며<span lang="EN-US">, </span>개인정보 수집과 관련된 세부내용은
개인정보처리방침을 따릅니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l1 level1 lfo29"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑤<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원의 개인정보<span lang="EN-US">, </span>상담 접수지 정보<span lang="EN-US">, </span>검사 정보<span lang="EN-US">, </span>상담
정보 등 그리고 회사가 운영하는 플랫폼에 회원이 게시한 글 등 서비스 이용과 관련된 정보와 자료는 회사가 보관합니다<span lang="EN-US">. <o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l1 level1 lfo29"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑥<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원의 서비스 이용과 관련된 모든
정보와 자료는 익명화되어 회사에게 귀속되며<span lang="EN-US">, </span>더 나은 서비스를 위하여 회사의 서비스 개발과 연구<span lang="EN-US">, </span>통계<span lang="EN-US">, </span>빅데이터 활용 등에 활용될 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l1 level1 lfo29"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">⑦<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span lang="EN-US" style="font-size:12.0pt;
line-height:107%;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">1:1
</span><span style="font-size:12.0pt;line-height:107%;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">상담 및 그룹모임 비밀보장 참가자의 신원은 익명이 기본이며 모든
상담 내용은 비밀이 보장됩니다<span lang="EN-US">. </span>하지만 모든 생명은 소중하고 존엄하기에 내담자 본인 및 타인의 생명이나
안전에 위험이 있다고 판단되는 경우 회사 또는 상담사는 추가적인 개인정보를 요구할 수 있고 비밀보장이 제한될 수 있습니다<span lang="EN-US">. </span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd"><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제21조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-spacerun:yes">&nbsp;</span>(</span></b><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">이용계약의 해지<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l23 level1 lfo30"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원이 이용계약의 해지를 원할 경우<span lang="EN-US">, </span>서비스 내의 회원 탈퇴 기능을 통하여 언제든지 회원탈퇴를 신청할 수 있습니다<span lang="EN-US">.
</span>단<span lang="EN-US">, </span>해지의사를 통지하기 전에 현재 진행 중인 모든 상품의 거래를 완료하거나 환불 정책에
맞게 취소 또는 환불을 진행하여야 하며<span lang="EN-US">, </span>거래를 취소했을 때 발생할 수 있는 불이익은 회원 본인이 부담하여야
합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l23 level1 lfo30"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원 탈퇴로 인해 발생한 불이익에
대한 책임은 회원 본인이 부담하여야 하며<span lang="EN-US">, </span>이용계약이 종료되면 회사는 회원에게 부가적으로 제공한 각종
혜택을 회수할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l23 level1 lfo30"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 불가피한 사정이 없는 한 회원의
탈퇴 요청을 처리해야 합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l23 level1 lfo30"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">④<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 회원이 본 이용약관 또는 관계법령
등을 위반하는 경우<span lang="EN-US">, </span>이용계약을 해지할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">제<span lang="EN-US">3</span>장 기타<span lang="EN-US"><o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제22조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">저작권<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l18 level1 lfo31"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">회사가 제공하는 컨텐츠<span lang="EN-US">, </span>회사가 개발한 검사지<span lang="EN-US">, </span>해설지 및 기타 서비스와 관련된 모든 저작물에 대한 저작권<span lang="EN-US">, </span>기타
지식재산권 등 일체의 권리는 회사에 귀속합니다<span lang="EN-US">. </span>회원은 회사가 제공하는 서비스를 이용함으로써 얻은 정보를
회사의 사전 허락 없이 복제<span lang="EN-US">, </span>재가공<span lang="EN-US">, </span>전송<span lang="EN-US">, </span>출판<span lang="EN-US">, </span>배포<span lang="EN-US">, </span>방송<span lang="EN-US">, 2</span>차적 저작물 작성 등 기타 방법에 의하여 영리 목적으로 이용하거나 제<span lang="EN-US">3</span>자에게
이용하게 하여서는 안 됩니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l18 level1 lfo31"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">회원이 서비스 내에 게시한 게시물의 저작권은 해당 게시물의 저작자에게 귀속됩니다<span lang="EN-US">. </span>단<span lang="EN-US">, </span>회사는 서비스의 운영<span lang="EN-US">, </span>전시<span lang="EN-US">, </span>전송<span lang="EN-US">, </span>배포<span lang="EN-US">, </span>홍보의 목적으로 회원의 별도의 허락 없이 무상으로 저작권법에
규정하는 공정한 관행에 합치되는 합리적인 범위 내에서 회원이 등록한 게시물을 사용할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l18 level1 lfo31"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">회원과 상담사가 상담과정에서 작성하는 상담내용과 게시하는 글 또는 회사가 운영하는 플랫폼에 게시하는 글 등 저작물의
저작권 또는 저작물의 이용권은 회사에 무상으로 영구적으로 귀속되며<span lang="EN-US">, </span>회사는 모든 자료와 정보를 익명화하여
수집하여 심리상담과 관련한 연구<span lang="EN-US">, </span>통계<span lang="EN-US">, </span>개발에 활용할 수
있습니다<span lang="EN-US">. </span>또한<span lang="EN-US">, </span>회사는 빅데이터 활용 또는 연구를 위해
제<span lang="EN-US">3</span>자에게 재이용권을 허락할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제23조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">서비스 게시물의 등록 및 관리<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l3 level1 lfo32"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">회원은 약관에서 규정하는 범위 내에서 자율적으로 게시물을 작성할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l3 level1 lfo32"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">회사는 회원의 게시물이 정보통신망법 및 저작권법 등 관련법에 위반되는 내용을 포함하였을 경우<span lang="EN-US">, </span>권리자는 관련법이 정한 절차에 따라 해당 게시물의 게시중단 및 삭제 등을 요청할 수 있으며<span lang="EN-US">, </span>회사는 관련법에 따라 조치를 취하여야 합니다<span lang="EN-US">. </span>또한<span lang="EN-US">, </span>권리자의 요청이 없는 경우라도 권리침해가 인정될 만한 사유가 있거나 기타 회사 정책 및 관련법에 위반되는 경우에는
관련법에 따라 해당 게시물에 대해 임시조치 등을 취할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제24조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">광고<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l17 level1 lfo34"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">회사는 서비스의 운영과 관련하여 서비스 내에 광고를 게재할 수 있습니다<span lang="EN-US">. </span>또한
수신에 동의한 회원에 한하여 전자우편<span lang="EN-US">, </span>문자서비스<span lang="EN-US">(LMS/SMS), </span>푸시메시지<span lang="EN-US">(Push Notification) </span>등의 방법으로 광고성 정보를 전송할 수 있습니다<span lang="EN-US">. </span>이 경우 회원은 언제든지 수신을 거절할 수 있으며<span lang="EN-US">, </span>회사는 회원의
수신 거절 시 광고성 정보를 발송하지 아니합니다<span lang="EN-US">. <o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l17 level1 lfo34"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">회사가 제공하는 서비스 중의 배너 또는 링크 등을 통해 타인이 제공하는 광고나 서비스에 연결될 수 있습니다<span lang="EN-US">. <o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l17 level1 lfo34"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">제<span lang="EN-US">2</span>항에 따라 타인이 제공하는 광고나 서비스에 연결될 경우 해당 영역에서
제공하는 서비스는 회사의 서비스 영역이 아니므로 회사가 신뢰성<span lang="EN-US">, </span>안정성 등을 보장하지 않으며<span lang="EN-US">, </span>그로 인한 회원의 손해에 대하여도 회사는 책임을 지지 않습니다<span lang="EN-US">. </span>다만<span lang="EN-US">, </span>회사가 고의 또는 중과실로 손해의 발생을 용이하게 하거나 손해 방지를 위한 조치를 취하지 아니한 경우에는 그러하지
아니합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제25조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">손해배상<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l9 level1 lfo33"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">회사가 관계법령에 따르는 규정을 위반한 행위로 회원에게 손해가 발생한 경우 회원은 회사에 대하여 손해배상 청구를 할
수 있습니다<span lang="EN-US">. </span>이 경우 회사는 고의<span lang="EN-US">, </span>과실이 없음을 입증하지
못하는 경우 책임을 면할 수 없습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l9 level1 lfo33"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">회원이 본 약관의 규정을 위반하여 회사에 손해가 발생한 경우 회사는 회원에 대하여 손해배상을 청구할 수 있습니다<span lang="EN-US">. </span>이 경우 회원은 고의<span lang="EN-US">, </span>과실이 없음을 입증하지 못하는 경우 책임을
면할 수 없습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l9 level1 lfo33"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">회사가 상담사 등 개별서비스 제공자와 제휴 계약을 맺고 회원에게 개별서비스를 제공하는 경우에 회원이 이 개별서비스
이용약관에 동의를 한 뒤 개별서비스 제공자의 고의 또는 과실로 인해 회원에게 손해가 발생한 경우에 그 손해에 대해서는 개별서비스 제공자가 책임을
집니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제26조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">면책조항<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l26 level1 lfo35"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">회사는 아래의 각 호에 해당하는 경우 책임이 면제되며 이로 인한 손해를 배상할 책임도 없습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l31 level1 lfo3;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">1.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">천재지변<span lang="EN-US">, </span>전쟁 및
기타 이에 준하는 불가항력으로 인하여 서비스를 제공할 수 없을 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l31 level1 lfo3;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">기간통신 사업자가 전기통신 서비스를 중지하거나 정상적으로 제공하지
아니하여 손해가 발생한 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l31 level1 lfo3;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">3.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">서비스용 설비의 보수<span lang="EN-US">, </span>교체<span lang="EN-US">, </span>정기점검<span lang="EN-US">, </span>공사 등의 부득이한 사유로 손해가 발생할 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l31 level1 lfo3;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">4.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">서비스 제공을 위하여 회사와 서비스 제휴계약을 체결한 제<span lang="EN-US">3</span>자의 고의적인 서비스 방해가 있는 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l31 level1 lfo3;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">5.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원의 귀책사유로 인해 서비스 이용의 장애 또는 손해가 발생할
경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l31 level1 lfo3;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">6.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">이용자의 컴퓨터 오류에 의해 손해가 발생할 경우<span lang="EN-US">, </span>또는 회원이 신상정보 및 이메일 주소를 부정확하게 입력하거나 비밀번호를 부실하게 입력하여 손해가 발생할 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l31 level1 lfo3;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">7.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원이 서비스를 이용하여 기대하는 이익을 얻지 못하거나 상실한
경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l31 level1 lfo3;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">8.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사의 고의 또는 중과실 없이<span lang="EN-US">,
</span>회원이 서비스를 이용하면서 얻은 자료로 손해가 발생할 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l31 level1 lfo3;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">9.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원이 서비스를 이용하며 타 회원의 본 약관 위반 행위로 인해
정신적 피해를 입게 될 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l31 level1 lfo3;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">10.</span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">회사의 고의 또는 중과실 없이<span lang="EN-US">, </span>회원이 서비스를 이용하면서 자료를 게시<span lang="EN-US">, </span>전송<span lang="EN-US">, </span>저장<span lang="EN-US">, </span>조회함으로써
법적인 책임이 발생한 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l31 level1 lfo3;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">11.</span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">회원 상호간 및 회원과 제<span lang="EN-US">3</span>자 상호 간에 서비스를 매개로 분쟁이 발생한 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l31 level1 lfo3;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">12.</span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">회원이 검사지와 해설지 등 회사에서 제공하는 콘텐츠가 자신과 맞지 않다고 판단하는 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l31 level1 lfo3;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">13.</span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">회원의 이용약관 위반으로 인하여 타 회원 또는 제<span lang="EN-US">3</span>자에게 손해가 발생한 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:35.7pt;text-align:left;text-indent:-17.85pt;line-height:
normal;mso-pagination:widow-orphan;mso-list:l31 level1 lfo3;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">14.</span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">회원의 귀책사유로 인하여 상담내용 등 개인정보가 노출된 경우<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l26 level1 lfo35"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">회사는 회원에게 무료로 제공하는 서비스의 이용과 관련해서는 어떠한 손해도 책임을 지지 않습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l26 level1 lfo35"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">회사는 비회원의 서비스 이용으로 발생한 손해에 대해서는 책임을 지지 않습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l26 level1 lfo35"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">④<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">회사는 제휴된 상담사가 서비스와 관련하여 게재한 정보<span lang="EN-US">, </span>자료<span lang="EN-US">, </span>사실의 신뢰도와 정확성 등의 내용에 대하여 상담사가 기존에 회사에 제출한 서면 자료의 위조 또는 변조<span lang="EN-US">, </span>기타 사유에 의해 정보가 허위임을 인지하지 못했을 경우에 대해 책임을 지지 않습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l26 level1 lfo35"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">⑤<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">회사는 회원에게 상담 서비스를 제공하기 위하여 상담사를 지정해주는 역할을 담당하지만<span lang="EN-US">,
</span>상담사의 상담 내용에 대한 진위 등을 판단할 수 없습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l26 level1 lfo35"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">⑥<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">회사는 회원과 상담사가 상담을 진행하는 과정에서 발생한 분쟁에 대해 개입할 수 없고 이로 인한 손해를 배상할 책임이
없습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제27조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">규정의 준용<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l11 level1 lfo36"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">본 약관은 대한민국법령에 의하여 규정되고 이행됩니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l11 level1 lfo36"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">본 약관에 규정되지 않은 사항에 대해서는 관련법령 및 상관습에 의합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;margin-left:20.0pt;mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l25 level1 lfo8;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제28조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">분쟁의 조정 및 기타<span lang="EN-US">) <o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l4 level1 lfo37"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">회사와 회원은 서비스와 관련하여 발생한 분쟁을 원만하게 해결하기 위하여 서로 성실하게 노력하여야 합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l4 level1 lfo37"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">회사가 제공하는 서비스는 합법적인 목적으로만 사용되어야 하며<span lang="EN-US">, </span>대한민국
법령에 반하는 형태의 사용은 금지됩니다<span lang="EN-US">. </span>회원의 금지된 형태의 사용으로 인하여 손해가 발생할 경우 회사는
이에 대해 책임을 지지 않습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l4 level1 lfo37"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">회사와 회원간에 발생한 전자상거래 분쟁과 관련하여 회원의 피해 구제 신청이 있는 경우에는 공정거래위원회 또는 시도지사가
의뢰하는 분쟁조정기관의 조정에 따를 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l4 level1 lfo37"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림"><span style="mso-list:Ignore">④<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;line-height:107%;
font-family:굴림">서비스 이용으로 발생한 분쟁에 대해 소송이 제기되는 경우 회사의 본사 소재지를 관할하는 법원을 관할 법원으로 합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">[</span><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt">부칙<span lang="EN-US">]<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">공고일자<span lang="EN-US">: 2020.12.09<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;text-autospace:
ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">시행일자<span lang="EN-US">: 2020.12.09<o:p></o:p></span></span></p>

<p class="MsoNormal"><span lang="EN-US"><o:p>&nbsp;</o:p></span></p><br></div>
</div>