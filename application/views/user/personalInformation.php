<style>
    #personalInformation ul { padding-left: 25px; }
    #personalInformation li { font-size: 15px; }
    #personalInformation * { font-size: 15px; font-family: NanumGothic!important; }
</style>
<div id="personalInformation" class="mb100 mt50">
<div><p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">개인정보처리방침<span lang="EN-US"><o:p></o:p></span></span></b></p>

<p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><o:p>&nbsp;</o:p></span></b></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">40FY</span><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">㈜<span lang="EN-US">(</span>이하<span lang="EN-US"> "</span>회사<span lang="EN-US">"</span>라 함<span lang="EN-US">)</span>는
개인정보보호법<span lang="EN-US">, </span>통신비밀보호법<span lang="EN-US">, </span>전기통신사업법<span lang="EN-US">, </span>정보통신망 이용촉진 및 정보보호 등에 관한 법률 등 정보통신서비스제공자가 준수하여야 할 관련 법령상의 개인정보보호
규정을 준수하며<span lang="EN-US">, </span>관련 법령에 의거한 개인정보처리방침을 정하여 이용자 권익 보호에 최선을 다하고 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">회사의 개인정보처리방침은 다음과 같습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l4 level1 lfo6;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제1조<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">개인정보의 수집 항목 및 수집 방법<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l0 level1 lfo7;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">수집항목<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">회사는 회원가입 및 관리<span lang="EN-US">, </span>고객상담<span lang="EN-US">, </span>재화 또는
서비스의 제공<span lang="EN-US">, </span>고충처리를 위해 아래와 같은 개인정보를 수집하고 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<ol start="1" type="1">
 <li class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
     text-align:left;line-height:normal;mso-pagination:widow-orphan;mso-list:
     l14 level1 lfo1;tab-stops:list 36.0pt;text-autospace:ideograph-numeric ideograph-other;
     word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;
     mso-bidi-font-family:굴림;mso-font-kerning:0pt">필수 항목<span lang="EN-US"> : </span>아이디<span lang="EN-US">, </span>이메일<span lang="EN-US">, </span>비밀번호<span lang="EN-US">, </span>이름<span lang="EN-US">, </span>성별<span lang="EN-US">, </span>생년월일<span lang="EN-US">, </span>핸드폰
     번호<span lang="EN-US">, </span>만 <span lang="EN-US">14</span>세 미만인 경우 법정 대리인 정보<span lang="EN-US">, </span>소셜 로그인<span lang="EN-US">(</span>페이스북<span lang="EN-US">, </span>트위터<span lang="EN-US">, </span>인스타그램<span lang="EN-US">, </span>네이버<span lang="EN-US">, </span>카카오톡<span lang="EN-US">, </span>구글<span lang="EN-US">) </span>계정 <span lang="EN-US">Key</span>값<span lang="EN-US">(</span>비식별화 정보<span lang="EN-US">)<o:p></o:p></span></span></li>
 <li class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
     text-align:left;line-height:normal;mso-pagination:widow-orphan;mso-list:
     l14 level1 lfo1;tab-stops:list 36.0pt;text-autospace:ideograph-numeric ideograph-other;
     word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;
     mso-bidi-font-family:굴림;mso-font-kerning:0pt">서비스 이용 시 수집되는 항목<span lang="EN-US">: </span><a name="_Hlk58065746">상담 접수지 및 검사지 기재 정보</a> <span lang="EN-US"><o:p></o:p></span></span></li>
 <li class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
     text-align:left;line-height:normal;mso-pagination:widow-orphan;mso-list:
     l14 level1 lfo1;tab-stops:list 36.0pt;text-autospace:ideograph-numeric ideograph-other;
     word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;
     mso-bidi-font-family:굴림;mso-font-kerning:0pt">서비스를 이용하기 위한 결제과정에서 수집되는 항목<span lang="EN-US"> : </span>성명<span lang="EN-US">, </span>휴대폰 번호<span lang="EN-US">, </span>결제에
     필요한 신용카드 결제정보<span lang="EN-US">(</span>카드사명<span lang="EN-US">, </span>카드 번호<span lang="EN-US">), </span>은행계좌정보<span lang="EN-US"><o:p></o:p></span></span></li>
 <li class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
     text-align:left;line-height:normal;mso-pagination:widow-orphan;mso-list:
     l14 level1 lfo1;tab-stops:list 36.0pt;text-autospace:ideograph-numeric ideograph-other;
     word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;
     mso-bidi-font-family:굴림;mso-font-kerning:0pt">맞춤형 개인화 서비스 이용 또는 이벤트 응모 과정에서
     해당 서비스의 이용자에 한해 수집될 수 있는 항목<span lang="EN-US"> : </span>성명<span lang="EN-US">,
     </span>핸드폰 번호<span lang="EN-US">, </span>통신사<span lang="EN-US">, </span>물품을 수령할
     주소<span lang="EN-US"><o:p></o:p></span></span></li>
 <li class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
     text-align:left;line-height:normal;mso-pagination:widow-orphan;mso-list:
     l14 level1 lfo1;tab-stops:list 36.0pt;text-autospace:ideograph-numeric ideograph-other;
     word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;
     mso-bidi-font-family:굴림;mso-font-kerning:0pt">선택 항목 <span lang="EN-US">: </span>이용자의
     선택에 따라 본인의<span lang="EN-US"> SNS </span>계정 등을 연결하기 위한 계정 정보<span lang="EN-US">, </span>핸드폰 번호<span lang="EN-US"><o:p></o:p></span></span></li>
 <li class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
     text-align:left;line-height:normal;mso-pagination:widow-orphan;mso-list:
     l14 level1 lfo1;tab-stops:list 36.0pt;text-autospace:ideograph-numeric ideograph-other;
     word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;
     mso-bidi-font-family:굴림;mso-font-kerning:0pt">서비스 이용과정이나 사업처리 과정에서 자동으로 생성되어
     수집될 수 있는 항목<span lang="EN-US"> : IP Address, </span>쿠키<span lang="EN-US">, </span>방문
     일시<span lang="EN-US">, </span>서비스 이용 기록<span lang="EN-US">, </span>불량 이용 기록<span lang="EN-US"><o:p></o:p></span></span></li>
</ol>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l0 level1 lfo7;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">유료 상담 시 서비스의 특성상 상담 신청 전 또는 상담과정에서
다음 각호와 같은 민감정보가 수집되며<span lang="EN-US">, </span>상담 내용과 진행에 따라 추가적인 개인정보가 포함될 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<ol start="1" type="1">
 <li class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
     text-align:left;line-height:normal;mso-pagination:widow-orphan;mso-list:
     l10 level1 lfo8;tab-stops:list 36.0pt;text-autospace:ideograph-numeric ideograph-other;
     word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;
     mso-bidi-font-family:굴림;mso-font-kerning:0pt">신체적<span lang="EN-US">/</span>정신적
     건강정보 <span lang="EN-US">(</span>상담경험<span lang="EN-US">, </span>정신과 진료경험<span lang="EN-US">, </span>현재 및 과거의 신체적<span lang="EN-US">/</span>정신적 호소 문제<span lang="EN-US">)<o:p></o:p></span></span></li>
 <li class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
     text-align:left;line-height:normal;mso-pagination:widow-orphan;mso-list:
     l10 level1 lfo8;tab-stops:list 36.0pt;text-autospace:ideograph-numeric ideograph-other;
     word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;
     mso-bidi-font-family:굴림;mso-font-kerning:0pt">직업<span lang="EN-US">, </span>학력<span lang="EN-US">, </span>성장 과정 등 <span lang="EN-US"><o:p></o:p></span></span></li>
</ol>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l0 level1 lfo7;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">전화로 상담 이용 시<span lang="EN-US">, </span>인터넷
환경의 문제로 시스템 전화 연결이 불안정할 경우 <span lang="EN-US">070 </span>가상번호를 이용하여 서비스가 진행될 수 있습니다<span lang="EN-US">. </span>이 때 담당 상담사에게 전화번호가 노출될 수 있으며 회사는 해당 번호를 수집하지 않습니다<span lang="EN-US">. <o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l0 level1 lfo7;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">④<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">이외에도 서비스 이용 과정에서 서비스 이용 기록<span lang="EN-US">, </span>기기정보가 생성되어 수집될 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;line-height:normal;mso-pagination:
widow-orphan;text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l4 level1 lfo6;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제2조<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">개인정보 수집 및 이용 목적<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">회사는 회원관리<span lang="EN-US">(</span>회원 가입 의사 확인<span lang="EN-US">, </span>본인확인<span lang="EN-US">, </span>개인식별<span lang="EN-US">, </span>회원탈퇴 의사 확인 등<span lang="EN-US">),
</span>회사 서비스 제공<span lang="EN-US">, </span>서비스 개발 및 개선<span lang="EN-US">, </span>안전한
인터넷 이용환경 구축 등 아래의 목적으로만 개인정보를 이용합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l8 level1 lfo9;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사 서비스 제공<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">서비스 이용요금 결제<span lang="EN-US">, </span>콘텐츠 제공<span lang="EN-US">, </span>상담 서비스
제공<span lang="EN-US">, </span>이메일 인증<span lang="EN-US">, </span>물품배송 또는 청구서 등 발송<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l8 level1 lfo9;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원관리<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">회원제 서비스 이용<span lang="EN-US">, </span>본인확인<span lang="EN-US">, </span>개인식별<span lang="EN-US">, </span>서비스 이용요금 결제와 상담 등 서비스 이용 과정에서 법령 및 회사 이용약관을 위반하는 회원에 대한 이용 제한
조치<span lang="EN-US">, </span>부정 이용방지와 비인가 사용방지<span lang="EN-US">, </span>가입의사 확인<span lang="EN-US">, </span>추후 법정 대리인 본인확인<span lang="EN-US">, </span>분쟁 조정을 위한 기록보존<span lang="EN-US">, </span>불만처리 등 민원처리<span lang="EN-US">, </span>고지사항 전달<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l8 level1 lfo9;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">신규 서비스 개발 및 마케팅<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">신규 서비스 개발 및 맞춤 서비스 제공<span lang="EN-US">, </span>통계학적 특성에 따른 서비스 제공 및 광고 게재<span lang="EN-US">, </span>서비스의 유효성 확인<span lang="EN-US">, </span>이벤트 및 광고성 정보 제공 및 참여기회
제공<span lang="EN-US">, </span>회원의 서비스 이용에 대한 통계<span lang="EN-US">, </span>접속빈도 파악<span lang="EN-US">, </span>심리 상담 관련 통계나 연구 등을 통해 심리상담 솔루션 개발 및 개선<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l8 level1 lfo9;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">④<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">맞춤형 개인화 서비스 개발<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">개인정보 및 상담에 기반한 맞춤형 추천 알고리즘 등의 서비스 개발 및 개선<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l4 level1 lfo6;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제3조<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">개인정보 제<span lang="EN-US">3</span>자 제공<span lang="EN-US">)</span></span></b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><o:p></o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l6 level1 lfo10;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 이용자들의 개인정보를 제<span lang="EN-US">2</span>조 개인정보 수집 및 이용 목적에서 고지한 범위 내에서 사용하며<span lang="EN-US">, </span>이용자의
사전 동의 없이는 동 범위를 초과하여 이용하거나 원칙적으로 이용자의 개인정보를 외부에 공개하지 않습니다<span lang="EN-US">. </span>다만<span lang="EN-US">, </span>아래의 경우를 예외로 합니다<span lang="EN-US">. <o:p></o:p></span></span></p>

<ol start="1" type="1">
 <li class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
     text-align:left;line-height:normal;mso-pagination:widow-orphan;mso-list:
     l1 level1 lfo2;tab-stops:list 36.0pt;text-autospace:ideograph-numeric ideograph-other;
     word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;
     mso-bidi-font-family:굴림;mso-font-kerning:0pt">이용자가 외부 제휴사의 서비스를 이용하기 위하여 개인정보
     제공에 별도의 동의를 한 경우<span lang="EN-US"><o:p></o:p></span></span></li>
 <li class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
     text-align:left;line-height:normal;mso-pagination:widow-orphan;mso-list:
     l1 level1 lfo2;tab-stops:list 36.0pt;text-autospace:ideograph-numeric ideograph-other;
     word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;
     mso-bidi-font-family:굴림;mso-font-kerning:0pt">법령의 규정에 의거하거나<span lang="EN-US">, </span>수사 목적으로 법령에 정해진 절차와 방법에 따라 수사기관 또는 정보기관 등의 요구가 있는 경우<span lang="EN-US"><o:p></o:p></span></span></li>
 <li class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
     text-align:left;line-height:normal;mso-pagination:widow-orphan;mso-list:
     l1 level1 lfo2;tab-stops:list 36.0pt;text-autospace:ideograph-numeric ideograph-other;
     word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;
     mso-bidi-font-family:굴림;mso-font-kerning:0pt">이용자의 생명이나 안전에 급박한 위험이 확인되어 이를
     해소하기 위한 경우<span lang="EN-US"><o:p></o:p></span></span></li>
</ol>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal"><span style="font-family:나눔고딕;color:#2A2929;background:white">②
</span><span style="font-size:12.0pt;line-height:107%;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">기타 세금과 관련하여 부가가치세 징수 목적 또는 연말정산 등을
위해 관계법령에 따라 개인정보가 국세청 또는 관세청 등 과세 관련 처분청에 제공될 수 있습니다<span lang="EN-US">.</span></span><span lang="EN-US"><o:p></o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l4 level1 lfo6;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제4조<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">개인정보의 처리 위탁<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l12 level1 lfo11;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 편리하고 더 나은 서비스를 제공하기 위해 업무 중 일부를
외부에 위탁할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l12 level1 lfo11;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 이용자들이 전문적인 상담서비스를 제공받게 하기 위하여
심리상담 서비스 제공업무를 제휴된 심리상담사에게 위탁하고 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l12 level1 lfo11;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 서비스 제공을 위하여 필요한 업무 중 일부를 외부 업체에
위탁할 수 있으며<span lang="EN-US">, </span>위탁할 경우 위탁 받은 업체가 정보통신망법에 따라 개인정보를 안전하게 처리하도록
필요한 사항을 규정하고 관리<span lang="EN-US">/</span>감독을 합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l12 level1 lfo11;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">④<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 서비스 향상을 위하여 아래와 같이 개인정보를 위탁하고
있으며<span lang="EN-US">, </span>개인정보의 수탁자와 위탁업무의 범위는 아래와 같습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<ol start="1" type="1">
 <li class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
     text-align:left;line-height:normal;mso-pagination:widow-orphan;mso-list:
     l9 level1 lfo3;tab-stops:list 36.0pt;text-autospace:ideograph-numeric ideograph-other;
     word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;
     mso-bidi-font-family:굴림;mso-font-kerning:0pt">심리상담 제공 <span lang="EN-US"><o:p></o:p></span></span></li>
</ol>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:54.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-18.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l11 level1 lfo15;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">-<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">위탁받는 자<span lang="EN-US">(</span>수탁자<span lang="EN-US">): </span>제휴 상담사<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:54.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-18.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l11 level1 lfo15;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">-<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">위탁하는 업무의 내용<span lang="EN-US">: </span>심리상담
서비스 제공<span lang="EN-US">, </span>심리검사 내용 확인 및 해석 등 상담 서비스와 관련된 업무 중 일부<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:36.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-18.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l9 level1 lfo3;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">국내 위탁<span lang="EN-US"><o:p></o:p></span></span></p>

<table class="MsoTableGrid" border="1" cellspacing="0" cellpadding="0" style="margin-left:42.3pt;border-collapse:collapse;border:none;mso-border-alt:
 solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt">
 <tbody><tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
  <td width="108" style="width:80.8pt;border:solid windowtext 1.0pt;mso-border-alt:
  solid windowtext .5pt;background:#D9E2F3;mso-background-themecolor:accent1;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><b><span style="mso-bidi-font-size:10.0pt;font-family:
  굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">수탁자<span lang="EN-US"><o:p></o:p></span></span></b></p>
  </td>
  <td width="197" style="width:147.8pt;border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#D9E2F3;mso-background-themecolor:accent1;mso-background-themetint:
  51;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><b><span style="mso-bidi-font-size:10.0pt;font-family:
  굴림;mso-bidi-font-family:굴림;color:black;mso-color-alt:windowtext;mso-font-kerning:
  0pt">위탁업무</span></b><b><span lang="EN-US" style="mso-bidi-font-size:10.0pt;
  font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><o:p></o:p></span></b></p>
  </td>
  <td width="240" style="width:179.9pt;border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#D9E2F3;mso-background-themecolor:accent1;mso-background-themetint:
  51;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><b><span style="mso-bidi-font-size:10.0pt;font-family:
  굴림;mso-bidi-font-family:굴림;color:black;mso-color-alt:windowtext;mso-font-kerning:
  0pt">보유 및 이용기간</span></b><b><span lang="EN-US" style="mso-bidi-font-size:10.0pt;
  font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:1">
  <td width="108" style="width:80.8pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">주식회사 카카오<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="197" style="width:147.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">간편회원가입<span lang="EN-US">, 1:1 </span>문의<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="240" style="width:179.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원탈퇴 또는 위탁계약 종료시<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:2">
  <td width="108" style="width:80.8pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span lang="EN-US" style="mso-bidi-font-size:10.0pt;
  font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">Amazon Web
  Service<o:p></o:p></span></p>
  </td>
  <td width="197" style="width:147.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span lang="EN-US" style="mso-bidi-font-size:10.0pt;
  font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">AWS</span><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">를 통한 시스템 관리<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="240" style="width:179.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원탈퇴 또는 위탁계약 종료시<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:3">
  <td width="108" style="width:80.8pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">카카오페이<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="197" style="width:147.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">결제시스템 제공<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="240" style="width:179.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원탈퇴 또는 위탁계약 종료시<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:4">
  <td width="108" style="width:80.8pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">주식회사 네이버<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="197" style="width:147.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">간편회원가입 <span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="240" style="width:179.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원탈퇴 또는 위탁계약 종료시<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:5">
  <td width="108" style="width:80.8pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">㈜알리는사람들<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="197" style="width:147.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">문자 전송 <span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="240" style="width:179.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원탈퇴 또는 위탁계약 종료시<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:6">
  <td width="108" style="width:80.8pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">오피스콘<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="197" style="width:147.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">모바일 쿠폰 및 경품 전송 <span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="240" style="width:179.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원탈퇴 또는 위탁계약 종료시<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:7">
  <td width="108" style="width:80.8pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">나이스 신용평가 주식회사<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="197" style="width:147.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">본인인증 및 실명 확인<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="240" style="width:179.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원탈퇴 또는 위탁계약 종료시<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:8">
  <td width="108" style="width:80.8pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">주식회사 다날<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="197" style="width:147.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">본인인증 및 결제시스템 <span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="240" style="width:179.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원탈퇴 또는 위탁계약 종료시<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:9">
  <td width="108" style="width:80.8pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">한국정보통신 주식회사<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="197" style="width:147.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">결제 시스템 제공 <span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="240" style="width:179.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원탈퇴 또는 위탁계약 종료시<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:10;mso-yfti-lastrow:yes">
  <td width="108" style="width:80.8pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">씨제이 대한통운 주식회사<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="197" style="width:147.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">택배예약 서비스<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="240" style="width:179.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원탈퇴 또는 위탁계약 종료시<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
</tbody></table>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:36.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-18.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l9 level1 lfo3;tab-stops:list 36.0pt;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><span style="mso-list:Ignore">3.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">국외 위탁<span lang="EN-US"><o:p></o:p></span></span></p>

<table class="MsoTableGrid" border="1" cellspacing="0" cellpadding="0" width="548" style="width:411.05pt;margin-left:42.3pt;border-collapse:collapse;border:none;
 mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0cm 5.4pt 0cm 5.4pt">
 <tbody><tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
  <td width="123" style="width:92.15pt;border:solid windowtext 1.0pt;mso-border-alt:
  solid windowtext .5pt;background:#D9E2F3;mso-background-themecolor:accent1;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><b><span style="mso-bidi-font-size:10.0pt;font-family:
  굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">수탁업체명<span lang="EN-US"><o:p></o:p></span></span></b></p>
  </td>
  <td width="425" style="width:318.9pt;border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span lang="EN-US" style="mso-bidi-font-size:10.0pt;
  font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt;mso-bidi-font-weight:
  bold">Google Cloud Platform<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:1">
  <td width="123" style="width:92.15pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#D9E2F3;mso-background-themecolor:accent1;mso-background-themetint:
  51;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><b style="mso-bidi-font-weight:normal"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  color:black;mso-color-alt:windowtext;mso-font-kerning:0pt">위탁업무 및 목적</span></b><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="mso-bidi-font-size:
  10.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><o:p></o:p></span></b></p>
  </td>
  <td width="425" style="width:318.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span lang="EN-US" style="mso-bidi-font-size:10.0pt;
  font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">Google Firestore</span><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">를 통한 서비스 이용 데이터 관리<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:2">
  <td width="123" style="width:92.15pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#D9E2F3;mso-background-themecolor:accent1;mso-background-themetint:
  51;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><b style="mso-bidi-font-weight:normal"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  color:black;mso-color-alt:windowtext;mso-font-kerning:0pt">개인정보 이전국가</span></b><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="mso-bidi-font-size:
  10.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><o:p></o:p></span></b></p>
  </td>
  <td width="425" style="width:318.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">일본<span lang="EN-US">(Google
  Firestore Tokyo Region)<o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:3">
  <td width="123" style="width:92.15pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#D9E2F3;mso-background-themecolor:accent1;mso-background-themetint:
  51;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><b style="mso-bidi-font-weight:normal"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  color:black;mso-color-alt:windowtext;mso-font-kerning:0pt">이전되는 개인정보 항목</span></b><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="mso-bidi-font-size:
  10.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><o:p></o:p></span></b></p>
  </td>
  <td width="425" style="width:318.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">개인 데이터 및 서비스 이용<span lang="EN-US">(</span>구매<span lang="EN-US">) </span>내역 관리<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:4">
  <td width="123" style="width:92.15pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#D9E2F3;mso-background-themecolor:accent1;mso-background-themetint:
  51;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><b style="mso-bidi-font-weight:normal"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  color:black;mso-color-alt:windowtext;mso-font-kerning:0pt">개인정보 이전일시</span></b><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="mso-bidi-font-size:
  10.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><o:p></o:p></span></b></p>
  </td>
  <td width="425" style="width:318.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span lang="EN-US" style="mso-bidi-font-size:10.0pt;
  font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">40FY </span><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">회원가입 및 서비스 이용시점<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:5">
  <td width="123" style="width:92.15pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#D9E2F3;mso-background-themecolor:accent1;mso-background-themetint:
  51;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><b style="mso-bidi-font-weight:normal"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  color:black;mso-color-alt:windowtext;mso-font-kerning:0pt">개인정보 이전방법</span></b><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="mso-bidi-font-size:
  10.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><o:p></o:p></span></b></p>
  </td>
  <td width="425" style="width:318.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span lang="EN-US" style="mso-bidi-font-size:10.0pt;
  font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">Google </span><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">클라우드 컴퓨팅 환경에 개인정보 보관<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:6;mso-yfti-lastrow:yes">
  <td width="123" style="width:92.15pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#D9E2F3;mso-background-themecolor:accent1;mso-background-themetint:
  51;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><b style="mso-bidi-font-weight:normal"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  color:black;mso-color-alt:windowtext;mso-font-kerning:0pt">보유 및 이용기간</span></b><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="mso-bidi-font-size:
  10.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><o:p></o:p></span></b></p>
  </td>
  <td width="425" style="width:318.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
  line-height:normal;mso-pagination:widow-orphan;text-autospace:ideograph-numeric ideograph-other;
  word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;
  mso-bidi-font-family:굴림;mso-font-kerning:0pt">회원탈퇴 및 위탁계약 종료시<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
</tbody></table>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:36.0pt;text-align:left;line-height:normal;mso-pagination:
widow-orphan;text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><a style="mso-comment-reference:문우_2;mso-comment-date:20201207T1946;mso-comment-parent:
1"></a><a style="mso-comment-reference:DL_1;mso-comment-date:20201207T1750"></a><span style="mso-comment-continuation:2"><span class="MsoCommentReference"><span lang="EN-US" style="font-size:9.0pt"><!--[if !supportAnnotations]--><a class="msocomanchor" id="_anchor_1" href="file:///C:/Users/kaii/Downloads/DL_40FY_%EA%B0%9C%EC%9D%B8%EC%A0%95%EB%B3%B4%20%EC%B2%98%EB%A6%AC%EB%B0%A9%EC%B9%A8_1209_%EC%B5%9C%EC%A2%85%EB%B3%B8(2).docx#_msocom_1" language="JavaScript" name="_msoanchor_1">[DL1]</a><!--[endif]--><span style="mso-special-character:comment">&nbsp;</span></span></span></span><span class="MsoCommentReference"><span lang="EN-US" style="font-size:9.0pt"><!--[if !supportAnnotations]--><a class="msocomanchor" id="_anchor_2" href="file:///C:/Users/kaii/Downloads/DL_40FY_%EA%B0%9C%EC%9D%B8%EC%A0%95%EB%B3%B4%20%EC%B2%98%EB%A6%AC%EB%B0%A9%EC%B9%A8_1209_%EC%B5%9C%EC%A2%85%EB%B3%B8(2).docx#_msocom_2" language="JavaScript" name="_msoanchor_2"><font face="맑은 고딕">[문우2]</font></a><!--[endif]--><span style="mso-special-character:comment">&nbsp;</span></span></span><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><o:p></o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l4 level1 lfo6;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제5조<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">개인정보 보유 및 이용 기간<span lang="EN-US">) <o:p></o:p></span></span></b></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">원칙적으로 이용자가 서비스에 가입한 날로부터 계정 삭제 또는 해지 시까지 개인정보를 보유 및 이용합니다<span lang="EN-US">.
</span>회원 탈퇴 시 회사는 이용자의 수집된 개인정보가 열람 또는 이용될 수 없도록 파기 처리합니다<span lang="EN-US">. </span>단<span lang="EN-US">, </span>다음과 같이 관계법령의 규정에 의하여 보존할 필요가 있는 경우<span lang="EN-US">, </span>회사는
관계법령에서 정한 일정한 기간 동안 회원정보를 보관합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<table class="MsoTableGrid" border="1" cellspacing="0" cellpadding="0" style="margin-left:6.0pt;border-collapse:collapse;border:none;mso-border-alt:
 solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt">
 <tbody><tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
  <td width="360" style="width:270.2pt;border:solid windowtext 1.0pt;mso-border-alt:
  solid windowtext .5pt;background:#D9E2F3;mso-background-themecolor:accent1;
  mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
  10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><b style="mso-bidi-font-weight:normal"><span style="mso-bidi-font-size:10.0pt;
  font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">해당<span style="color:black;mso-color-alt:windowtext"> 사항</span><span lang="EN-US"><o:p></o:p></span></span></b></p>
  </td>
  <td width="233" style="width:174.55pt;border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#D9E2F3;mso-background-themecolor:accent1;mso-background-themetint:
  51;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
  10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><b style="mso-bidi-font-weight:normal"><span style="mso-bidi-font-size:10.0pt;
  font-family:굴림;mso-bidi-font-family:굴림;color:black;mso-color-alt:windowtext;
  mso-font-kerning:0pt">기간</span></b><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:
  굴림;mso-font-kerning:0pt"><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:1">
  <td width="360" style="width:270.2pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
  10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">관계 법령 위반에 따른 수사∙조사 등이 진행중인 경우<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="233" style="width:174.55pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
  10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">해당 수사·조사 종료시까지<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:2">
  <td width="360" style="width:270.2pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
  10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">서비스 이용에 따른 채권·채무관계가 잔존하는 경우<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="233" style="width:174.55pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
  10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">채권·채무관계 정산시까지<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:3">
  <td width="360" style="width:270.2pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
  10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">원천징수세 납부 기록의 보관을 위하여 필요한 경우<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="233" style="width:174.55pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
  10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">법정신고기한이 지난 날로부터 <span lang="EN-US">5</span>년<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:4">
  <td width="360" style="width:270.2pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
  10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">서비스 이용 관련 로그인 기록<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="233" style="width:174.55pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
  10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:
  굴림;mso-font-kerning:0pt">3</span><span style="mso-bidi-font-size:10.0pt;
  font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">개월<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:5">
  <td width="360" style="width:270.2pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
  10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">표시∙광고에 관한 기록<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="233" style="width:174.55pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
  10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:
  굴림;mso-font-kerning:0pt">6</span><span style="mso-bidi-font-size:10.0pt;
  font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">개월<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:6">
  <td width="360" style="width:270.2pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
  10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">계약 또는 청약철회에 관한 기록<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="233" style="width:174.55pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
  10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:
  굴림;mso-font-kerning:0pt">5</span><span style="mso-bidi-font-size:10.0pt;
  font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">년<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:7">
  <td width="360" style="width:270.2pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
  10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">대금결제 및 재화등의 공급에 관한 기록<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="233" style="width:174.55pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
  10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:
  굴림;mso-font-kerning:0pt">5</span><span style="mso-bidi-font-size:10.0pt;
  font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">년<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:8">
  <td width="360" style="width:270.2pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
  10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">소비자의 불만 또는 분쟁처리에 관한 기록<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="233" style="width:174.55pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
  10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:
  굴림;mso-font-kerning:0pt">3</span><span style="mso-bidi-font-size:10.0pt;
  font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">년<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:9;mso-yfti-lastrow:yes">
  <td width="360" style="width:270.2pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
  10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">전자금융 거래에 관한 기록<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
  <td width="233" style="width:174.55pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="center" style="mso-margin-top-alt:auto;margin-bottom:
  10.0pt;text-align:center;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="mso-bidi-font-size:10.0pt;font-family:굴림;mso-bidi-font-family:
  굴림;mso-font-kerning:0pt">5</span><span style="mso-bidi-font-size:10.0pt;
  font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">년<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
</tbody></table>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l4 level1 lfo6;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제6조<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">개인정보 파기 절차 및 방법<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l3 level1 lfo12;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 원칙적으로 이용자가 회원 탈퇴 시 회사는 특정 보호 기간을
거쳐 이용자의 수집된 개인정보가 열람 또는 이용될 수 없도록 파기 처리합니다<span lang="EN-US">. </span>단<span lang="EN-US">, </span>이용자에게 개인정보 보관기간에 대해 별도의 동의를 얻은 경우<span lang="EN-US">, </span>또는
법령에서 일정 기간 정보 보관 의무를 부과하는 경우에는 해당 기간 동안 개인정보를 안전하게 보관합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l3 level1 lfo12;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">개인정보 파기의 절차 및 방법은 다음과 같습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<ol start="1" type="1">
 <li class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
     text-align:left;line-height:normal;mso-pagination:widow-orphan;mso-list:
     l5 level1 lfo4;tab-stops:list 36.0pt;text-autospace:ideograph-numeric ideograph-other;
     word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;
     mso-bidi-font-family:굴림;mso-font-kerning:0pt">파기 절차<span lang="EN-US"> : </span>이용자가
     입력한 정보는 목적 달성 후 별도의<span lang="EN-US"> DB</span>에 옮겨져<span lang="EN-US"> (</span>종이의
     경우 별도의 서류<span lang="EN-US">) </span>내부 방침 및 기타 관련 법령에 따라 일정기간 저장된 후 혹은 즉시 파기됩니다<span lang="EN-US">. </span>이 때<span lang="EN-US">, DB</span>로 옮겨진 개인정보는 법률에 의한 경우가 아니고서는
     다른 목적으로 이용되지 않습니다<span lang="EN-US"><o:p></o:p></span></span></li>
 <li class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
     text-align:left;line-height:normal;mso-pagination:widow-orphan;mso-list:
     l5 level1 lfo4;tab-stops:list 36.0pt;text-autospace:ideograph-numeric ideograph-other;
     word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;
     mso-bidi-font-family:굴림;mso-font-kerning:0pt">파기 방법<span lang="EN-US"> : </span>종이에
     출력된 개인정보는 분쇄기로 분쇄하거나 소각을 통하여 파기합니다<span lang="EN-US">. </span>전자적 파일 형태로 저장된
     개인정보는 기록을 재생할 수 없는 기술적인 방법을 사용하여 삭제합니다<span lang="EN-US">.<o:p></o:p></span></span></li>
</ol>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:36.0pt;text-align:left;line-height:normal;mso-pagination:
widow-orphan;text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l4 level1 lfo6;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제7조<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">개인정보의 수집<span lang="EN-US">/</span>이용<span lang="EN-US">/</span>제공 동의 철회<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">이용자가 회원가입 등을 통해 개인정보의 수집<span lang="EN-US">/</span>이용<span lang="EN-US">/</span>제공에
대해 동의하신 내용을 언제든지 철회할 수 있습니다<span lang="EN-US">. </span>동의 철회는 관리 책임자에게 전자적 통신수단<span lang="EN-US">(</span>이메일 등<span lang="EN-US">) </span>및 서비스 상의 기능 등을 이용하여 신청할 수 있으며<span lang="EN-US">, </span>회사는 이를 확인 후 회원 탈퇴를 위해 필요한 조치를 취합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l4 level1 lfo6;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제8조<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;
font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">개인정보 자동 수집 장치의 설치<span lang="EN-US">/</span>운영 및 거부에 관한 사항<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l15 level1 lfo13;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 이용자에게 개별적인 맞춤서비스를 제공하기 위해 이용정보를
저장하고 수시로 불러오는<span lang="EN-US"> ‘</span>쿠키<span lang="EN-US">(cookie)’</span>를 사용합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l15 level1 lfo13;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">쿠키는 웹사이트를 운영하는데 이용되는 서버<span lang="EN-US">(http)</span>가 이용자의 컴퓨터 브라우저에게 보내는 소량의 정보이며 이용자들의<span lang="EN-US">
PC </span>컴퓨터내의 하드디스크에 저장되기도 합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l15 level2 lfo13;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">1.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">쿠키의 사용목적<span lang="EN-US">: </span>이용자가
방문한 각 서비스와 웹 사이트들에 대한 방문 및 이용형태<span lang="EN-US">, </span>인기 검색어<span lang="EN-US">, </span>보안접속 여부<span lang="EN-US">, </span>등을 파악하여 이용자에게 최적화된 정보 제공을 위해
사용됩니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l15 level2 lfo13;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">쿠키의 설치<span lang="EN-US">∙</span>운영
및 거부<span lang="EN-US">: </span>웹브라우저 상단의 도구<span lang="EN-US">&gt;</span>인터넷 옵션<span lang="EN-US">&gt;</span>개인정보 메뉴의 옵션 설정을 통해 쿠키 저장을 거부 할 수 있습니다<span lang="EN-US">. <o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:40.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l15 level2 lfo13;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">3.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">쿠키 저장을 거부할 경우 원활한 서비스 이용에 어려움이 발생할
수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;line-height:normal;mso-pagination:
widow-orphan;text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" style="margin-left:20.0pt;mso-para-margin-left:0gd;
text-indent:-20.0pt;mso-list:l4 level1 lfo6"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제9조<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp; </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;
line-height:107%;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">개인정보보호를
위한 기술적<span lang="EN-US">/</span>관리적 대책<span lang="EN-US">) </span></span></b><span lang="EN-US" style="font-size:12.0pt;line-height:107%;font-family:굴림;mso-bidi-font-family:
굴림;mso-font-kerning:0pt"><o:p></o:p></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">회사는 이용자의 개인정보를 보호하기 위해 기술적 대책과 관리적 대책을 마련하고 있으며<span lang="EN-US">, </span>이를
적용하고 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l7 level1 lfo14;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">개인정보 암호화<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">회사의 회원 아이디<span lang="EN-US">(ID)</span>의 비밀번호는 암호화되어 저장 및 관리되고 있어 본인만이 알고 있으며<span lang="EN-US">, </span>개인정보의 확인 및 변경도 비밀번호를 알고 있는 본인에 의해서만 가능합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l7 level1 lfo14;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">해킹 등에 대비한 대책<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">회사는 해킹이나 컴퓨터 바이러스 등에 의해 회원의 개인정보가 유출되거나 훼손되는 것을 막기 위해 최선을 다하고 있습니다<span lang="EN-US">. </span>개인정보의 훼손에 대비해서 자료를 수시로 백업하고 있고<span lang="EN-US">, </span>최신 백신프로그램을
이용하여 이용자들의 개인정보나 자료가 누출되거나 손상되지 않도록 방지하고 있으며<span lang="EN-US">, </span>암호화통신 등을 통하여
네트워크상에서 개인정보를 안전하게 전송할 수 있도록 하고 있습니다<span lang="EN-US">. </span>그리고 침입차단시스템을 이용하여
외부로부터의 무단 접근을 통제하고 있으며<span lang="EN-US">, </span>기타 시스템적으로 보안성을 확보하기 위한 가능한 모든 기술적
장치를 갖추려 노력하고 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l7 level1 lfo14;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">취급 직원의 최소화 및 교육<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">회사의 개인정보관련 취급 직원은 담당자에 한정시키고 있고 이를 위한 별도의 접근 계정을 부여하여 정기적으로 갱신하고 있으며<span lang="EN-US">, </span>담당자에 대한 수시 교육을 통하여 회사의 개인정보처리방침의 준수를 항상 강조하고 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l7 level1 lfo14;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">④<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">개인정보보호전담기구의 운영<span lang="EN-US"><o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">사내 개인정보보호전담기구 등을 통하여 회사의 개인정보처리방침 이행사항 및 담당자의 준수여부를 확인하여 문제가 발견될 경우 즉시 수정하고
바로잡을 수 있도록 노력하고 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l4 level1 lfo6;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제10조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">이용자의 권리<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l2 level1 lfo16;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">①<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">정보주체는 회사에 대해 언제든지 개인정보 열람<span lang="EN-US">∙</span>정정<span lang="EN-US">∙</span>삭제<span lang="EN-US">∙</span>처리정지 요구
등의 권리를 행사할 수 있습니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l2 level1 lfo16;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">②<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">제<span lang="EN-US">1</span>항에 따른 권리
행사는 회사에 대해 서면<span lang="EN-US">, </span>전자우편<span lang="EN-US">, </span>모사전송<span lang="EN-US">(FAX) </span>등을 통하여 하실 수 있으며<span lang="EN-US">, </span>회사는 이에 대해 지체없이
조치하겠습니다<span lang="EN-US">. <o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l2 level1 lfo16;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">③<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">제<span lang="EN-US">1</span>항에 따른 권리
행사는 정보주체의 법정대리인이나 위임을 받은 자 등 대리인을 통하여 하실 수 있습니다<span lang="EN-US">. </span>이 경우 회사에
위임장을 제출하셔야 합니다<span lang="EN-US">. <o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l2 level1 lfo16;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">④<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">개인정보 열람 및 처리정지 요구는 개인정보보호법에 의하여 정보주체의
권리가 제한될 수 있습니다<span lang="EN-US">. <o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l2 level1 lfo16;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑤<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">개인정보의 정정 및 삭제 요구는 다른 법령에서 그 개인정보가
수집 대상으로 명시되어 있는 경우에는 그 삭제를 요구할 수 없습니다<span lang="EN-US">. <o:p></o:p></span></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l2 level1 lfo16;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">⑥<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">회사는 정보주체 권리에 따른 열람의 요구<span lang="EN-US">, </span>정정<span lang="EN-US">·</span>삭제의 요구<span lang="EN-US">, </span>처리정지의
요구 시 열람 등 요구를 한 자가 본인이거나 정당한 대리인인지를 확인합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l4 level1 lfo6;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제11조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">개인 정보 관련 담당자 연락처<span lang="EN-US">) <o:p></o:p></span></span></b></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">이용자는 회사의 서비스를 이용하며 발생하는 모든 개인정보보호 관련 민원을 개인정보관리책임자 혹은 담당부서로 신고하실 수 있습니다<span lang="EN-US">. </span>회사는 회원의 신고사항에 대해 신속하게 답변을 드릴 것입니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<table class="MsoTableGrid" border="1" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt">
 <tbody><tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
  <td width="601" colspan="2" valign="top" style="width:450.8pt;border:solid windowtext 1.0pt;
  mso-border-alt:solid windowtext .5pt;background:#D9E2F3;mso-background-themecolor:
  accent1;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
  0pt">개인정보보호책임자<span lang="EN-US"><o:p></o:p></span></span></b></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:1">
  <td width="113" valign="top" style="width:84.8pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
  0pt">이름<span lang="EN-US"><o:p></o:p></span></span></b></p>
  </td>
  <td width="488" valign="top" style="width:366.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
  0pt">문우리<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:2">
  <td width="113" valign="top" style="width:84.8pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
  0pt">소속<span lang="EN-US"><o:p></o:p></span></span></b></p>
  </td>
  <td width="488" valign="top" style="width:366.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">40FY</span><span style="font-size:12.0pt;font-family:
  굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">㈜<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:3">
  <td width="113" valign="top" style="width:84.8pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
  0pt">직위<span lang="EN-US"><o:p></o:p></span></span></b></p>
  </td>
  <td width="488" valign="top" style="width:366.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
  0pt">대표이사<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:4">
  <td width="113" valign="top" style="width:84.8pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
  0pt">전화번호<span lang="EN-US"><o:p></o:p></span></span></b></p>
  </td>
  <td width="488" valign="top" style="width:366.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">070-7938-4009<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:5">
  <td width="113" valign="top" style="width:84.8pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
  0pt">이메일<span lang="EN-US"><o:p></o:p></span></span></b></p>
  </td>
  <td width="488" valign="top" style="width:366.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">woori_moon@40fy.us<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:6">
  <td width="601" colspan="2" valign="top" style="width:450.8pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#D9E2F3;mso-background-themecolor:accent1;mso-background-themetint:
  51;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;color:black;
  mso-color-alt:windowtext;mso-font-kerning:0pt">개인정보관리담당자</span></b><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt"><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:7">
  <td width="113" valign="top" style="width:84.8pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
  0pt">이름<span lang="EN-US"><o:p></o:p></span></span></b></p>
  </td>
  <td width="488" valign="top" style="width:366.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
  0pt">문우리<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:8">
  <td width="113" valign="top" style="width:84.8pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
  0pt">소속<span lang="EN-US"><o:p></o:p></span></span></b></p>
  </td>
  <td width="488" valign="top" style="width:366.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">40FY</span><span style="font-size:12.0pt;font-family:
  굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">㈜<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:9">
  <td width="113" valign="top" style="width:84.8pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
  0pt">직위<span lang="EN-US"><o:p></o:p></span></span></b></p>
  </td>
  <td width="488" valign="top" style="width:366.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
  0pt">대표이사<span lang="EN-US"><o:p></o:p></span></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:10">
  <td width="113" valign="top" style="width:84.8pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
  0pt">전화번호<span lang="EN-US"><o:p></o:p></span></span></b></p>
  </td>
  <td width="488" valign="top" style="width:366.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">070-7938-4009<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:11;mso-yfti-lastrow:yes">
  <td width="113" valign="top" style="width:84.8pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><b><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
  0pt">이메일<span lang="EN-US"><o:p></o:p></span></span></b></p>
  </td>
  <td width="488" valign="top" style="width:366.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:left;line-height:normal;mso-pagination:widow-orphan;
  text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
  mso-font-kerning:0pt">woori_moon@40fy.us<o:p></o:p></span></p>
  </td>
 </tr>
</tbody></table>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l4 level1 lfo6;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제12조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">권익침해 구제방법<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">기타 개인정보침해에 대한 신고나 상담이 필요하신 경우에는 아래 기관에 문의하시기 바랍니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<ol start="1" type="1">
 <li class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
     text-align:left;line-height:normal;mso-pagination:widow-orphan;mso-list:
     l13 level1 lfo5;tab-stops:list 36.0pt;text-autospace:ideograph-numeric ideograph-other;
     word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;
     mso-bidi-font-family:굴림;mso-font-kerning:0pt">개인정보침해신고센터<span lang="EN-US">
     (</span></span><span lang="EN-US"><a href="http://www.118.or.kr/"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;color:blue;
     mso-font-kerning:0pt">http://www.118.or.kr</span></a></span><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:
     굴림;mso-font-kerning:0pt"> / </span><span style="font-size:12.0pt;
     font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">국번없이<span lang="EN-US"> 118)<o:p></o:p></span></span></li>
 <li class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
     text-align:left;line-height:normal;mso-pagination:widow-orphan;mso-list:
     l13 level1 lfo5;tab-stops:list 36.0pt;text-autospace:ideograph-numeric ideograph-other;
     word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;
     mso-bidi-font-family:굴림;mso-font-kerning:0pt">대검찰청 사이버 범죄 수사단<span lang="EN-US"> (</span></span><span lang="EN-US"><a href="http://www.spo.go.kr/"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;color:blue;
     mso-font-kerning:0pt">http://www.spo.go.kr</span></a></span><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:
     굴림;mso-font-kerning:0pt"> / 02-3480-2000)<o:p></o:p></span></li>
 <li class="MsoNormal" style="mso-margin-top-alt:auto;margin-bottom:10.0pt;
     text-align:left;line-height:normal;mso-pagination:widow-orphan;mso-list:
     l13 level1 lfo5;tab-stops:list 36.0pt;text-autospace:ideograph-numeric ideograph-other;
     word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;
     mso-bidi-font-family:굴림;mso-font-kerning:0pt">경찰청 사이버테러대응센터<span lang="EN-US"> (</span></span><span lang="EN-US"><a href="http://www.ctrc.go.kr/"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;color:blue;
     mso-font-kerning:0pt">http://www.ctrc.go.kr</span></a></span><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:
     굴림;mso-font-kerning:0pt"> / 1566-0112)<o:p></o:p></span></li>
</ol>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l4 level1 lfo6;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제13조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">기타<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">회사에 링크되어 있는 웹사이트들이 개인정보를 수집하는 행위에 대해서는 본<span lang="EN-US"> "</span>회사 개인정보처리방침<span lang="EN-US">"</span>이 적용되지 않음을 알려 드립니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoListParagraph" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;margin-left:20.0pt;mso-margin-top-alt:auto;mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0gd;text-align:left;text-indent:-20.0pt;line-height:normal;
mso-pagination:widow-orphan;mso-list:l4 level1 lfo6;text-autospace:ideograph-numeric ideograph-other;
word-break:keep-all"><!--[if !supportLists]--><b><span lang="EN-US" style="font-size:
12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt"><span style="mso-list:Ignore">제14조<span style="font:7.0pt &quot;Times New Roman&quot;"> </span></span></span></b><!--[endif]--><b><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">(</span></b><b><span style="font-size:12.0pt;font-family:
굴림;mso-bidi-font-family:굴림;mso-font-kerning:0pt">고지의 의무<span lang="EN-US">)<o:p></o:p></span></span></b></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">현 회사 개인정보처리방침에 내용이 추가되거나 삭제 및 수정이 있을 시 개정 최소<span lang="EN-US"> 7</span>일 전부터
홈페이지의<span lang="EN-US"> ‘</span>공지사항<span lang="EN-US">’ </span>및 다양한 방법으로 고지할 것입니다<span lang="EN-US">. </span>단<span lang="EN-US">, </span>개인정보의 수집 및 활용<span lang="EN-US">, </span>제<span lang="EN-US">3</span>자 제공 등과 같이 이용자 권리의 중요한 변경이 있을 경우에는 최소<span lang="EN-US"> 30</span>일
전에 고지합니다<span lang="EN-US">.<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt">[</span><span style="font-size:12.0pt;font-family:굴림;
mso-bidi-font-family:굴림;mso-font-kerning:0pt">부칙<span lang="EN-US">]<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">공고일자<span lang="EN-US"> : 2020.12.09<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;mso-font-kerning:
0pt">시행일자<span lang="EN-US"> : 2020.12.09<o:p></o:p></span></span></p>

<p class="MsoNormal" align="left" style="mso-margin-top-alt:auto;margin-bottom:
10.0pt;text-align:left;line-height:normal;mso-pagination:widow-orphan;
text-autospace:ideograph-numeric ideograph-other;word-break:keep-all"><span lang="EN-US" style="font-size:12.0pt;font-family:굴림;mso-bidi-font-family:굴림;
mso-font-kerning:0pt"><o:p>&nbsp;</o:p></span></p>

<div style="mso-element:comment-list"><!--[if !supportAnnotations]-->


</div><br></div>
</div>
