<div class="row text-center">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mind-scan-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mind-scan-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<?php echo $layout->breadcrumbs?>

<div class="row text-center mb30">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mindle-result-text-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mindle-result-text-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<div class="row mindle-results">
   <div class="col-lg-12 mb30">
        <p class="title"><span class="text-color2">▶</span> 내 마음 속 악동은?</p>
   </div>
   <?php foreach ($result['svr_classify'] as $idx => $value) { ?>
        <div class="col-lg-12 mb50 item">
            <a href="<?php echo base_url('mindscan/mindleResult')?>?type=<?php echo $value?>">
                <img src="<?php echo base_url('assets/user/img/mindle-result-'.$value.'.png')?>" class="w100p pc"/>
                <img src="<?php echo base_url('assets/user/img/mobile/mindle-result-'.$value.'.png')?>" class="w100p mobile"/>
            </a>
        </div>
   <?php } ?>
   <div class="col-lg-12 mt10">
        <p class="title"><span class="text-color2">▶</span> MINDLE은 어떤 서비스인가요?</p>
        <div class="p30">
            <video controlsList="nodownload" controls width="100%" poster="<?php echo base_url('assets/user/img/mindle-opening.png')?>">
                <source src="https://40fy.s3.ap-northeast-2.amazonaws.com/video/mindle_opening.mp4" type="video/mp4">
            </video>
        </div>
   </div>
   <div class="col-lg-12 text-center mt30 mb100">
        <button type="button" class="button4" onclick="location.href='<?php echo base_url('mindle/type')?>'">MINDLE 시작하기</button>
    </div>
</div>