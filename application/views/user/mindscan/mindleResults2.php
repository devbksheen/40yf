<div class="row text-center">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mind-scan-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mind-scan-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<?php echo $layout->breadcrumbs?>

<div class="row text-center mb30">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mindle-result-text-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mindle-result-text-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<div class="row mindle-results">
   <div class="col-lg-12 mb30">
        <p class="title"><span class="text-color2">▶</span> 내 마음 속 악동은? <span class="small">(* 악동 캐릭터를 클릭 시 상세한 설명을 확인하실 수 있습니다.)</span></p>
   </div>
   <div class="col-lg-12">
        <?php $charArr = array('A', 'B', 'E', 'C', 'D');?>
        <?php foreach ($charArr as $idx => $value) { ?>
            <div class="char-area">
                <?php if (is_array($result['svr_classify']) && in_array($value, $result['svr_classify'])) { ?>
                    <a href="<?php echo base_url('mindscan/mindleResult')?>?type=<?php echo $value?>">
                        <img src="<?php echo base_url('assets/user/img/mindle-result/'.$value.'_color.png')?>" class="w90p pc"/>
                        <img src="<?php echo base_url('assets/user/img/mobile/mindle-result/'.$value.'_color.png')?>" class="w90p mobile"/>
                    </a>
                <?php } else { ?>
                    <img src="<?php echo base_url('assets/user/img/mindle-result/'.$value.'_gray.png')?>" class="w90p pc"/>
                    <img src="<?php echo base_url('assets/user/img/mobile/mindle-result/'.$value.'_gray.png')?>" class="w90p mobile"/>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row mindle-results">
    <div class="col-lg-12">
        <p class="title"><span class="text-color2">▶</span> 내 마음 속 악동의 파워는?</p>
        <div id="graph" class="graph-area mb50">
        </div>
    </div>
    <div class="col-lg-12 mb70">
        <img src="<?php echo base_url('assets/user/img/mindle-result/result-detail.png')?>" class="pc w100p"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mindle-result/result-detail.png')?>" class="mobile w100p"/>
    </div>
    <div class="col-lg-12">
        <p class="title"><span class="text-color2">▶</span> MINDLE은 어떤 서비스인가요?</p>
        <div class="p30">
            <video controlsList="nodownload" controls width="100%" poster="<?php echo base_url('assets/user/img/mindle-opening.png')?>">
                <source src="https://40fy.s3.ap-northeast-2.amazonaws.com/video/mindle_opening.mp4" type="video/mp4">
            </video>
        </div>
   </div>
   <div class="col-lg-12 text-center mt30 mb100">
        <button type="button" class="button4" onclick="location.href='<?php echo base_url('mindle/type')?>'">MINDLE 시작하기</button>
    </div>
</div>

<script>
    $(document).ready(function($) {
        $('#graph').dvstr_graph({
            points: [
                {
                    title: '건강',
                    color: '#11b5ff'
                },
                {
                    title: '보통',
                    color: '#b7e419'
                },
                {
                    title: '주의',
                    color: '#ff9829'
                },
                {
                    title: '심각',
                    color: '#f1471a'
                }
            ],
            graphs: [
                <?php foreach ($charArr as $idx => $value) { ?>
                    <?php 
                        $score = $result['scoreArr'][$value]; 
                        if ($score < 4.4) {
                            $color = '#11b5ff';
                        } else if ($score < 6.8) {
                            $color = '#b7e419';
                        } else if ($score < 8) {
                            $color = '#ff9829';
                        } else {
                            $color = '#f1471a';
                        }
                    ?>
                    {
                        label: '<?php echo $this->survey_lib->getCharacter($value)?>',
                        value: [<?php echo $score?>],
                        color: ['<?php echo $color?>']
                    },
                <?php } ?>
            ],
            grid_wmax: 10
        });
    });
</script>
