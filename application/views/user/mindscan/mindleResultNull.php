<div class="row text-center">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mind-scan-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mind-scan-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<?php echo $layout->breadcrumbs?>

<div class="row text-center mb50">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mindle-result-text-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mindle-result-text-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<div class="row mindle-results">
    <div class="col-lg-12 mb20">
        <p class="title"><span class="text-color2">▶</span> 내 마음 속 악동은?</p>
    </div>
    <div class="col-lg-12 mb40 item">
        <img src="<?php echo base_url('assets/user/img/mindle-no-result.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mindle-no-result.png')?>" class="w100p mobile"/>
    </div>
    <div class="col-lg-12 pl30 pr30 pb40" style="border-bottom: solid 1px #898989;">
        <p class="mb30">소소한 스트레스는 가볍게 넘기고 잘 지내고 계신가요? 힘든 일을 겪어도, 건강하고 유연하게 대처하면서 중심을
        되찾고 스트레스를 회복하는 마음의 자원을 비교적 잘 갖추고 게신것 같아요.</p>

        <p class="mb30">지금처럼 내 마음의 악동들을 잘 관리하면서! 건강한 마음을 잘 가꾸어 나가시길 바랍니다^^<p>

        <p class="mb30">그런데 “어? 나 사실 스트레스도 크고 마음도 힘든데, 왜 악동이 하나도 없지?” 라는 생각이 드실 수 있어요.
        이러한 경우는 평소에는 비교적 괜찮았는데, 최근에 나를 힘들게 하는 특정 외부적인 사건이나 스트레스 경험이
        있을수 있어요.</p>

        <p class="mb30">내 안에 나의 삶을 반복적으로 뒤흔드는 분명한 악동의 모습이 있지는 않지만, 특별한 이유로 인해서 일시적으로
        마음이 힘든 상황일 수 있는 것이죠.</p>

        <p class="mb30">또 다른 경우는, 사람의 마음 안에는 스트레스를 유발하는 다양한 패턴이 존재하는데, 이번 마인들에는 그 중에서
        <span class="text-color2">대표적인 5가지 유형</span>을 소개해 드리는 것이기 때문에 당신의 마음 속 악동을 찾지 못한 것일 수도 있어요~</p>

        <p class="mb40">마인들에서 앞으로 더 다양한 유형의 악동들을 다루어나갈 예정입니다! <span class="text-color2">나의 특별한 악동이야기</span>를 들려주고
        싶으시다면, 내게 반복되는 스트레스에 대해서 마인들에 남겨주세요!</p>

        <form id="noResultSendForm" method="POST">
            <input type="hidden" name="svr_id" value="<?php echo $result['svr_id']?>"/>
            <textarea class="w100p no-result-textarea mb40" name="svr_classify_txt" maxlength="1000"><?php echo element(0, $result['svr_classify_txt'])?></textarea>
            <div class="text-right">
                <button class="button2 mb40">내 악동이야기 보내기</button>
            </div>
        </form>
    </div>
    <div class="col-lg-12 mt60">
        <p class="title"><span class="text-color2">▶</span> 내 마음 속 악동의 파워는?</p>
    </div>
    <div class="col-lg-12">
        <div id="graph" class="graph-area mb50">
        </div>
    </div>
    <div class="col-lg-12 mb70">
        <img src="<?php echo base_url('assets/user/img/mindle-result/result-detail.png')?>" class="pc w100p"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mindle-result/result-detail.png')?>" class="mobile w100p"/>
    </div>
    <div class="col-lg-12">
        <p class="title"><span class="text-color2">▶</span> MINDLE은 어떤 서비스인가요?</p>
        <div class="p30">
            <video controlsList="nodownload" controls width="100%" poster="<?php echo base_url('assets/user/img/mindle-opening.png')?>">
                <source src="https://40fy.s3.ap-northeast-2.amazonaws.com/video/mindle_opening.mp4" type="video/mp4">
            </video>
        </div>
    </div>
    <div class="col-lg-12 text-center mt30 mb100">
        <button type="button" class="button4" onclick="location.href='<?php echo base_url('mindle/type')?>'">MINDLE 시작하기</button>
    </div>
</div>
<?php $charArr = array('A', 'B', 'E', 'C', 'D');?>
<script>
    $(document).ready(function($) {
        $('#graph').dvstr_graph({
            points: [
                {
                    title: '건강',
                    color: '#11b5ff'
                },
                {
                    title: '보통',
                    color: '#b7e419'
                },
                {
                    title: '주의',
                    color: '#ff9829'
                },
                {
                    title: '심각',
                    color: '#f1471a'
                }
            ],
            graphs: [
                <?php foreach ($charArr as $idx => $value) { ?>
                    <?php 
                        $score = $result['scoreArr'][$value]; 
                        if ($score < 4.4) {
                            $color = '#11b5ff';
                        } else if ($score < 6.8) {
                            $color = '#b7e419';
                        } else if ($score < 8) {
                            $color = '#ff9829';
                        } else {
                            $color = '#f1471a';
                        }
                    ?>
                    {
                        label: '<?php echo $this->survey_lib->getCharacter($value)?>',
                        value: [<?php echo $score?>],
                        color: ['<?php echo $color?>']
                    },
                <?php } ?>
            ],
            grid_wmax: 10
        });
    });
</script>