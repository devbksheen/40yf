<div class="row text-center">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mind-scan-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mind-scan-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<?php echo $layout->breadcrumbs?>

<div class="row text-center mb30">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mind-ab-result-text-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mind-ab-result-text-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<div class="row mind-ab-result">
    <div class="col-lg-5 mb20">
        <div class="result-img-area">
            <img src="<?php echo base_url('assets/user/img/result'.$result['svr_classify'].'.png')?>" class="w100p"/>
        </div>
    </div>
    <div class="col-lg-7 mb40">
        <p class="result-title mb30">현재 마음은 '<?php echo $result['svr_classify_txt']?>' 상태입니다.</p>
        <?php if ($result['svr_classify'] == 'A') { ?>
            <div class="result-contents">
                <p>스트레스를 잘 관리하고 계시네요!</p>
                <p>검사결과상 유의미한 수준의 우울감은 관찰되지 않습니다. 스스로의 마음 건강을 잘 지켜내고 계신 당신에게 박수!</p>
                <p>앞으로도 Mind-Ab 프로그램과 함께 지금처럼 내 마음을 잘 관리해주세요.</p>
            </div>
        <?php } else if ($result['svr_classify'] == 'B') { ?>
            <div class="result-contents">
                <p>우울감이 관찰되나 일상생활에 유의미한 지장을 초래할 정도는 아닙니다.</p>
                <p>그러나, 이러한 수준의 우울감이 장기간 지속된다면 대인관계나 개인의 능력 발휘 저하가 초래될 수 있습니다.</p>
                <p>내 마음이 더 오랜기간 힘들어지거나, 내 마음의 어려움이 더 심해지지 않도록 보살펴주세요!</p>
                <p>Mind-Ab에서 알려드리는 마음관리 방법들을 확용해볼까요?</p>
            </div>
        <?php } else if ($result['svr_classify'] == 'C') { ?>
            <div class="result-contents">
                <p>혼자 버티느라 힘겨울 당신... 무거운 마음을 나누어 보아요!</p>
                <p>
                    중등도의 우울감이 관찰되며, 이 정도의 우울감은 당신의 일상생활에 영향을 주게됩니다. 
                    가까운 전문기관을 방문하여 보다 정밀한 평가와 도움을 받아보시기를 권고합니다.
                </p>
                <p>Mind-Ab 프로그램에서 알려드리는 마음건강 관리법을 적극 활용해보세요! </p>
                <p>
                    성남시의료원 임직원분들의 경우, 성남시의료원 정신건강의학과(031-738-7180)
                    외래에서 개인상담이 가능하니 연락주세요. 
                </p>
            </div>
        <?php } else if ($result['svr_classify'] == 'D') { ?>
            <div class="result-contents">
                <p>당신을 위해서 빨리 행동하셔야 해요! 심각한 수준의 우울감이 관찰됩니다.</p>
                <p>
                    현재 당신의 마음은 지치고 힘든 상태입니다. 힘겨워하는 당신을 위해서 가까운
                    전문기관을 방문하여 전문가의 도움을 받으시길 강력히 권고합니다.
                </p>
                <p>
                    오늘의 나를 구할 수 있는건 나 자신이란걸 잊지마세요!
                    또한, Mind-Ab 프로그램에서 알려드리는 마음건강관리법을 활용하여 내 마음을
                    돌보아보세요!
                </p> 
                <p>
                    성남시의료원 임직원분들의 경우, 성남시의료원 정신건강의학과(031-738-7180)
                    외래에서 개인상담이 가능하니 연락주세요.
                </p>
            </div>
        <?php } ?>
    </div>
    <div class="col-lg-12 mt40 mb70 text-right">
        <button type="button" class="button1" onclick="location.href='<?php echo base_url('mindab')?>'">Mind-Ab 바로가기</button>
    </div>
</div>