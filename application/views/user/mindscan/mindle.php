<div class="row text-center">
    <div class="col-lg-12 p0">
        <img src="<?php echo base_url('assets/user/img/mind-scan-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mind-scan-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<?php echo $layout->breadcrumbs?>

<div class="row text-center mb30">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mindle-check-text-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mindle-check-text-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<div class="row">
    <form id="surveyForm" method="POST">
        <input type="hidden" name="type" value="mindle"/>
        <input type="hidden" name="step" value="<?php echo $step?>"/>
        <?php if ($step == 2) { ?>
            <?php foreach ($post as $key => $value) { ?>
                <input type="hidden" name="<?php echo $key?>" value="<?php echo $value?>"/>
            <?php } ?>
        <?php } ?>
        <div class="col-lg-12 mb40">
            <div class="table-area">
                <table class="table1">
                    <colgroup>
                        <col width="7%"/>
                        <col/>
                        <col width="9%"/>
                        <col width="9%"/>
                        <col width="9%"/>
                        <col width="9%"/>
                        <col width="9%"/>
                    </colgroup>
                    <tr>
                        <th>구분</th>
                        <th>문항</th>
                        <th>전혀<br/>아니다<br/>(1점)</th>
                        <th>별로<br/>아니다<br/>(2점)</th>
                        <th>어느쪽도<br/>아니다<br/>(3점)</th>
                        <th>다소<br/>그렇다<br/>(4점)</th>
                        <th>매우<br/>그렇다<br/>(5점)</th>
                    </tr>
                    <?php foreach ($question as $key => $item) { ?>
                        <tr>
                            <td class="text-center"><?php echo $step == 1 ? ($key+1) : 25 + ($key+1) ?></td>
                            <td><?php echo $item['question']?></td>
                            <?php foreach ($item['score'] as $idx => $score) { ?>
                                <td class="text-center">
                                    <div class="checkbox1">
                                        <input type="checkbox" name="<?php echo $item['name']?>" class="radio-checkbox" value="<?php echo $item['diagram'].':'.$score?>"/>
                                    </div>
                                </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
        <div class="col-lg-12 mb60">
            <?php if ($step == 2) { ?>
                <button class="button3 fl" type="button" onclick="history.back(-1)">이전으로</button>
            <?php } ?>
            <button class="button2 fr" type="button" onclick="formSubmit()"><?php echo $buttonText?></button>
        </div>
    </form>
</div>

<script>
function formSubmit()
{
    var Length = $(".radio-checkbox").length;   
    var chkdLength = $(".radio-checkbox:checked").length;   
    if (Length/5 == chkdLength) {
        $("#surveyForm").submit();
    } else {
        alert("모든 문항에 답변을 체크해주세요.");
    }
}
</script>