<div class="row text-center">
    <div class="col-lg-12 p0">
        <img src="<?php echo base_url('assets/user/img/mind-scan-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mind-scan-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<?php echo $layout->breadcrumbs?>

<div class="row text-center mb50">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mindle-add-info-text-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mindle-add-info-text-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<div class="add-info">
    <div class="row text-center mb50">
        <div class="col-lg-12">
            <p>정확한 결과분석을 위해 아래 정보를 추가로 기입해주세요.</p>
            <p>해당 정보는 <span class="text-color2">개인 맞춤형 데이터 분석</span>을 위해 사용됩니다.</p>
        </div>
    </div>
    <div class="row mb110">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-8 input-area pt40 pb40">
            <form id="addInfoForm" method="POST">
                <div class="row form-group mb30">
                    <div class="col-sm-3">
                        <label class="input-label" for="user_name">이름</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="user_name" name="user_name" maxlength="10" style="width: 270px;" value="<?php echo $user['user_name']?>">
                    </div>
                </div>
    
                <div class="row form-group mb30">
                    <div class="col-sm-3">
                        <label class="input-label" for="user_gender">성별</label>
                    </div>
                    <div class="col-sm-9 radio-group">
                        <input type="radio" id="user_gender_M" name="user_gender" value="M" <?php echo $user['user_gender'] == 'M' ? 'checked':'';?>/>
                        <label class="mr10" for="user_gender_M">남자</label>
    
                        <input type="radio" id="user_gender_W" name="user_gender" value="W" <?php echo $user['user_gender'] == 'W' ? 'checked':'';?>/>
                        <label for="user_gender_W">여자</label>
                    </div>
                </div>
    
                <div class="row form-group mb0">
                    <div class="col-sm-3">
                        <label class="input-label" for="user_birth">생년월일</label>
                    </div>
                    <div class="col-sm-9 birth-group">
                        <input type="text" name="user_birth[]" class="form-control birth-input1" onlynum maxlength="4" placeholder="YYYY" value="<?php echo element(0, $user['user_birth'])?>"/> 년
                        <input type="text" name="user_birth[]" class="form-control birth-input2" onlynum maxlength="2" placeholder="MM" value="<?php echo element(1, $user['user_birth'])?>"/> 월
                        <input type="text" name="user_birth[]" class="form-control birth-input2" onlynum maxlength="2" placeholder="DD" value="<?php echo element(2, $user['user_birth'])?>"/> 일
                    </div>
                </div>
            </form>
        </div>
        <div class="col-lg-2">
        </div>
    </div>
    <div class="row mb70">
        <div class="col-lg-12 text-right">
            <button class="button2" type="button" onclick="formSubmit()">결과 보러가기</button>
        </div>
    </div>
</div>

<script>
function formSubmit()
{
    var user_name = $('#user_name');
    if (!$.trim(user_name.val())) {
        alert("이름을 입력해주세요.");
        user_name.focus();
        return false;
    }
    var user_gender = $('[name="user_gender"]:checked');
    if (user_gender.length == 0) {
        alert("성별을 선택해주세요.");
        $('[name="user_gender"]:first').focus();
        return false;
    }
    var user_birth = $('[name="user_birth[]"]');
    if (user_birth.length > 0) {
        var rs = true;
        $.each(user_birth, function(idx, item){
            var value = $.trim($(item).val());
            var maxlength = $(item).attr('maxlength');
            if (!value) {
                alert("생년월일을 입력해주세요.");
                $(item).focus();
                rs = false;
                return false;
            }
            if (maxlength && value.length != maxlength) {
                alert("생년월일을 정확히 입력해주세요.");
                $(item).focus();
                rs = false;
                return false;
            }
        });
        if (!rs) return false;
    }
    $("#addInfoForm").submit();
}
</script>