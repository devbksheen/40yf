<div class="row text-center">
    <div class="col-lg-12 p0">
        <img src="<?php echo base_url('assets/user/img/mind-scan-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mind-scan-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<?php echo $layout->breadcrumbs?>

<div class="row text-center mb30">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mind-ab-text-banner2.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mind-ab-text-banner2.png')?>" class="w100p mobile"/>
    </div>
</div>

<div class="row">
    <form method="POST">
        <input type="hidden" name="type" value="mind-ab"/>
        
        <div class="col-lg-12 mb40">
            <div class="table-area">
                <table class="table2">
                    <colgroup>
                        <col width="7%"/>
                        <col/>
                        <col width="10%"/>
                        <col width="10%"/>
                        <col width="10%"/>
                        <col width="10%"/>
                    </colgroup>
                    <tr>
                        <th>구분</th>
                        <th>문항</th>
                        <th>거의<br/>드물게<br/>(1일 이하)</th>
                        <th>때로<br/>(1~2일)</th>
                        <th>상당히<br/>(3~4일)</th>
                        <th>대부분<br/>(5~7일)</th>
                    </tr>
                    <?php foreach ($question as $key => $item) { ?>
                        <tr>
                            <td class="text-center"><?php echo $key+1?></td>
                            <td><?php echo $item['question']?></td>
                            <?php foreach ($item['score'] as $idx => $score) { ?>
                                <td class="text-center">
                                    <div class="checkbox1">
                                        <input type="checkbox" name="<?php echo $item['name']?>" class="radio-checkbox" value="<?php echo $score?>"/>
                                    </div>
                                </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
        <div class="col-lg-12 text-right mb60">
            <button class="button1">결과보러가기</button>
        </div>
    </form>
</div>