<div class="row text-center">
    <div class="col-lg-12 p0">
        <img src="<?php echo base_url('assets/user/img/mind-scan-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mind-scan-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<?php echo $layout->breadcrumbs?>

<div class="row pt30 mb60">
    <div class="mindscan-type1">
        <a href="<?php echo base_url('mindscan/mindab')?>">
            <img src="<?php echo base_url('assets/user/img/mind-ab-button.png')?>" class="w100p pc"/>
            <img src="<?php echo base_url('assets/user/img/mobile/mind-ab-button.png')?>" class="w100p mobile"/>
        </a>
    </div>
    <div class="mindscan-type2">
        <a href="<?php echo base_url('mindscan/mindle')?>">
            <img src="<?php echo base_url('assets/user/img/mindle-button.png')?>" class="w100p pc"/>
            <img src="<?php echo base_url('assets/user/img/mobile/mindle-button.png')?>" class="w100p mobile"/>
        </a>
    </div>
</div>
<div class="row mb100">
    <div class="text-center">
        <button class="button4" type="button" onclick="window.open('https:\/\/forms.gle/kGbR18fThJvfVocG9')">성남시 의료원 후기작성</button>
    </div>
</div>