<div class="row text-center">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mind-scan-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mind-scan-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<?php echo $layout->breadcrumbs?>

<div class="row text-center mb30">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mindle-result-text-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mindle-result-text-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<div class="row mindle-results">
    <div class="col-lg-12 mb30">
        <p class="title"><span class="text-color2">▶</span> 내 마음 속 악동은?</p>
    </div>
    <div class="col-lg-12 mb50 item">
        <img src="<?php echo base_url('assets/user/img/mindle-result-'.$type.'-detail.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mindle-result-'.$type.'-detail.png')?>" class="w100p mobile"/>
    </div>
    <div class="col-lg-12 mb30">
        <p class="title"><span class="text-color2">▶</span> <?php echo $character?>와 비슷한 인물은?</p>
    </div>
</div>
<div class="row mindle-results mb50">
    <div class="col-lg-12">
        <div class="image-area">
            <img src="<?php echo base_url('assets/user/img/mindle-'.$type.'-person1.png')?>" class="w100p"/>
        </div>
        <div class="text-area pl30">
            <?php if ($type == 'A') { ?>
                <p class="person-title mb30">스카이캐슬의 “쓰앵님”</p>
                <p class="mb30">“어머니, 제 말을 들으셔야 합니다.” <span class="text-color2">빡센 기준과 높은 목표</span>를 향해 끊임없이 본인뿐만 아니라 주변 사람들을 푸쉬하는 쓰앵님! 하지만 그 빡셈에 본인도 가족도 주변사람도 점점 지치게 되죠~</p>
                <p class="mb30">내 안의 <span class="text-color2">“쓰앵님”이 들이대는 엄격한 잣대</span>로 인해서 스스로를 힘들게 몰아붙이고 있지는 않나요?</p>
                <p class="mb30">그렇다면 마인들링과 함께 엄격이를 찾아서 다루어줄 때 입니다!</p>


            <?php } else if ($type == 'B') { ?>
                <p class="person-title mb30">흥부와 놀부의 “흥부”</p>
                <p class="mb30"><span class="text-color2">“형님~ 저는 괜찮습니다. 다 가지세요~”</span> 형한테 탈탈 다 털어주고 나온 것도 모자라, 나중에 제비가 가져다 준 금은보화도 거지가 된 형에게 다 퍼주는, 역사속의 최강 물렁이 흥부!</p>
                <p>흥부처럼 내가 가진 것 없어도 다 퍼주고 맞춰주느라, 정작 나는 힘들지 않은가요?</p>
                <p><span class="text-color2">놀부같은 이들에게 상처</span>받지는 않았나요? 그렇다면 이제 나를 위해 내 안의 물렁이를 돌아볼 때입니다!</p>
                
                
            <?php } else if ($type == 'C') { ?>
                <p class="person-title mb30">겨울왕국의 “엘사”</p>
                <p class="mb30">엘사에겐 <span class="text-color2">특별한 마법 능력</span>이 있죠. 엘사는 이 능력을 꽁꽁 숨기고 싶어해서 아무리
                안나가 “나랑 눈사람 만들래~?”해도 방에서 나오지 않죠. 심지어 <span class="text-color2">얼음성</span>을 짓고 혼자
                “렛이꼬~”를 부르며 숨어지내려고 합니다. 사실 엘사의 능력은 타인을 해칠 수도 있고, 도울 수도 있는 <span class="text-color2">양면</span>이 있음에도요.</p>
                <p>엘사처럼 내 안의 어떤 점이 드러날까 두려워, 자꾸만 혼자있으려 하지는 않나요?</p>
                <p>그렇다면 마인들과 함께 당신의 고독이를 다루어 볼 때입니다.</p>


            <?php } else if ($type == 'D') { ?>
                <p class="person-title mb30">곰돌이 푸의 “피글렛”</p>
                <p class="mb30">곰돌이 푸의 귀여운 소심끝판왕. 달고사는 말은 <span class="text-color2">“어쩜 좋지?”, “난 몰라!”</span>일 정도로
                겁도 많고 소심한 우리 피글렛!</p>
                <p>왜소한 체격에 소심한 성격까지 더해지면서 친구들에게 은근 무시도 당하지만...</p>
                <p class="mb30">점점 내 안의 소중함과 대단함을 깨달으면서 위기의 순간에 친구들을 구합니다!</p>
                <p>“피글렛”처럼 용기를 내고 싶은데 자꾸만 걱정되고 불안한가요? 그렇다면 내 안의 콩콩이를 마인들과 함께 만나보아요~</p>    


            <?php } else if ($type == 'E') { ?>
                <p class="person-title mb30">둘리의 “고길동”</p>
                <p class="mb30">기본표정 인상 팍! 맨날 버럭! 뿅망치로 둘리의 머리를 뿅뿅 때리고 다니지만, 사실은 동네 말썽쟁이들을 다 걷어키워 준 속마음은 따뜻한 우리 길동이 아저씨.</p>
                <p class="mb30">겉으로 드러나는게 <span class="text-color2">츤츤! 버럭!</span>일 뿐, 당신의 <span class="text-color2">마음 속에도 고길동씨의 인간미</span>가 있을텐데... </p>
                <p>마인들과 함께 버럭!이 아닌 다른 방법으로 내 마음을 표현해볼까요?</p>
            <?php } ?>
        </div>
    </div>
</div>

<div class="row mindle-results mb60">
    <div class="col-lg-12">
        <div class="image-area">
            <img src="<?php echo base_url('assets/user/img/mindle-'.$type.'-person2.png')?>" class="w100p"/>
        </div>
        <div class="text-area pl30">
            <?php if ($type == 'A') { ?>
                <p class="person-title mb30">왕좌의 게임의 “아리아 스타크”</p>
                <p>복수를 위해 <span class="text-color2">철저한 계획</span>을 세우는 “아리아 스타크”.</p> 
                <p><span class="text-color2">쉼없이 노력</span>하지 않으면 살아남을 수 없는 환경탓에 안쓰러울 정도로 스스로를 몰아 부치고 훈련하죠! 덕분에 엄청난 내공으로 끝판왕을 물리치는데 결정적인 역할을</p>
                <p class="mb30">하지만... 그 과정에서 아리아가 겪는 고난과 희생은 눈물없이 볼 수 없다는거.</p>
                <p>아리아처럼 <span class="text-color2">목표를 위해 나를 갈아 넣고</span> 있지는 않은가요? 그렇다면 마인들링과 함께 내 안의 엄격이를 다루어 봅시다!</p> 
            
        
            <?php } else if ($type == 'B') { ?>
                <p class="person-title mb30">도라에몽의 “도라에몽”</p>
                <p class="mb30">사고뭉치 진구의 뒤치닥거리 전담! 진구가 조르면 <span class="text-color2">당할 껄 알면서도</span> 온갖 비밀도구를 빌려주고 도와주는 해결사 도라에몽!</p>
                <p class="mb30">가족들은 물론, 진구와 진구 친구들을 <span class="text-color2">보살펴 주는 보호자 역할</span>까지... 도라에몽처럼 <span class="text-color2">남들이 원하는 것</span>을 들어주고 맞추어주느라 지치고 있지 않나요?</p>
                <p>도라에몽 노릇은 이제 그만! 마인들링과 함께 내 안의 물렁이를 다루어 봅시다!</p>  


            <?php } else if ($type == 'C') { ?>
                <p class="person-title mb30">미녀와 야수의 “야수”</p>
                <p class="mb30">야수는 <span class="text-color2">남들과 다른</span> 자신의 모습이 드러나는 것이 싫어서 <span class="text-color2">큰 성안에 꽁꽁 숨어서</span>
                지내고 있습니다. 그 누구도 자신을 받아들이거나 사랑하지 않을거라고 굳게 믿고
                마음의 문을 닫았지요. 하지만 <span class="text-color2">있는 그대로의 “야수” 모습을 사랑해주는</span> “벨”을
                만나면서, 함께하는 것의 행복을 알게 되지요.</p>
                <p>야수처럼 “ 그 누구도 그대로의 나를 좋아하지 않을거야”라고 숨게 되지는 않나요?</p>
                <p>마인들과 함께 내 안의 고독이를 만나봅시다!</p>
                

            <?php } else if ($type == 'D') { ?>
                <p class="person-title mb30">미생의 “장그래”</p>
                <p class="mb30">바둑만 두다가 스펙없이 회사생활을 시작한 장그래! <span class="text-color2">“과연 내가 잘 할 수 있을까?”</span>
                두렵기도 하고, 어떻게 해야 하나... 걱정도 많죠.</p>
                <p class="mb30">이미 바둑판에서 실패를 경험했기에, 남들보다 더 두렵고, 스스로에 대한 의구심이 
                컸던 장그래이지만... <span class="text-color2">조금씩 도전하며 성장</span>해 나갑니다!</p>
                <p>장그래처럼 걱정많고 소심하지만, 내 꿈을 향해 한 걸음씩 나아가고 싶나요?</p>
                <p>마인들과 함께 내 안의 콩콩이부터 다루어 봅시다!</p>


            <?php } else if ($type == 'E') { ?>
                <p class="person-title mb30">싸이코지만 괜찮아의 “고문영”</p>
                <p>입만 열면 버럭! 까칠! 주변 사람들을 벌벌 떨게하는, 그래서 <span class="text-color2">혼자 외로웠던</span> 고문영.</p>
                <p class="mb30">어쩌면 그렇게 화를 내는 것 말고는 내 마음이 힘들다는 것을 표현하는 방법을 잘 몰랐을거에요.</p>
                <p class="mb30">까칠한 문영이가 강태를 만나 서서히 변화해 나가는 모습이 감동 포인트! 문영이처럼 <span class="text-color2">속마음은 여리고 힘든데, 어쩐지 자꾸 버럭</span>하는 식으로만 표현하게 되나요?</p>
                <p>그렇다면 마인들과 함께 내 안의 버럭이를 다루러 가봅시다!</p>
            <?php } ?>
        </div>
    </div>
</div>

<div class="row mindle-results mb40">
    <div class="col-lg-12">
        <p class="title"><span class="text-color2">▶ (맛보기)</span> STEP 1. <?php echo $character?> 만나기</p>
        <div class="p20">
            <video controlsList="nodownload" controls width="100%" poster="<?php echo base_url('assets/user/img/mindle-'.$type.'/video1.png')?>">
                <source src="https://40fy.s3.ap-northeast-2.amazonaws.com/video/mindle-<?php echo $type?>1.mp4" type="video/mp4">
            </video>
        </div>
    </div>
</div>
<div class="row mindle-results mb60">
    <div class="button-area">
        <button type="button" class="button3" onclick="location.href='<?php echo base_url('mindscan/mindleResult')?>?type=all'">결과 페이지로</button>
        <button type="button" class="button2 fr" onclick="location.href='<?php echo base_url('mindle/type')?>'">MINDLE 시작하기</button>
    </div>
</div>


