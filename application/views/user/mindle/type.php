<div class="row text-center">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mindle-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mindle-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<?php echo $layout->breadcrumbs?>

<div class="row text-center mb30">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mindle-text-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mindle-text-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<div class="row mindle-results">
    <div class="col-lg-12 mb30">
        <p class="title"><span class="text-color2">▶</span> 궁금한 악동유형을 선택해서 마인들을 시작해봅시다!</p>
    </div>

    <div class="col-lg-12 mb50 item">
        <a href="<?php echo base_url('mindle/list')?>?type=A">
            <img src="<?php echo base_url('assets/user/img/mindle-A/start.png')?>" class="w100p"/>
            <!-- <img src="<?php echo base_url('assets/user/img/mobile/mindle-A/start.png')?>" class="w100p mobile"/> -->
        </a>
    </div>

    <div class="col-lg-12 mb50 item">
        <a href="<?php echo base_url('mindle/list')?>?type=B">
            <img src="<?php echo base_url('assets/user/img/mindle-B/start.png')?>" class="w100p"/>
            <!-- <img src="<?php echo base_url('assets/user/img/mobile/mindle-B/start.png')?>" class="w100p mobile"/> -->
        </a>
    </div>

    <div class="col-lg-12 mb50 item">
        <a href="<?php echo base_url('mindle/list')?>?type=C">
            <img src="<?php echo base_url('assets/user/img/mindle-C/start.png')?>" class="w100p"/>
            <!-- <img src="<?php echo base_url('assets/user/img/mobile/mindle-C/start.png')?>" class="w100p mobile"/> -->
        </a>
    </div>

    <div class="col-lg-12 mb50 item">
        <a href="<?php echo base_url('mindle/list')?>?type=D">
            <img src="<?php echo base_url('assets/user/img/mindle-D/start.png')?>" class="w100p"/>
            <!-- <img src="<?php echo base_url('assets/user/img/mobile/mindle-D/start.png')?>" class="w100p mobile"/> -->
        </a>
    </div>

    <div class="col-lg-12 mb50 item">
        <a href="<?php echo base_url('mindle/list')?>?type=E">
            <img src="<?php echo base_url('assets/user/img/mindle-E/start.png')?>" class="w100p"/>
            <!-- <img src="<?php echo base_url('assets/user/img/mobile/mindle-E/start.png')?>" class="w100p mobile"/> -->
        </a>
    </div>
        
</div>