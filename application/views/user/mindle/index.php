<div class="row text-center">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mindle-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mindle-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<?php echo $layout->breadcrumbs?>

<div class="row text-center mb30">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mindle-text-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mindle-text-banner.png')?>" class="w100p mobile"/>
    </div>
</div>
<div class="row text-center mb50 mindle">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mindle-banner2.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mindle-banner2.png')?>" class="w100p mobile"/>
        <img src="<?php echo base_url('assets/user/img/mindle-banner3.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mindle-banner3.png')?>" class="w100p mobile"/>
    </div>
    <div class="col-lg-12 text-center mt40">
        <!-- <button type="button" class="button5" onclick="window.open('https:\/\/docs.google.com/forms/d/e/1FAIpQLScd6MflgDqKroiN81ZmNvoNNH4G6bXssxR8ZhIew0ztFxS_3A/viewform')">무료체험 신청하기</button> -->
        <button type="button" class="button4" onclick="location.href = '<?php echo base_url('mindscan/mindle')?>'">MINDLE 시작하기</button>
    </div>
</div>

<div class="row mindle mb50">
    <div class="col-lg-12">
        <div class="video-box">
            <p class="title mb20"><span class="text-color2">▶</span> MINDLE? MINDLING!</p>
            <video controlsList="nodownload" controls width="100%" poster="<?php echo base_url('assets/user/img/video5.png')?>">
                <source src="https://40fy.s3.ap-northeast-2.amazonaws.com/video/mindle.mp4" type="video/mp4">
            </video>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mindle-contents.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mindle-contents.png')?>" class="w100p mobile"/>
    </div>
</div>


<style>
    .header { border-bottom: none!important; }
</style>