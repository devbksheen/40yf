<div class="row text-center">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mindle-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mindle-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<?php echo $layout->breadcrumbs?>

<div class="row text-center mb30">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mindle-text-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mindle-text-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<div class="mindle-result">
    <?php foreach ($videos as $idx => $video) { ?>
        <?php if ($video['id'] == 'final') continue; ?>
        <div class="row contents-area">
            <?php if ($video['src']) { ?>
                <a href="<?php echo base_url('mindle/view/'.$type)?>?id=<?php echo $video['id']?>">
            <?php } else { ?>
                <a href="javascript: alert('향후 정식버전에서 이용가능합니다!');">
            <?php } ?>
            <div class="col-lg-4">
                <img src="<?php echo base_url($video['poster'])?>" class="w100p"/>
            </div>
            <div class="col-lg-8">
                <p class="contents-title mt10"><?php echo $video['title']?></p>
                <ul class="contents">
                    <?php if (isset($video['stage'])) { ?>
                        <?php foreach ($video['stage'] as $idx2 => $stage) { ?>
                            <li><p><span class="text-color2"><?php echo $stage['color']?></span> <?php echo $stage['title']?></p></li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
            </a>
        </div>
    <?php } ?>
</div>

<style>
    .header { border-bottom: none!important; }
</style>