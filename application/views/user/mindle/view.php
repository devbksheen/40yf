<div class="row text-center">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mindle-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mindle-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<?php echo $layout->breadcrumbs?>

<div class="row text-center mb40">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mindle-text-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mindle-text-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<div class="row mindle-view">

    <div class="col-lg-12">
        <p class="video-title text-center mb60">내 마음속 악동은? <span class="text-color2"><?php echo $character?> (<?php echo $video['viewTitle']?>)</span></p>
        <?php if (isset($video['src']) && $video['src']) { ?>
            <div class="step-area" data-idx="0">
                <img src="<?php echo base_url($video['titleImg'])?>" class="w100p mb40 pc">
                <img src="<?php echo base_url($video['titleMobileImg'])?>" class="w100p mb40 mobile">
        
                <div class="video-area mb60">
                    <video controls controlsList="nodownload" width="100%" poster="<?php echo base_url($video['poster'])?>" id="video">
                        <source src="<?php echo $video['src']?>" type="video/mp4">
                    </video>
                    <ul class="contents">
                        <?php if (isset($video['stage'])) { ?>
                            <?php foreach ($video['stage'] as $idx2 => $stage) { ?>
                                <li><p><span class="text-color2"><?php echo $stage['color']?></span> <?php echo $stage['title']?></p></li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
        
                <div class="button-area mb60">
                    <button type="button" class="button3" onclick="location.href='<?php echo base_url('mindle/list')?>?type=<?php echo $type?>'">목록으로</button>
                    <button type="button" class="button2 fr" onclick="nextStep(1)" style="width: auto; padding-left: 20px; padding-right: 20px;">악동 길들이러가기</button>
                </div>
            </div>
        <?php } ?>
    </div>
    
    <?php foreach ($video['step'] as $idx => $step) { ?>
        <div class="col-lg-12 step-area" data-idx="<?php echo $idx + 1?>" style="display: none;">
            <img src="<?php echo base_url($step['img'])?>" class="w100p mb40 pc">
            <img src="<?php echo base_url($step['mobileImg'])?>" class="w100p mb40 mobile">

            <div class="button-area">
                <?php if ($video['id'] == 'final') { ?>
                    <button type="button" class="button3 mb60" onclick="location.href='<?php echo base_url('mindle/list')?>?type=<?php echo $type?>'">목록으로</button>

                    <?php if ($this->login_lib->normalUser()) { ?>
                        <button type="button" class="button2 fr mb60" onclick="window.open('https:\/\/docs.google.com/forms/d/e/1FAIpQLSc0TCYAU6OGgOvOPTA9bLkrIwPPxEA4LrJP72HGEnWeYtuF_g/viewform'); location.href='<?php echo base_url('mindle/type')?>'; " style="width: auto; padding-left: 20px; padding-right: 20px;">악동 길들이기 완료</button>
                    <?php } else { ?>
                        <button type="button" class="button2 fr mb60" onclick="location.href='<?php echo base_url('mindle/type')?>'; " style="width: auto; padding-left: 20px; padding-right: 20px;">악동 길들이기 완료</button>
                    <?php } ?>
                <?php } else { ?>
                    <button type="button" class="button2 fr mb60" onclick="nextStep(<?php echo $idx + 2?>)" style="width: auto; padding-left: 20px; padding-right: 20px;">다음으로</button>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
</div>

<style>
    .header { border-bottom: none!important; }
</style>

<script>
    var video = <?php echo json_encode($video)?>;
    var nextVideo = <?php echo !empty($nextVideo) ? json_encode($nextVideo) : 'false' ?>;
    $(document).ready(function() {
        if (!nextVideo && $('.step-area[data-idx="0"]').length == 0) {
            nextStep(1);
        }
    });
    
    function nextStep(idx)
    {
        if ($('#video').length > 0) {
            $('#video').get(0).pause();
        }
        if (video.step[idx - 1]) {
            $('.step-area').hide();
            $('.step-area[data-idx='+idx+']').show();
        } else {
            if (nextVideo && nextVideo.src) {
                location.href='<?php echo base_url('mindle/view')?>/<?php echo $type?>?id='+nextVideo.id;
            } else {
                location.href='<?php echo base_url('mindle/view')?>/<?php echo $type?>?id=final';
            }
        }
    }
</script>