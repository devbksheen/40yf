<div class="row text-center">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mind-ab-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mind-ab-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<?php echo $layout->breadcrumbs?>

<div class="row text-center mb20">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mind-ab-text-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mind-ab-text-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<div class="mind-ab">
    <?php foreach ($videos as $idx => $video) { ?>
        <div class="row contents-area">
            <a href="<?php echo base_url('mindab/view/'.$video['id'])?>">
            <div class="col-lg-4">
                <img src="<?php echo base_url($video['poster'])?>" class="w100p"/>
            </div>
            <div class="col-lg-8">
                <p class="contents-title mt10"><?php echo $video['title']?></p>
                <ul class="contents">
                    <?php if (isset($video['stage'])) { ?>
                        <?php foreach ($video['stage'] as $idx2 => $stage) { ?>
                            <li><p><span class="text-color1"><?php echo $stage['color']?></span> <?php echo $stage['title']?></p></li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
            </a>
        </div>
    <?php } ?>
</div>

<style>
    .header { border-bottom: none!important; }
</style>