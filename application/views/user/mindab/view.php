<div class="row text-center">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mind-ab-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mind-ab-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<?php echo $layout->breadcrumbs?>

<div class="row text-center mb20">
    <div class="col-lg-12">
        <img src="<?php echo base_url('assets/user/img/mind-ab-text-banner.png')?>" class="w100p pc"/>
        <img src="<?php echo base_url('assets/user/img/mobile/mind-ab-text-banner.png')?>" class="w100p mobile"/>
    </div>
</div>

<div class="row mind-ab-view">
    <div class="col-lg-12">
        <p class="video-title text-center mt40 mb40"><?php echo $video['title']?></p>
        <div class="video-area mb60">
            <video controls controlsList="nodownload" width="100%" poster="<?php echo base_url($video['poster'])?>">
                <source src="<?php echo $video['src']?>" type="video/mp4">
            </video>
            <ul class="contents">
                <?php if (isset($video['stage'])) { ?>
                    <?php foreach ($video['stage'] as $idx2 => $stage) { ?>
                        <li><p><span class="text-color1"><?php echo $stage['color']?></span> <?php echo $stage['title']?></p></li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
        <div class="button-area mb60">
            <button type="button" class="button3" onclick="location.href='<?php echo base_url('mindab')?>'">목록으로</button>
            <?php if ($nextVideo) { ?>
                <button type="button" class="button1 fr" onclick="location.href='<?php echo base_url('mindab/view/'. $nextVideo['id'])?>'">다음 단계가기</button>
            <?php } ?>
        </div>
    </div>
</div>

<style>
    .header { border-bottom: none!important; }
</style>