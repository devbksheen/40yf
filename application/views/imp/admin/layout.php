<!DOCTYPE html>
<html>
    <?php echo $layout->head ?>
    <body>
        <?php echo $layout->header ?>
        
        <!-- Nav가 Header와 분리된 레이아웃일때 사용 -->
        <?php // echo $layout->nav ?>
        
        <?php echo $layout->contents ?>
        
        <?php echo $layout->footer ?>
    </body>
</html>