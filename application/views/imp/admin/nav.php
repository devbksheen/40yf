<nav>
    <ul>
        <?php if (!empty($_nav)) { ?>
            <?php foreach ($_nav as $key => $item) { ?>
                <li class="<?php echo $item['url'] == $this->uri->segment(1) ? 'on' : ''?>">
                    <a href="<?php echo base_url($item['url'])?>"><?php echo $item['title']?></a>
                    <?php if (isset($item['subNav']) && count($item['subNav']) > 0) { ?>
                        <ul style="<?php echo $item['url'] == $this->uri->segment(1) ? '' : 'display: none;'?>">
                            <?php foreach ($item['subNav'] as $key2 => $subNav) { ?>
                                <li><a href="<?php echo base_url($subNav['url'])?>"><?php echo $subNav['title']?></a></li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </li>
            <?php } ?>
        <?php } ?>
    </ul>
</nav>