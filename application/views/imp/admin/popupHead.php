<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="title" content="<?php echo $layout->title?>" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="" id="og_link" />
    <meta property="og:title" content="" id="og_title" />
    <meta property="og:description" content="" id="og_desc" />
    <meta property="og:image" content="" id="og_image" />
    <title><?php echo $layout->title?></title>
    
    <!-- 추가 Js -->
    <?php if (!empty($layout->addJs)) { ?>
        <?php foreach($layout->addJs as $key => $js) { ?>
            <script type="text/javascript" src="<?php echo $js?>"></script>
        <?php } ?>
    <?php } ?>

    <!-- 추가 Css -->
    <?php if (!empty($layout->addCss)) { ?>
        <?php foreach($layout->addCss as $key => $css) { ?>
            <link rel="stylesheet" href="<?php echo $css?>">
        <?php } ?>
    <?php } ?>
</head>