<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <!-- <meta name="viewport" content="width=1920, initial-scale=1.0"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="title" content="<?php echo $layout->title?>" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="" id="og_link" />
    <meta property="og:title" content="" id="og_title" />
    <meta property="og:description" content="" id="og_desc" />
    <meta property="og:image" content="" id="og_image" />
    <title><?php echo $layout->title?></title>

    <!-- JS -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <!-- <script type="text/javascript" src="<?php echo base_url('plugin/jquery-kk-message/message.js')?>"></script> -->
    <script type="text/javascript" src="<?php echo base_url('assets/common/js/common.js')?>"></script>

    <!-- 추가 JS -->
    <?php if (!empty($layout->addJs)) { ?>
        <?php foreach($layout->addJs as $key => $js) { ?>
            <script type="text/javascript" src="<?php echo $js?>"></script>
        <?php } ?>
    <?php } ?>


    <!-- CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/common/css/common.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/user/css/40fy.css')?>">
        
    <!-- 추가 CSS -->
    <?php if (!empty($layout->addCss)) { ?>
        <?php foreach($layout->addCss as $key => $css) { ?>
            <link rel="stylesheet" href="<?php echo $css?>">
        <?php } ?>
    <?php } ?>
    
</head>
