<!-- header -->
<header>
    <div class="row header">
        <div class="col-lg-12">
            <a href="<?php echo base_url()?>" class="logo">
                <img src="<?php echo base_url('assets/user/img/logo.png')?>" class="pc"/>
                <img src="<?php echo base_url('assets/user/img/mobile/logo.png')?>" class="mobile"/>
            </a>
            <?php if (!empty($_nav)) { ?>
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <?php foreach ($_nav as $key => $item) { ?>
                                <?php if (isset($item['hide']) && $item['hide'] == true) continue; ?>
                                <?php if ((!$this->login_lib->info('is_login') && $item['url'] == 'mindab') || ($this->login_lib->normalUser() && $item['url'] == 'mindab')) continue;?>
                                    <li class="<?php echo $item['url'] == $this->uri->segment(1) ? 'on' : ''?>"><a href="<?php echo $item['url'] ? base_url($item['url']) : 'javascript: alert(\'준비중입니다.\');'?>"><?php echo $item['title']?></a></li>
                                <?php } ?>
                            <li class="login-btn">
                                <?php if ($this->login_lib->info('is_login')) { ?>
                                    <a href="<?php echo base_url('logout')?>">로그아웃</a>
                                <?php } else { ?>
                                    <a href="<?php echo base_url('login')?>">로그인</a>
                                <?php } ?>
                            </li>
                        </ul>
                    </div>
                </nav>
            <?php } ?>
        </div>
    </div>
</header>
<!-- header -->