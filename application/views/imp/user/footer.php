<!-- footer -->
<footer>
    <div class="row footer">
        <p class="mb10"><a href="<?php echo base_url('personalInformation')?>">개인정보 처리방침</a> | <a href="<?php echo base_url('serviceAgreement')?>">서비스 이용약관</a></p>
        <p>(주)포티파이(40FY Inc.) | 대표 문우리  | 문의처 : help@40fy.us | 카카오 채널 "40FY" 검색 후 1:1 문의</p>
        <p>서울특별시 중구 청계천로 100, 10층 1046호 | 사업자등록번호 : 733-86-01947 | 통신판매업신고 : 2020-서울중구-2123</p>
    </div>
</footer>
<!-- footer -->