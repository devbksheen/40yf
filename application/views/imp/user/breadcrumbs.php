<?php if (!empty($_nav) && $this->uri->segment(1)) { ?>
    <div class="row">
        <div class="col-xl-12 pl20">
            <div class="breadcrumbs">
                <p>
                    <a href="<?php echo base_url()?>">
                        <img src="<?php echo base_url('assets/user/img/home.png')?>" class="home"/> 
                        홈
                    </a>
                    <?php $segment = $this->uri->segment(1);?>
                    <?php $segment2 = $this->uri->segment(2);?>
                    <?php foreach ($_nav as $key => $item) { ?>
                        <?php if ($item['url'] == $segment) { ?>
                            > <a href="<?php echo base_url($item['url'])?>"><?php echo $item['title']?></a>
                        <?php } ?>
                        <?php if ($segment2 && isset($item['subnav'])) { ?>
                            <?php foreach ($item['subnav'] as $key2 => $item2) { ?>
                                <?php if ($item2['url'] == $segment2) { ?>
                                    > <a href="<?php echo base_url($item['url'].'/'.$item2['url'])?>"><?php echo $item2['title']?></a>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                </p>
            </div>
        </div>
    </div>
<?php } ?>