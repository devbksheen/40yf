<!DOCTYPE html>
<html>
    <?php echo $layout->head ?>
    <body>
        <?php echo $layout->header ?>
        
        <?php echo $layout->contents ?>
        
        <?php echo $layout->footer ?>
    </body>
</html>