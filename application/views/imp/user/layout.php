<!DOCTYPE html>
<html>
    <?php echo $layout->head ?>
    <body>
        <div id="wrap" class="container">
            <?php echo $layout->header ?>

            <!-- Nav가 Header와 분리된 레이아웃일때 사용 -->
            <?php // echo $layout->nav ?>
            
            <?php echo $layout->contents ?>
            
            <?php echo $layout->footer ?>
        </div>
    </body>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-1XEEECJ3G4"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'G-1XEEECJ3G4');
    </script>
</html>