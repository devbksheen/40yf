<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package     CodeIgniter
 * @author      ExpressionEngine Dev Team
 * @copyright   Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license     http://codeigniter.com/user_guide/license.html
 * @link        http://codeigniter.com
 * @since       Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Alert Helpers
 *
 * @package     CodeIgniter
 * @subpackage  Helpers
 * @category    Helpers
 * @author      ExpressionEngine Dev Team
 * @link        http://codeigniter.com/user_guide/helpers/array_helper.html
 */


// 경고메세지를 경고창으로 주소가있다면 이동.
function alert($msg='', $url='') {
 $CI =& get_instance();

 if (!$msg) $msg = '잘못된 접근입니다.';

 echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=".$CI->config->item('charset')."\">";
 echo "<script type='text/javascript'>alert('".$msg."');";
    if ($url) {
        echo "
            if (window.parent.length > 0) {
                window.parent.location.replace('".$url."');
            } else {
                location.replace('".$url."');
            }
        ";
    } else {
        echo "history.go(-1);";
    }
 echo "</script>";
 exit;
}

function alert_close($msg='', $reload = true, $url = "") {
 $CI =& get_instance();

 if (!$msg) $msg = '잘못된 접근입니다.';

 echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=".$CI->config->item('charset')."\">";
 echo "<script type='text/javascript'>alert('".$msg."');";
 echo "window.close();";
    if ($reload) {
        echo "opener.parent.location.reload();";
    }
    if ($url) {
        echo "opener.parent.location = '".$url."';";
    }
 echo "</script>";
 exit;
}

// 경고메세지를 경고창으로 주소가있다면 이동.
function __app_alert($msg='', $url='') {
 $CI =& get_instance();

 if (!$msg) $msg = '올바른 방법으로 이용해 주십시오.';

 echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=".$CI->config->item('charset')."\">";
 echo "<script type='text/javascript'>alert('".$msg."');";
    if ($url) {
        echo "
            location.replace('".$url."');
        ";
    } else {
        echo "history.go(-1);";
    }
 echo "</script>";
 exit;
}
