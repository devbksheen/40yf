$(document).ready(function(){
    // 검사 페이지 checkbox 한개만 클릭 가능
    $(document).on('click', '.radio-checkbox', function(){
        var name = $(this).attr('name');
        $('.radio-checkbox[name="'+name+'"]').prop('checked', false);
        $(this).prop('checked', true);
    });

    // 개발자 도구 방지
    $(document).bind('keydown',function(e){
        if ( e.keyCode == 123 /* F12 */) {
            e.preventDefault();
            e.returnValue = false;
        }
    });

    // 우측 클릭 방지
    document.onmousedown = function(event) {
        if (event.button == 2) {
            alert('보안을 위해 마우스 우클릭은 사용하실 수 없습니다.');
            return false;
        }
    }
    $("video").bind( 'contextmenu', function() { return false; } );

    // onlynum
    $(document).on('keyup', '[onlynum]', function(){
        $(this).val($(this).val().replace(/[^0-9]/g,""));
    });
});
