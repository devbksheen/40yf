-- --------------------------------------------------------
-- 호스트:                          127.0.0.1
-- 서버 버전:                        10.1.9-MariaDB - mariadb.org binary distribution
-- 서버 OS:                        Win64
-- HeidiSQL 버전:                  10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 40fy 데이터베이스 구조 내보내기
DROP DATABASE IF EXISTS `40fy`;
CREATE DATABASE IF NOT EXISTS `40fy` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `40fy`;

-- 테이블 40fy.organ_user 구조 내보내기
DROP TABLE IF EXISTS `organ_user`;
CREATE TABLE IF NOT EXISTS `organ_user` (
  `orguser_id` int(11) NOT NULL AUTO_INCREMENT,
  `orguser_num` varchar(50) NOT NULL,
  `orguser_birth` varchar(6) NOT NULL,
  `orguser_regdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `orguser_moddate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`orguser_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- 테이블 데이터 40fy.organ_user:~0 rows (대략적) 내보내기
DELETE FROM `organ_user`;
/*!40000 ALTER TABLE `organ_user` DISABLE KEYS */;
INSERT INTO `organ_user` (`orguser_id`, `orguser_num`, `orguser_birth`, `orguser_regdate`, `orguser_moddate`) VALUES
	(1, '111', '931020', '2020-11-19 23:43:04', NULL);
/*!40000 ALTER TABLE `organ_user` ENABLE KEYS */;

-- 테이블 40fy.user 구조 내보내기
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_loginid` varchar(50) NOT NULL,
  `user_password` varchar(200) NOT NULL,
  `user_regdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_moddate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- 테이블 데이터 40fy.user:~0 rows (대략적) 내보내기
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`user_id`, `user_loginid`, `user_password`, `user_regdate`, `user_moddate`) VALUES
	(1, 'testid', '', '2020-11-19 23:44:22', NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
